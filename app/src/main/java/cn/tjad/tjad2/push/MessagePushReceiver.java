package cn.tjad.tjad2.push;

import android.content.Context;
import android.util.Log;

import cn.jpush.android.api.CustomMessage;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.service.JPushMessageReceiver;
import cn.tjad.tjad2.application.TjadApplication;

public class MessagePushReceiver extends JPushMessageReceiver {

    private static final String TAG = "MessagePushReceiver";
    private NotificationUtils notificationUtils = new NotificationUtils(TjadApplication.getInstance());


    @Override
    public void onAliasOperatorResult(Context context, JPushMessage jPushMessage) {
        Log.d(TAG, "set align: " + jPushMessage.getErrorCode() + ", " + jPushMessage.getAlias());
        TagAliasOperatorHelper.getInstance().onTagOperatorResult(context, jPushMessage);
    }

    @Override
    public void onTagOperatorResult(Context context, JPushMessage jPushMessage) {
        Log.d(TAG, "set tag: " + jPushMessage.getErrorCode() + ", " + jPushMessage.getTags().toString());
        TagAliasOperatorHelper.getInstance().onTagOperatorResult(context, jPushMessage);
    }

    @Override
    public void onCheckTagOperatorResult(Context context, JPushMessage jPushMessage) {
        TagAliasOperatorHelper.getInstance().onTagOperatorResult(context, jPushMessage);
    }

    @Override
    public void onMessage(Context context, CustomMessage customMessage) {
        Log.d(TAG, customMessage.toString());
        notificationUtils.sendNotification(customMessage.title, customMessage.message);
    }

}
