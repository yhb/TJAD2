package cn.tjad.tjad2.push;

public interface PushMessageId {

    int SET_ALIGN = 2001;

    int SET_PHONE_NUMBER = 2002;

    int SET_TAG = 2003;
}
