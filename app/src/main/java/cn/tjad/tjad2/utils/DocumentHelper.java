package cn.tjad.tjad2.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.qiandian.android.base.common.utils.UrlUtils;

import java.io.File;

import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.consts.TjadConsts;
import cn.tjad.tjad2.consts.UrlConsts;
import cn.tjad.tjad2.data.bean.Document;
import cn.tjad.tjad2.net.TjadHttpServer;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.module.common.dialog.OnDialogClickListener;
import cn.tjad.tjad2.module.common.dialog.TjDialogFragment;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DocumentHelper {

    public static void openModelFile(Context context, Document document) {
        openModelFile(context, document, null, 0);
    }

    public static void openModelFile(Context context, Document document, String target, int targetId) {
        if (document == null || !"0".equals(document.forgeStatus)) {
            return;
        }
        String localFile = TjadConsts.UNZIP_DOCUMENT + "/" + document.name;
        final String sourceFile = TjadConsts.DOWNLOAD_FILE + "/" + document.name + ".zip";
        File file = new File(localFile);
        File sourceFileZip = new File(sourceFile);
        if (file.exists()) {
            openLocalFile(context, localFile, document.id, target, targetId);
        } else if (sourceFileZip.exists()){
            //本地存在zip包, 直接解压. 解压后打开
            Observable.create(observableEmitter -> ZipUtils.unzipAndRenameFolder(sourceFile, TjadConsts.UNZIP_DOCUMENT))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnComplete(() -> openModelFile(context, document, target, targetId))
                    .subscribe();
        } else {
            viewOnlineFileWithNetStatus(context, document, target, targetId);
        }
    }

    public static void openLocalFile(Context context, String localFile, int documentId) {
        openLocalFile(context, localFile, documentId, null, 0);
    }

    public static void openLocalFile(Context context, String localFile, int documentId, String target, int targetId) {
        //本地已存在解压后的文件
        String token = TjadApplication.getInstance().getSession().getToken();
        String url = UrlUtils.getUrl(UrlConsts.LOCAL_DOCUMENT_PREVIEW_URL, "" + TjadHttpServer.PORT, TjadConsts.APPHTML,
                Tools.getProjectId() + "", token, localFile);
        if (target != null) {
            url = new StringBuilder(url)
                    .append("&")
                    .append(target)
                    .append("=")
                    .append(targetId)
                    .toString();
        }
        if (documentId > 0) {
            url = new StringBuilder(url)
                    .append("&documentId=")
                    .append(documentId)
                    .toString();
        }
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        RouteUtils.toPage(context, PageId.WEBVIEW, bundle);
    }


    public static void viewOnlineFileWithNetStatus(Context context, final Document document) {
        viewOnlineFileWithNetStatus(context, document, null, 0);
    }

    public static void viewOnlineFileWithNetStatus(Context context, final Document document, String target, int targetId) {
        if (document != null && !"0".equals(document.forgeStatus)) {
            return;
        }
        if (!NetworkUtils.isNetworkConnected(context)) {
            Tools.showToast("网络连接失败, 请检查网络!");
            return;
        }
        if (!NetworkUtils.isWifi(context)) {
            TjDialogFragment.Builder builder = new TjDialogFragment.Builder(context);
            builder.setTitle("提示")
                    .setMessage("您当前为非WIFI网络, 是否继续?")
                    .setPositiveButton("继续", new OnDialogClickListener() {
                        @Override
                        public boolean onClick(Dialog dialog, int which) {
                            viewOnlineFile(context, document, target, targetId);
                            return true;
                        }
                    })
                    .setNegativeButton("取消", null)
                    .create().show(((AppCompatActivity)context).getSupportFragmentManager(), "viewOnlineFile");
        } else {
            viewOnlineFile(context, document, target, targetId);
        }
    }

    //预览在线文件
    public static void viewOnlineFile(Context context, Document document, String target, int targetId) {
        if (document == null) {
            return;
        }
        if (document != null && "0".equals(document.forgeStatus)) {
            String localPath = TjadConsts.APPHTML;
            int id = document.id;
            String token = TjadApplication.getInstance().getSession().getToken();
            String url = UrlUtils.getUrl(UrlConsts.REMOTE_DOCUMENT_PREVIEW_URL, "" + TjadHttpServer.PORT, localPath,
                    String.valueOf(Tools.getProjectId()), token, String.valueOf(id));
            if (target != null) {
                url = new StringBuilder(url)
                        .append("&")
                        .append(target)
                        .append("=")
                        .append(targetId)
                        .toString();
            }
            Bundle bundle = new Bundle();
            bundle.putString("url", url);
            RouteUtils.toPage(context, PageId.WEBVIEW, bundle);
        }
    }
}
