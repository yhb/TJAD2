package cn.tjad.tjad2.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.bruce.pickerview.popwindow.DatePickerPopWin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatePickUtils {

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static void showDatePicker(Context context, String date, DatePickerPopWin.OnDatePickedListener onDatePickedListener) {
        if (TextUtils.isEmpty(date)) {
            date = dateFormat.format(new Date());
        }
        DatePickerPopWin datePickerPopWin = new DatePickerPopWin.Builder(context, onDatePickedListener).textCancel("取消")
                .textConfirm("确定")
                .btnTextSize(16)
                .viewTextSize(25)
                .minYear(1990)
                .maxYear(2500)
                .showDayMonthYear(true)
                .dateChose(date)
                .build();
        datePickerPopWin.showPopWin((Activity) context);
    }
}
