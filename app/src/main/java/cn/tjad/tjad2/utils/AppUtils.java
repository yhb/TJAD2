package cn.tjad.tjad2.utils;


import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;

import cn.tjad.tjad2.application.TjadApplication;

/**
 * Created by yanghongbing on 17/10/14.
 */

public class AppUtils {

    public static String getString(int resId) {
        return TjadApplication.getInstance().getString(resId);
    }

    public static String getString(int res, Object... args) {
        return TjadApplication.getInstance().getString(res, args);
    }

    public static String[] getStringArray(int resId) {
        return TjadApplication.getInstance().getResources().getStringArray(resId);
    }

    public static int getColor(int colorResId) {
        return TjadApplication.getInstance().getResources().getColor(colorResId);
    }

    public static int getDimens(int dimenResId) {
        return TjadApplication.getInstance().getResources().getDimensionPixelSize(dimenResId);
    }

    public static ColorStateList getColorStateList(int colorResId) {
        return TjadApplication.getInstance().getResources().getColorStateList(colorResId);
    }

    public static Drawable getDrawable(int drawableResId) {
        return TjadApplication.getInstance().getResources().getDrawable(drawableResId);
    }
}
