package cn.tjad.tjad2.utils;


import android.os.Handler;

public class ExitAppManager extends Handler implements Runnable {

    private long lastClickTime;

    private int exitInterval = 2000;

    private OnExitAppTriggerListener onExitAppTriggerListener;

    public void setOnExitAppTriggerListener(OnExitAppTriggerListener onExitAppTriggerListener) {
        this.onExitAppTriggerListener = onExitAppTriggerListener;
    }

    public void onBackClick() {
        if (lastClickTime == 0) {
            lastClickTime = System.currentTimeMillis();
            Tools.showToast("再按一次返回键退出APP");
        } else {
            long now = System.currentTimeMillis();
            long duration = now - lastClickTime;
            if (duration < exitInterval) {
                if (onExitAppTriggerListener != null) {
                    onExitAppTriggerListener.onExitAppTriggered();
                }
            } else {
                lastClickTime = now;
            }
        }
        postDelayed(this, exitInterval);
    }

    @Override
    public void run() {
        lastClickTime = 0;
    }


    public interface OnExitAppTriggerListener {
        void onExitAppTriggered();
    }
}
