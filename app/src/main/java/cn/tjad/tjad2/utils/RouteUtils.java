package cn.tjad.tjad2.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.alibaba.android.arouter.launcher.ARouter;

import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.route.PageId;

public class RouteUtils {

    public static void toPage(Context context, String path, Bundle params) {
        ARouter.getInstance().build(path)
                .with(params)
                .navigation(context);
    }

    public static void toPageForResult(Activity activity, String path, Bundle params, int requestCode) {
        ARouter.getInstance().build(path)
                .with(params)
                .navigation(activity, requestCode);
    }

    public static void toFragment(Fragment container, String pageId, Bundle args) {
        Fragment fragment = (Fragment) ARouter.getInstance().build(pageId).navigation();
        if (fragment != null) {
            fragment.setArguments(args);
            FragmentManager fragmentManager = container.getChildFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(container.getView().getId(), fragment);
            transaction.addToBackStack(fragment.getClass().getSimpleName());
            transaction.commit();
        }
    }

    public static void toFragmentActivity(Context context, String fragmentId, Bundle args) {
        ARouter.getInstance().build(PageId.FRAGMENT_CONTAINER_ACTIVITY)
                .withString(Keys.FRAGMENT_ID, fragmentId)
                .withBundle(Keys.PAGE_ARGS, args)
                .navigation(context);
    }
}
