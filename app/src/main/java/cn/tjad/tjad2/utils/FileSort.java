package cn.tjad.tjad2.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import cn.tjad.tjad2.data.bean.FileBean;

public class FileSort {

    public static final int SORT_BY_NAME = 0;
    public static final int SORT_BY_TIME = 1;
    public static final int SORT_BY_SIZE = 2;
    public static final int SORT_BY_TYPE = 3;

    public static final int SORT_ASCEND = 1;
    public static final int SORT_DESCEND = -1;
    private static FileComparision fileComparision = new FileComparision();

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void sortBy(List<FileBean> files, int sortBy, int order) {
        fileComparision.setOrder(order);
        fileComparision.setSortby(sortBy);
        Collections.sort(files,fileComparision);
    }

    private static class FileComparision implements Comparator<FileBean> {

        private int sortby = SORT_BY_NAME;
        private int order = SORT_ASCEND;

        public void setOrder(int order) {
            this.order = order;
        }

        public void setSortby(int sortby) {
            this.sortby = sortby;
        }


        @Override
        public int compare(FileBean file1, FileBean file2) {
            if (file1 == null) {
                return 0;
            }
            if (file2 == null) {
                return 0;
            }
            int result = 0;
            if (sortby == SORT_BY_NAME) {
                result = file1.name.compareToIgnoreCase(file2.name) * order;
            } else if (sortby == SORT_BY_TIME) {
                String time1 = file1.createTime;
                String time2 = file2.createTime;
                try {
                    Date d2 = dateFormat.parse(time2);
                    Date d1 = dateFormat.parse(time1);
                    result = d1.compareTo(d2);
                } catch (ParseException e) {
                    e.printStackTrace();
                    result = 0;
                }
                result = result * order;
            } else if (sortby == SORT_BY_SIZE) {
                long size1 = file1.size;
                long size2 = file2.size;
                if (size1 > size2) {
                    result = 1;
                } else if (size1 < size2) {
                    return -1;
                } else {
                    result = 0;
                }
                result *= order;
            } else if (sortby == SORT_BY_TYPE) {
                if (file1.directory && !file2.directory) {
                    result = 1;
                } else if (!file1.directory && file2.directory) {
                    result = -1;
                } else if (file1.directory && file2.directory) {
                    result = 0;
                } else {
                    String[] name1 = file1.name.split(".");
                    String[] name2 = file2.name.split(".");
                    String type1 = "***";
                    String type2 = "***";
                    if (name1.length > 1) {
                        type1 = name1[name1.length - 1];
                    }
                    if (name2.length > 1) {
                        type2 = name2[name2.length - 1];
                    }
                    int temp = type1.compareToIgnoreCase(type2);
                    if (temp == 0) {
                        result = file1.name.compareToIgnoreCase(file2.name);
                    } else {
                        result = temp;
                    }
                }
                result *= order;
            }
            return result;
        }
    }
}
