package cn.tjad.tjad2.utils;

import android.content.Context;
import android.content.SharedPreferences;

import cn.tjad.tjad2.application.TjadApplication;

public class SpUtils {

    private SharedPreferences sp;

    public SpUtils(String fileName) {
        sp = TjadApplication.getInstance().getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    public void putString(String key, String value) {
        sp.edit().putString(key, value).apply();
    }

    public void putInt(String key, int value) {
        sp.edit().putInt(key, value).apply();
    }

    public void putBoolean(String key, boolean value) {
        sp.edit().putBoolean(key, value).apply();
    }

    public String getString(String key) {
        return sp.getString(key, null);
    }

    public int getInt(String key, int defaultValue) {
        return sp.getInt(key, defaultValue);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sp.getBoolean(key, defaultValue);
    }

    public void remove(String key) {
        sp.edit().remove(key).apply();
    }
}
