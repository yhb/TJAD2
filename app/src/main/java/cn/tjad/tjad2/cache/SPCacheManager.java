package cn.tjad.tjad2.cache;


import cn.tjad.tjad2.utils.SpUtils;

public class SPCacheManager {

    private SpUtils spUtils;

    public SPCacheManager() {
        spUtils = new SpUtils("tjad_cache");
    }

    public void putString(String key, String value) {
        spUtils.putString(key, value);
    }

    public void putInt(String key, int value) {
        spUtils.putInt(key, value);
    }

    public void putBoolean(String key, boolean value) {
        spUtils.putBoolean(key, value);
    }

    public String getString(String key) {
        return spUtils.getString(key);
    }

    public int getInt(String key, int defaultValue) {
        return spUtils.getInt(key, defaultValue);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return spUtils.getBoolean(key, defaultValue);
    }

    public void remove(String key) {

    }
}
