package cn.tjad.tjad2.route;

public interface PageId {

    String GROUP = "/tjad";

    String LOGIN = GROUP + "/login";
    String MAIN = GROUP + "/main";
    String PROJECT_LIST = GROUP + "/projectList";
    String WEBVIEW = GROUP + "/webview";
    String LAND_WEBVIEW = GROUP + "/landWebView";
    String FILE_TRANSFER = GROUP + "/fileTransfer";
    String MEMBER_DEPARTMENT = "/member/department";
    String MODEL_PAGE = GROUP + "/model";
    String MEMBER_USER_DETAIL = "/member/userDetail";
    String DOC_LINK = GROUP + "/docLink";
    String FILE_SELECT = GROUP + "/fileSelect";
    String DOC_PERMISSION = GROUP + "/docPermission";
    String PERMISSION_UPDATE = GROUP + "/permissionUpdate";
    String PERMISSION_BODY_SELECT = GROUP + "/permissionBodySelect";
    String USER_SELECT = GROUP + "/userSelect";
    String OFFLINE_LIST = GROUP + "/offlineList";
    String USER_LIST = GROUP + "/userList";
    String MODIFY_PASSWORD = GROUP + "/modifyPassword";
    String CONSTRUCTION_DETAIL = GROUP + "/constructionDetail";
    String DESIGN_DETAIL = GROUP + "/designDetail";
    String USER_SELECT_2 = GROUP + "/userSelect2";
    String PICTURE_PREVIEW = GROUP + "/picturePreview"; //图片预览
    String DOC_HISTORY = GROUP + "/docHostory"; //文档历史版本

    String CONSTRUCTION_MENU = GROUP + "/constructionMenu";
    String CONSTRUCTION_LIST = GROUP + "/constructionList";
    String PLAN_LIST = GROUP + "/planList";
    String PLAN_TASK_LIST = GROUP + "/planTaskList";
    String PLAN_TASK_DETAIL = GROUP + "/planTaskDetail";
    String PLAN_TASK_EDIT = GROUP + "/planTaskEdit";

    String MESSAGE_MENU = GROUP + "/messageMenu";
    String FRAGMENT_CONTAINER_ACTIVITY = GROUP + "/fragmentContainerActivity";
    String MESSAGE_LIST = GROUP + "/messageList";
    String MESSAGE_SETTINGS = GROUP + "/messageSettings";
    String MESSAGE_SETTING_MENU = GROUP + "/messageSettingMenu";

    String MATERIAL_TAB = GROUP + "/materialTab";
    String MATERIAL_TRAKING = GROUP + "/materialTracking";
    String MATERIAL_DETAIL = GROUP + "/materialDetail";

    String FORM_TAB = GROUP + "/formTab";
    String FORM_DETAIL = GROUP + "/formDetail";
    String FORM_HANDLE = GROUP + "/formHandle";
    String FORM_REJECT = GROUP + "/formReject";
    String FORM_CREATE = GROUP + "/formCreate";
}
