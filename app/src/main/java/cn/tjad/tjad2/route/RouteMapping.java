package cn.tjad.tjad2.route;

import java.util.HashMap;
import java.util.Map;

public class RouteMapping {

    private static final Map<String, RouteElement> routeMapping = new HashMap<>();

    static {
        routeMapping.put(PageId.LOGIN, new RouteElement(PageId.LOGIN, "登录"));
        routeMapping.put(PageId.MAIN, new RouteElement(PageId.MAIN, null));
        routeMapping.put(PageId.PROJECT_LIST, new RouteElement(PageId.PROJECT_LIST, "项目列表"));
        routeMapping.put(PageId.WEBVIEW, new RouteElement(PageId.WEBVIEW, "TJAD"));
        routeMapping.put(PageId.FILE_TRANSFER, new RouteElement(PageId.FILE_TRANSFER,"传输列表"));
        routeMapping.put(PageId.LAND_WEBVIEW, new RouteElement(PageId.LAND_WEBVIEW, ""));
        routeMapping.put(PageId.MEMBER_DEPARTMENT, new RouteElement(PageId.MEMBER_DEPARTMENT, "部门"));
        routeMapping.put(PageId.MODEL_PAGE, new RouteElement(PageId.MODEL_PAGE, ""));
        routeMapping.put(PageId.MEMBER_USER_DETAIL, new RouteElement(PageId.MEMBER_USER_DETAIL, "用户信息"));
        routeMapping.put(PageId.DOC_LINK, new RouteElement(PageId.DOC_LINK,  "文件链接"));
        routeMapping.put(PageId.FILE_SELECT, new RouteElement(PageId.FILE_SELECT, "选择文件"));
        routeMapping.put(PageId.DOC_PERMISSION, new RouteElement(PageId.DOC_PERMISSION, "文件夹权限"));
        routeMapping.put(PageId.PERMISSION_UPDATE, new RouteElement(PageId.PERMISSION_UPDATE, "文件夹权限"));
        routeMapping.put(PageId.PERMISSION_BODY_SELECT, new RouteElement(PageId.PERMISSION_BODY_SELECT, "权限主体选择"));
        routeMapping.put(PageId.USER_SELECT, new RouteElement(PageId.USER_SELECT, "人员选择"));
        routeMapping.put(PageId.OFFLINE_LIST, new RouteElement(PageId.OFFLINE_LIST, "离线模型"));
        routeMapping.put(PageId.USER_LIST, new RouteElement(PageId.USER_LIST, "人员"));
        routeMapping.put(PageId.MODIFY_PASSWORD, new RouteElement(PageId.MODIFY_PASSWORD, "修改密码"));
        routeMapping.put(PageId.CONSTRUCTION_DETAIL, new RouteElement(PageId.CONSTRUCTION_DETAIL, "施工指导"));
        routeMapping.put(PageId.DESIGN_DETAIL, new RouteElement(PageId.DESIGN_DETAIL, "设计详情"));
        routeMapping.put(PageId.USER_SELECT_2, new RouteElement(PageId.USER_SELECT_2, "选择成员"));
        routeMapping.put(PageId.PICTURE_PREVIEW, new RouteElement(PageId.PICTURE_PREVIEW, "图片预览"));
    }

    public static RouteElement getRouteEliment(String pageId) {
        return routeMapping.get(pageId);
    }
}
