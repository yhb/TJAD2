package cn.tjad.tjad2.route;

public class RouteElement {

    private String pageId;
    private String title;
    private boolean needLogin;

    public RouteElement(String pageId, String title) {
        this.pageId = pageId;
        this.title = title;
        this.needLogin = false;
    }

    public RouteElement(String pageId, String title, boolean needLogin) {
        this.pageId = pageId;
        this.title = title;
        this.needLogin = needLogin;
    }

    public String getTitle() {
        return title;
    }
}
