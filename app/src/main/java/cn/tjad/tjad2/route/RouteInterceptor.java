package cn.tjad.tjad2.route;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;

@Interceptor(priority = 8, name = "interceptor")
public class RouteInterceptor implements IInterceptor {
    @Override
    public void process(Postcard postcard, InterceptorCallback callback) {
        String path = postcard.getPath();
        RouteElement element = RouteMapping.getRouteEliment(path);
        if (element != null) {
            postcard.withString("title", element.getTitle());
        }
        callback.onContinue(postcard);
    }

    @Override
    public void init(Context context) {

    }
}
