package cn.tjad.tjad2.application;

import android.support.multidex.MultiDexApplication;

import com.alibaba.android.arouter.launcher.ARouter;
import com.mob.MobSDK;
import com.qiandian.android.base.ActivityRecords;
import com.tencent.bugly.crashreport.CrashReport;

import cn.jpush.android.api.JPushInterface;
import cn.tjad.tjad2.BuildConfig;
import cn.tjad.tjad2.cache.SPCacheManager;
import cn.tjad.tjad2.data.session.Session;
import cn.tjad.tjad2.net.filedownload.FileDownloader;
import cn.tjad.tjad2.net.fileupload.FileUploader;

public class TjadApplication extends MultiDexApplication {

    private static TjadApplication instance;
    private Session session;
    private ActivityRecords activityRecords;

    private SPCacheManager spCacheManager;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        JPushInterface.setDebugMode(BuildConfig.DEBUG);
        JPushInterface.init(this);
        CrashReport.initCrashReport(this, "d11df060ec", BuildConfig.DEBUG);
        spCacheManager = new SPCacheManager();
        activityRecords = new ActivityRecords();
        registerActivityLifecycleCallbacks(activityRecords);
        if (BuildConfig.DEBUG) {
            ARouter.openLog();
            ARouter.openDebug();
        }
        ARouter.init(this);
        FileUploader.getInstance().init(this);
        FileDownloader.getInstance().init(this);
        MobSDK.init(this);
//        QbSdk.initX5Environment(this, null);
    }


    public static TjadApplication getInstance() {
        return instance;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public SPCacheManager getSpCacheManager() {
        return spCacheManager;
    }

    public ActivityRecords getActivityRecords() {
        return activityRecords;
    }
}
