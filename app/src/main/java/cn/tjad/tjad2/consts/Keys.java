package cn.tjad.tjad2.consts;

public interface Keys {

    String KEEP_PASSWORD = "keepPwd"; //是否保存密码
    String AUTO_LOGIN = "autoLogin"; // 自动登录
    String USER_ACCOUNT = "userAccount"; //用户名
    String PASSWORD = "password"; //密码
    String FIRST_RUN = "isFirstRun"; // 是否首次运行
    String SESSION = "session";

    String SETTING_TRANSFER_WITH_WIFI = "transferWithWifi";

    String FILE = "file";
    String FILE_ID = "fileId";
    String TITLE = "title";
    String PERMISSION = "permission";
    String PERMISSION_TYPE = "permissionType";
    String BODY = "body";
    String CHOICE_MODE = "choiceMode";
    String SELECTED_RESULTS = "selectedResults";
    String CONSTRUCTION_ID = "constructionId";
    String DESIGN_ID = "designId";

    String PICTURE_URL_LIST = "imageUrlList";

    String PAGE_ID = "pageId";
    String PAGE_ARGS = "pageArgs";
    String PAGE_MEMU = "pageMenu";
    String PLAN_TASKS = "planTasks";
    String PLAN_TASK = "planTask";
    String TASK_ITEM = "taskItem";
    String PLAN = "plan";

    String FRAGMENT_ID = "fragmentId";

    String MESSAGE_TYPE = "messageType";
    String MESSAGE_TYPE_NAME = "messageTypeName";

    String MATERIAL_ID = "materialId";

    String FORM_ID = "formId";
    String FORM_DETAIL_STEPS = "formDetailSteps";

    String SUFFIX = "suffix";
}
