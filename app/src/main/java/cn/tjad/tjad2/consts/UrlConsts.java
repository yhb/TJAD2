package cn.tjad.tjad2.consts;

public interface UrlConsts {

    String HOST = "http://bim.tjad.cn";

    String URL_PREFIX = HOST + "/tj1/";

    /**上传图片 POST
     * body:
     * ["base64", "base64", ...]
     * 返回值为["**.jpg", "**.jpg", ...]
     */
    String UPLOAD_IMAGE = URL_PREFIX + "upload/image";

    /**
     * 登录 POST
     * {“userName”:”xxx”, “password”:”xxx”}
     * 返回值中带有token字段，需要放到后续http请求的Authorization header中
     * Example :   Authorization: bearer TOKEN
     * 如后续请求中返回错误码  1002，则说明token已失效，需客户端主动重新注册
     */
    String USER_AUTH = URL_PREFIX + "users/auth";

    //当前登录用户的信息
    String USER_INFO = URL_PREFIX + "users/me";

    /**
     * 注册 POST
     * {“name”:”xxx”, “password”:”xxx”,”phone”:”xxx”, “email”:”xxx”, “avatar”:”xxx”}
     */
    String USER_REGISTER = URL_PREFIX + "users";

    /**
     * 修改个人信息 PUT
     * {“name”:”xxx”,”phone”:”xxx”, “email”:”xxx”, “avatar”:”xxx”,”company”:{“id”:1}, “department”:{“id”:2}}
     */
    String USER_INFO_MODIFY = URL_PREFIX + "users/$*$";

    /**
     * 修改密码
     */
    String MODIFY_PASSWORD = URL_PREFIX + "users/password";

    /**
     * 项目列表 GET
     */
    String PROJECT_LIST = URL_PREFIX + "projects?start=$*$&limit=$*$";

    /**
     * 文件列表
     * type  0=项目文档  1=我的文档  2=回收站
     *parentId  文件夹id， 如根目录则不传parentId
     *search  搜索字段
     */
    String FILE_LIST = URL_PREFIX + "projects/$*$/files?parentId=$*$&type=$*$&search=$*$";

    /**
     *新建文件夹 POST
     * {“mine”:true, “directory”:true, “name”:”xxx”, “parent”:{“id”:1}}
     * mine  false=项目文档  true=我的文档
     parent.id  父文件夹id， 如根目录则不传parentId
     */
    String CREATE_FOLDER = URL_PREFIX + "projects/$*$/files/";

    /**
     * 新建文件
     * file  <<file_content>>
     *mine true/false
     *parent.id  1
     *目前使用 multipart/form-data 上传文件
     *mine  false=项目文档  true=我的文档
     *parent.id  父文件夹id， 如根目录则不传parentId
     */
    String UPLOAD_FILE = URL_PREFIX + "projects/$*$/files/file/upload";

    /**
     * 全部成员架构
     */
    String MEMBER_LIST = URL_PREFIX + "projects/$*$/companies?showAll=true";

    /**
     * 离线文档下载链接
     */
    String DOCUMENT_DOWNLOAD_URL = HOST + ":8081" + "/offline/sj/$*$.zip";

    /**
     * 离线文件预览url
     */
    String LOCAL_DOCUMENT_PREVIEW_URL = "http://127.0.0.1:$*$/$*$/client.html#/home?projectId=$*$&token=$*$&path=$*$";

    /**
     * 在线文件预览Url
     */
    String REMOTE_DOCUMENT_PREVIEW_URL = "http://127.0.0.1:$*$/$*$/client.html#/home?projectId=$*$&token=$*$&documentId=$*$";


    /**
     * 删除文件
     * delete
     * body:[3, 4]文件id列表
     */
    String DELETE_FILE = URL_PREFIX + "projects/$*$/files";

    /**
     * 重命名 PUT
     */
    String RENAME_FILE = URL_PREFIX + "projects/$*$/files/$*$/rename?newName=$*$";

    /**
     * 移动到 PUT
     * 点击之后，显示树状文件夹，仍然是一层一层获取， 仅获取文件夹不获取文件加上参数 directory=true, 选中一个文件夹，点击移动
     * {parentId:1, ids:[3,4,5]}
     */
    String MOVE_FILE = URL_PREFIX + "projects/$*$/files/$*$/move";

    /**
     * models
     */
    String MODEL = URL_PREFIX + "projects/$*$/documents/selected";

    /**
     * 文件共享
     * body: {userIds:[1,2,3], ids:[3,4,5]}
     * userIds 是选中需要分享的人员id数组
     *ids是选中的当前需要分享的文件或者文件夹id的数组
     */
    String DOC_SHARE = URL_PREFIX + "projects/$*$/files/$*$/share";

    /**
     * 链接列表
     */
    String DOC_LINK = URL_PREFIX + "projects/$*$/documents/$*$/links";

    /**
     * 新增链接
     *
     * documentId是原始文件的file.document.id
     * 参数linkId是选中的file.id
     */
    String ADD_DOC_LINK = URL_PREFIX + "projects/$*$/documents/$*$/links?linkId=$*$";

    /**
     * 删除链接
     */
    String DELETE_DOC_LINK = URL_PREFIX + "projects/$*$/documents/$*$/links/$*$";

    /**
     * 权限列表
     * GET请求，返回一个permission列表
     *可能 有 company或者department或者user属性， 显示name
     */
    String PERMISSION_LIST = URL_PREFIX + "projects/$*$/files/$*$/permission";


    /**
     * 新增权限 POST
     * {“id”:1, “company”:{“id”:1}, “department”:{“id”:1}, “user”:{“id”:1}, view:true/false,
     *create:true/false, download:true/false, move:true/false, rename:true/false, setPermission:true/false }
     *如果是新增， 不传id， company department user 3选1
     *如果是修改， 传id， 不需要传company/department/user
     */
    String ADD_PERMISSION = URL_PREFIX + "projects/$*$/files/$*$/permission";

    /**
     * 删除权限 DELETE
     */
    String DELETE_PERMISSION = URL_PREFIX + "projects/$*$/files/$*$/permission/$*$";

    /**
     * 分享
     * {“encrypt”:true/false, “expireIn”:3/7/30/-1, “topic”:”abc”}
     * expireIn -1表示无限期
     返回url 和密码（如果需要加密）
     */
    String SHARE_CONTENT = URL_PREFIX + "projects/$*$/documents/$*$/share";

    /**
     * 施工列表 GET
     */
    String CONSTRUCTION_LIST = URL_PREFIX + "projects/$*$/constructions?start=$*$&limit=$*$";

    /**
     * 施工详情 GET
     */
    String CONSTRUCTION_DETAIL = URL_PREFIX + "projects/$*$/constructions/$*$";

    /**
     * 施工完成 POST
     */
    String CONSTRUCTION_FINISH = URL_PREFIX + "projects/$*$/constructions/$*$/finish";


    String[] RES_FILE_URL = {
            HOST + "/sj/client/forge/view.js",
            HOST + "/sj/client/forge/view.html"
    };

    //新增功能，改用retrofit
    //获取设计列表
    String DESIGN_LIST = "/tj1/projects/{projectId}/designquestions";

    //获取单个设计问题
    String DESIGN_DETAIL = "/tj1/projects/{projectId}/designquestions/{designId}";

    //回复设计问题
    String REPLY_DESIGN = "/tj1/projects/{projectId}/designquestions/{designId}/reply";

    //完成设计问题
    String FINISH_DESIGN = "/tj1/projects/{projectId}/designquestions/{designId}/finish";

    //查询设计类型
    String DESIGN_TYPE = "/tj1/projects/{projectId}/datas/?type=4";

    /**
     * 获取进度计划列表
     */
    String PLAN_LIST = "/tj1/projects/{projectId}/progress_plans";

    /**
     * 获取进度计划详情
     */
    String PLAN_DETAIL = "/tj1/projects/{projectId}/progress_plans/{id}";

    /**
     * 更新进度计划
     * 请求参数：
     *  {
     *   "id": 1,                           // 任务id
     *   "name": "name",                    // 任务名字
     *   "planStartTime": "2018-09-28",     //任务计划开始时间  必填
     *   "planEndTime": "2018-09-30",       //任务计划结束时间  必填， 需大于planStartTime
     *   "actualStartTime": "2018-07-30",   //任务实际开始时间  选填
     *   "actualEndTime": "2018-07-31"      //任务实际结束时间  选填，需大于actualStartTime
     * }
     */
    String UPDATE_PLAN_PROGRESS = "/tj1/projects/{projectId}/progress_plans/{id}/tasks";


    /**
     * 获取用户配置
     */
    String USER_SETTINGS = "/tj1/users/settings";

    /**
     * 修改用户消息配置
     * 请求参数
     *  {
     *   "type": 1,                //配置类型，根据获取配置的类型id获取
     *   "appPush": true,          //是否开启推送
     *   "email": false,           //app无此功能，需要按照原样传回
     *   "message": true,          //app无此功能，需要按照原样传回
     *   "popupWin": true,         //app无此功能，需要按照原样传回
     *   "sms": false              //app无此功能，需要按照原样传回
     * }
     */
    String UPDATE_SETTINGS = "/tj1/users/settings/message";

    /**
     * 获取消息列表
     * 参数:
     * 参数名	必选	类型	说明
     * type	否	int	消息类型 1=系统消息 2=操作日志 3=流程表单 4=进度计划
     * read	否	boolean	是否阅读
     */
    String MESSAGE_LIST = "/tj1/projects/{projectId}/messages";

    /**
     * 读取消息
     */
    String READ_MESSAGE = "/tj1/projects/{projectId}/messages/{id}/reads";

    /**
     * 获取消息总数
     * 请求参数
     * 参数名	必选	类型	说明
     * type	否	int	消息类型 1=系统消息 2=操作日志 3=流程表单 4=进度计划
     * read	否	boolean	是否阅读
     * 返回示例：
     * {"total":10}
     */
    String MESSAGE_COUNT = "/tj1/projects/{projectId}/messages/count";

    /**
     * 获取所有未读消息
     */
    String UNREAD_MESSAGE_COUNT = "/tj1/projects/{projectId}/messages/countall";

    /**
     * 获取文档版本历史
     */
    String DOC_HISTORY = "/tj1/projects/{projectId}/files/{fileId}/histories";

    /**
     * 获取人员信息
     */
    String USERS = "/tj1/projects/{projectId}/users";

    /**
     * 获取任务列表
     */
    String TASK_LIST = "/tj1/projects/{projectId}/progress_plans/{planId}/tasks/{taskId}";

    /**
     * 获取物料追踪列表
     * */
    String MATERIAL_TRACKING_LIST = "/tj1/projects/{projectId}/materialtraces";

    /**
     * 获取物料追踪详情
     */
    String MATERIAL_TRAKING_DETAIL = "/tj1/projects/{projectId}/materialtraces/{id}";

    /**
     * 更新物料追踪进度
     */
    String UPDATE_MATERIAL_TRAKING_PROGRESS = "/tj1/projects/{projectId}/materialtraces/{id}/items/forward";

    /**
     * 退回物料追踪进度
     */
    String BACK_MATERIAL_TRAKING_PROGRESS = "/tj1/projects/{projectId}/materialtraces/{id}/items/backward";

    /**
     * 物料指定责任人
     */
    String MATERIAL_ASIGN = "/tj1/projects/{projectId}/materialtraces/{id}/items/assign";

    /**
     * 获取自定义表单列表
     */
    String GET_FORM_LIST = "/tj1/projects/{projectId}/forms";

    /**
     * 获取自定义表单详情
     */
    String GET_FORM_DETAIL = "/tj1/projects/{projectId}/forms/{id}";

    /**
     * 创建自定义表单
     */
    String CREATE_FORM = "/tj1/projects/{projectId}/forms/";

    /**
     * 更新自定义表单
     */
    String UPDATE_FORM = "/tj1/projects/{projectId}/forms/{formId}/process";

    /**
     * 返回上一步
     */
    String FORM_REJECT = "/tj1/projects/{projectId}/forms/{formId}/reject";

    /**
     * 撤销自定义表单
     */
    String RECALL_FORM = "/tj1/projects/{projectId}/forms/{formId}/recall";

    /**
     * 重新分配自定义表单
     */
    String ASSIGN_FORM = "/tj1/projects/{projectId}/forms/{formId}/assign";

    /**
     * 模板查询
     */
    String CUSTOM_FORMS = "/tj1/projects/{projectId}/custom_forms";

    /**
     * 新建文件
     * file  <<file_content>>
     *mine true/false
     *parent.id  1
     *目前使用 multipart/form-data 上传文件
     *mine  false=项目文档  true=我的文档
     *parent.id  父文件夹id， 如根目录则不传parentId
     */
    String UPLOAD_FILE_URL = "/tj1/projects/{projectId}/files/file/upload";

    //文件列表查询.可以加上查询参数suffix来过滤文件类型
    String FILE_LIST_URL = "/tj1/projects/{projectId}/files/tree?parentId=0&type=0";

}
