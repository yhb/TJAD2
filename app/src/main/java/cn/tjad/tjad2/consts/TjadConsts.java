package cn.tjad.tjad2.consts;

import android.os.Environment;

public class TjadConsts {

    public static final String FILE_ROOT = Environment.getExternalStorageDirectory().getPath() + "/tjad";

    public static final String DOWNLOAD_FILE = FILE_ROOT + "/downloads";

    public static final String UNZIP_DOCUMENT = FILE_ROOT + "/documents";

    public static final String WEB = FILE_ROOT + "/web";

    public static final String TEMP = FILE_ROOT + "/temp";

    public static final String APPHTML = WEB + "/apphtml";

    public static final boolean TRANSFER_WITH_WIFI_DEFAULT = true;


}
