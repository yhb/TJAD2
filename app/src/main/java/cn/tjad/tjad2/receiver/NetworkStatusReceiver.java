package cn.tjad.tjad2.receiver;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;

import com.hjy.http.download.DownloadManager;
import com.hjy.http.download.FileDownloadTask;

import java.util.List;

import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.business.SettingManager;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.dialog.OnDialogClickListener;
import cn.tjad.tjad2.module.common.dialog.TjDialogFragment;


public class NetworkStatusReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
            NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (info != null) {
                if (NetworkInfo.State.CONNECTED == info.getState() && info.isAvailable()) {
                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                        Tools.showToast("连接上了wifi");
                        resumeDownloads();
                    } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                        Tools.showToast("连接上了移动网络");
                        boolean transferWithWifi = SettingManager.getTransferWithWifi();
                        if (hasUnfinishedDownloadTask()) {
                            if (transferWithWifi) {
                                Activity activity = TjadApplication.getInstance().getActivityRecords().getTopActivity();
                                if (activity != null) {
                                    TjDialogFragment.Builder builder = new TjDialogFragment.Builder(activity);
                                    builder.setTitle("提示")
                                            .setMessage("您当前连接到移动网络, 下载列表中有文件未下载完成, 是否继续下载?")
                                            .setPositiveButton("继续下载", new OnDialogClickListener() {
                                                @Override
                                                public boolean onClick(Dialog dialogInterface, int i) {
                                                    resumeDownloads();
                                                    return true;
                                                }
                                            })
                                            .setNegativeButton("取消", null)
                                            .show(((AppCompatActivity)activity).getSupportFragmentManager());
                                }
                            } else {
                                resumeDownloads();
                            }
                        }
                    } else {

                    }
                } else {
                    Tools.showToast("网络断开");
                }
            }
        }
    }

    private boolean hasUnfinishedDownloadTask() {
        List<FileDownloadTask> tasks = DownloadManager.getInstance().getmAllTaskList();
        if (tasks != null && tasks.size() > 0) {
            for (FileDownloadTask task : tasks) {
                if (task.getDownloadStatus() != FileDownloadTask.STATUS_DOWNLOAD_SUCCESS) {
                    return true;
                }
            }
        }
        return false;
    }

    private void resumeDownloads() {
        DownloadManager.getInstance().resumeTasks();
    }
}
