package cn.tjad.tjad2.business;

import com.hjy.http.upload.listener.OnUploadListener;
import com.hjy.http.upload.listener.OnUploadProgressListener;
import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.bean.CommonListBean;
import com.qiandian.android.base.common.utils.JsonUtils;
import com.qiandian.android.base.common.utils.UrlUtils;
import com.qiandian.android.base.net.http.HttpId;
import com.qiandian.android.base.net.http.HttpListenerImpl;
import com.qiandian.android.base.net.http.HttpRequest;
import com.qiandian.android.base.net.http.HttpUtils;
import com.qiandian.android.base.net.http.IDataCallBack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.tjad.tjad2.consts.UrlConsts;
import cn.tjad.tjad2.data.bean.Construction;
import cn.tjad.tjad2.data.bean.ConstructionDetail;
import cn.tjad.tjad2.data.bean.Document;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.data.bean.Link;
import cn.tjad.tjad2.data.bean.LoginBean;
import cn.tjad.tjad2.data.bean.Member;
import cn.tjad.tjad2.data.bean.PermissionBean;
import cn.tjad.tjad2.data.bean.Project;
import cn.tjad.tjad2.data.bean.ShareLink;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.net.fileupload.FileUploader;
import cn.tjad.tjad2.utils.FileHandler;

public class RemoteDataService {

    public static void login(String account, String password, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setResultBean(LoginBean.class);
        Map<String, Object> params = new HashMap<>();
        params.put("username", account);
        params.put("password", password);
        HttpRequest request = new HttpRequest(HttpId.LOGIN, UrlConsts.USER_AUTH, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }

    public static void register(String name, String password, String phone, String email, String avatar, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("password", password);
        params.put("phone", phone);
        params.put("email", email);
        params.put("avatar", avatar);
        HttpRequest request = new HttpRequest(HttpId.REGISTER, UrlConsts.USER_REGISTER, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }

    public static void modifyUserInfo(String name, String phone, String email, String avatar, int companyId, int departmentId, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("phone", phone);
        params.put("email", email);
        params.put("avatar", avatar);
        Map<String, Object> subParams = new HashMap<>();
        subParams.put("id", companyId);
        params.put("commpany", subParams);
        subParams = new HashMap<>();
        subParams.put("id", departmentId);
        params.put("department", subParams);
        HttpRequest request = new HttpRequest(HttpId.MODIFY_USER_INFO, UrlConsts.USER_INFO_MODIFY, params);
        HttpUtils.doHttpPut(request);
    }

    public static void getProjectList(IDataCallBack callBack, int start, int pageCount) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setResultBean(Project.class);
        httpListener.setRawType(CommonListBean.class);
        String url = UrlUtils.getUrl(UrlConsts.PROJECT_LIST, String.valueOf(start), String.valueOf(pageCount));
        HttpRequest request = new HttpRequest(HttpId.PROJECT_LIST, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    //directory参数为true, 只请求所以的文件夹， 不包含文件
    public static void getFileList(int projectId, int parentId, int type, String search, boolean directory, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setResultBean(FileBean.class);
        httpListener.setRawType(CommonBean.class);
        String parentIdStr = "";
        if (parentId > 0) {
            parentIdStr = String.valueOf(parentId);
        }
        String url = UrlUtils.getUrl(UrlConsts.FILE_LIST, String.valueOf(projectId), parentIdStr, String.valueOf(type), search);
        if (directory) {
            url += "&directory=true";
        }
        HttpRequest request = new HttpRequest(HttpId.FILE_LIST, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    public static void createFolder(int projectId, boolean mine, boolean directory, String name, int parentId, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setResultBean(BaseBean.class);
        httpListener.setRawType(CommonBean.class);
        String parentIdStr = "";
        if (parentId > 0) {
            parentIdStr = String.valueOf(parentId);
        }
        Map<String, Object> params = new HashMap<>();
        params.put("mine", mine);
        params.put("directory", directory);
        params.put("name", name);
        if (parentId != FileHandler.ROOT_ID) {
            params.put("parentId", parentId);
        }
        String url = UrlUtils.getUrl(UrlConsts.CREATE_FOLDER, String.valueOf(projectId));
        HttpRequest request = new HttpRequest(HttpId.CREATE_FOLDER, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }

//    public static void uploadFile(int projectId, boolean mine, int parentId, File file, IDataCallBack callBack) {
//        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
//        httpListener.setResultBean(BaseBean.class);
//        httpListener.setRawType(CommonBean.class);
//        Map<String, Object> params = new HashMap<>();
//        params.put("mine", mine);
//        params.put("parentId", parentId);
//        params.put("file", file);
//        String url = UrlUtils.getUrl(UrlConsts.UPLOAD_FILE, String.valueOf(projectId));
//        HttpRequest request = new HttpRequest(HttpId.CREATE_FILE, url, params);
//        request.setHttpCallBack(httpListener);
//        HttpUtils.postMultipartsRequest(request);
//    }

    public static void uploadFile(int projectId, String id, boolean mine, int parentId, String filePath, OnUploadListener uploadListener, OnUploadProgressListener progressListener) {
        String url = UrlUtils.getUrl(UrlConsts.UPLOAD_FILE, String.valueOf(projectId));
        Map<String, String> params = new HashMap<>();
        params.put("mine", String.valueOf(mine));
        if (parentId != FileHandler.ROOT_ID) {
            params.put("parentId", String.valueOf(parentId));
        }
        FileUploader.getInstance().uploadFile(params, id, filePath, null, url, uploadListener, progressListener);
    }

    public static void deleteFile(int projectId, List<Integer> ids, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        String url = UrlUtils.getUrl(UrlConsts.DELETE_FILE, String.valueOf(projectId));
        Map<String, Object> params = new HashMap();
        params.put("ids", ids);
        HttpRequest request = new HttpRequest(HttpId.DELETE_FILE, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpDelete(request);
    }

    public static void renameFile(int projectId, int fileId, String newName, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        String url = UrlUtils.getUrl(UrlConsts.RENAME_FILE, String.valueOf(projectId), String.valueOf(fileId), newName);
        Map<String, Object> params = new HashMap<>();
//        params.put("newName", newName);
        HttpRequest request = new HttpRequest(HttpId.RENAME_FILE, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPut(request);
    }

    public static void moveFile(int projectId, int parentId, List<Integer> docIds, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.MOVE_FILE, String.valueOf(projectId), "0");
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap();
        params.put("parentId", parentId);
        if (docIds != null && docIds.size() > 0) {
            ArrayList jsonArray = new ArrayList<>();
            for (int id : docIds) {
                jsonArray.add(id);
            }
            params.put("ids", jsonArray);
        }
        HttpRequest request = new HttpRequest(HttpId.MOVE_FILE, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPut(request);
    }

    public static void shareDocuments(int projectId, List<Integer> docIds, List<Integer> userIds, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.DOC_SHARE, String.valueOf(projectId), "0");
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap();
        if (docIds != null && docIds.size() > 0) {
            ArrayList jsonArray = new ArrayList<>();
            for (int id : docIds) {
                jsonArray.add(id);
            }
            params.put("ids", jsonArray);
        }
        if (userIds != null && userIds.size() > 0) {
            ArrayList jsonArray = new ArrayList<>();
            for (int id : userIds) {
                jsonArray.add(id);
            }
            params.put("userIds", jsonArray);
        }
        HttpRequest request = new HttpRequest(HttpId.DOC_SHARE, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }

    public static void getDocLink(int projectId, int documentId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.DOC_LINK, String.valueOf(projectId), String.valueOf(documentId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonListBean.class);
        httpListener.setResultBean(Link.class);
        HttpRequest request = new HttpRequest(HttpId.DOC_LINK, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    //linkId是fileId
    public static void addDocLink(int projectId, int documentId, int linkId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.ADD_DOC_LINK, String.valueOf(projectId), String.valueOf(documentId), String.valueOf(linkId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap<>();
        params.put("linkId", linkId);
        HttpRequest request = new HttpRequest(HttpId.ADD_DOC_LINK, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }

    public static void deleteDocLink(int projectId, int documentId, int linkId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.DELETE_DOC_LINK, String.valueOf(projectId), String.valueOf(documentId), String.valueOf(linkId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        HttpRequest request = new HttpRequest(HttpId.DELETE_DOC_LINK, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpDelete(request);
    }


    public static void getPermissionList(int projectId, int fileId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.PERMISSION_LIST, String.valueOf(projectId), String.valueOf(fileId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonListBean.class);
        httpListener.setResultBean(PermissionBean.class);
        HttpRequest request = new HttpRequest(HttpId.PERMISSION_LIST, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    //type取值:
    //"commpany"
    //"department"
    //"user"
    //typeId为对应的Id

    public static void addPermission(int projectId, int fileId, String type, int typeId,
                                     boolean view, boolean create, boolean download,
                                     boolean move, boolean rename, boolean setPermission,
                                     IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.ADD_PERMISSION, String.valueOf(projectId), String.valueOf(fileId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap<>();
        Map<String, Object> subParam = new HashMap<>();
        subParam.put("id", typeId);
        params.put(type, subParam);
        params.put("view", view);
        params.put("create", create);
        params.put("download", download);
        params.put("move", move);
        params.put("rename", rename);
        params.put("setPermission", setPermission);
        HttpRequest request = new HttpRequest(HttpId.ADD_PERMISSION, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }

    public static void updatePermission(int projectId, int fileId, int permissionId,
                                        boolean view, boolean create, boolean download,
                                        boolean move, boolean rename, boolean setPermission,
                                        IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.ADD_PERMISSION, String.valueOf(projectId), String.valueOf(fileId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap<>();
        params.put("id", permissionId);
        params.put("view", view);
        params.put("create", create);
        params.put("download", download);
        params.put("move", move);
        params.put("rename", rename);
        params.put("setPermission", setPermission);
        HttpRequest request = new HttpRequest(HttpId.UPDATE_PERMISSION, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }

    public static void deletePermission(int projectId, int fileId, int permissionId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.DELETE_PERMISSION, String.valueOf(projectId), String.valueOf(fileId), String.valueOf(permissionId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        HttpRequest request = new HttpRequest(HttpId.DELETE_PERMISSION, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpDelete(request);
    }

    public static void getModel(int projectId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.MODEL, String.valueOf(projectId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(Document.class);
        HttpRequest request = new HttpRequest(HttpId.MODEL, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    public static void getMemberList(int projectId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.MEMBER_LIST, String.valueOf(projectId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setResultBean(Member.class);
        httpListener.setRawType(CommonListBean.class);
        HttpRequest request = new HttpRequest(HttpId.MEMBER_LIST, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    public static void uploadImage(List<String> imageDataList, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonListBean.class);
        httpListener.setResultBean(String.class);
        Map<String, Object> params = new HashMap<>();
        params.put(HttpRequest.PARAM_LIST_KEY, JsonUtils.toJSONString(imageDataList));
        HttpRequest httpRequest = new HttpRequest(HttpId.UPLOAD_IMAGE, UrlConsts.UPLOAD_IMAGE, params);
        httpRequest.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(httpRequest);
    }

    public static void getShareData(int projectId, int documentId, boolean encrypt, int expireIn, String topic, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.SHARE_CONTENT, String.valueOf(projectId), String.valueOf(documentId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(ShareLink.class);
        Map<String, Object> params = new HashMap<>();
        params.put("encrypt", encrypt);
        params.put("expireIn", expireIn);
        params.put("topic", topic);
        HttpRequest httpRequest = new HttpRequest(HttpId.SHARE_CONTENT, url, params);
        httpRequest.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(httpRequest);
    }

    public static void getUserInfo(IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(User.class);
        HttpRequest request = new HttpRequest(HttpId.USER_INFO, UrlConsts.USER_INFO, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    public static void modifyPassword(String oldPwd, String newPwd, IDataCallBack callBack) {
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        Map<String, Object> params = new HashMap<>();
        params.put("oldPassword", oldPwd);
        params.put("password", newPwd);
        HttpRequest request = new HttpRequest(HttpId.MODIFY_PASSWORD, UrlConsts.MODIFY_PASSWORD, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPut(request);
    }

    //施工列表
    public static void getConstructionList(int projectId, int start, int limit, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.CONSTRUCTION_LIST, String.valueOf(projectId), String.valueOf(start), String.valueOf(limit));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonListBean.class);
        httpListener.setResultBean(Construction.class);
        HttpRequest request = new HttpRequest(HttpId.CONSTRUCTION_LIST, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    //施工详情
    public static void constructionDetail(int projectId, int constructionId, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.CONSTRUCTION_DETAIL, String.valueOf(projectId), String.valueOf(constructionId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(ConstructionDetail.class);
        HttpRequest request = new HttpRequest(HttpId.CONSTRUCTION_DETAIL, url, null);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpGet(request);
    }

    //施工完成
    public static void constructionFinish(int projectId, int constructionId, List<String> finishPhoto, int consStatus, IDataCallBack callBack) {
        String url = UrlUtils.getUrl(UrlConsts.CONSTRUCTION_FINISH, String.valueOf(projectId), String.valueOf(constructionId));
        HttpListenerImpl httpListener = new HttpListenerImpl(callBack);
        Map<String, Object> params = new HashMap<>();
        params.put("consStatus", consStatus);
        if (finishPhoto != null && finishPhoto.size() > 0) {
            params.put("finishPhotos", finishPhoto);
        }
        httpListener.setRawType(CommonBean.class);
        httpListener.setResultBean(BaseBean.class);
        HttpRequest request = new HttpRequest(HttpId.CONSTRUCTION_FINISH, url, params);
        request.setHttpCallBack(httpListener);
        HttpUtils.doHttpPost(request);
    }
}
