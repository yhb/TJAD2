package cn.tjad.tjad2.business;

import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.cache.SPCacheManager;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.consts.TjadConsts;

public class SettingManager {

    private static SPCacheManager cacheManager;

    static {
        cacheManager = TjadApplication.getInstance().getSpCacheManager();
    }

    public static boolean getTransferWithWifi() {
        return cacheManager.getBoolean(Keys.SETTING_TRANSFER_WITH_WIFI, TjadConsts.TRANSFER_WITH_WIFI_DEFAULT);
    }

    public static void setTransferWithWifi(boolean value) {
        cacheManager.putBoolean(Keys.SETTING_TRANSFER_WITH_WIFI, value);
    }
}
