package cn.tjad.tjad2.data.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class FormDetail extends FormBean {

    public ArrayList<FormDetailStep> steps;
    public ArrayList<FormDetailStep> nextSteps;

    public static class FormDetailStep extends FormBean.FormStep {
        public ArrayList<FormStepDetail> details;
        public User assignUser;
        public String description;
        public ArrayList<FileBean> rejectFileItems;
    }

    public static class FormStepDetail implements Serializable {
        public int id;
        public String value;
        public Detail detail;
        public ArrayList<User> userList;
        public ArrayList<FileBean> fileList;
        public Document document;
    }

    public static class Detail implements Serializable {
        public int id;
        public int control;
        public String name;
        public boolean mandatory;
        public int line; //app忽视
        public boolean textType;
        public String text;
        public int textPart1;
        public int textPart2;
        public int textPart3;
        public String text1;
        public String text2;
        public String text3;
        public String selection;
        public int attachFileNumber;
        public int userAutoSelectType;
        public int datePrecise;
        public int dateAutoFill;
    }
}
