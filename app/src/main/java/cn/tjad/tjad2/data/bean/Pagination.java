package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class Pagination extends BaseBean {
    public int start;
    public int limit;
    public int totalItems;
}
