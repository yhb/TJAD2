package cn.tjad.tjad2.data.event;

public class EditModeChangeEvent {

    private boolean isEditMode;

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
    }

    public boolean isEditMode() {
        return isEditMode;
    }
}
