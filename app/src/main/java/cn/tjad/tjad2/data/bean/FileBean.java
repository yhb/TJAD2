package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class FileBean extends BaseBean {
    public int id;
    public String name;
    public String path;
    public long size;
    public boolean directory;
    public int version;
    public User createUser;
    public boolean mine;
    public String createTime;
    public Permission permission;
    public Document document;
    public FileBean parent;
    public ArrayList<FileBean> children;
    public int parentId;

    /**
     * 如果是文件，directory为false, 没有parent和children字段
     * 如果是文件夹directory为true, 没有document字段
     */
}
