package cn.tjad.tjad2.data.bean;

import java.io.Serializable;

public class NoticeSettingElement implements Serializable {
    public int type;
    public String name;
    public boolean appPush;
    public boolean message;
    public boolean popupWin;
    public boolean email;
    public boolean sms;

    //本地标识用，非服务端返回字段
    public int noticeType;
    
    
    //根据noticeType返回是否开启
    public boolean isChecked() {
        boolean checked = false;
        if (noticeType == 1) {
            checked = appPush;
        } else if (noticeType == 2) {
            checked = email;
        } else if (noticeType == 3) {
            checked = sms;
        } else if (noticeType == 4) {
            checked = message;
        }
        return checked;
    }

    public void setChecked(boolean checked) {
        if (noticeType == 1) {
            appPush = checked;
        } else if (noticeType == 2) {
            email =checked;
        } else if (noticeType == 3) {
            sms = checked;
        } else if (noticeType == 4) {
            message = checked;
        }
    }
}
