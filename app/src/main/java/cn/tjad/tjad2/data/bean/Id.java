package cn.tjad.tjad2.data.bean;

import java.io.Serializable;

public class Id implements Serializable {
    public int id;

    public Id() {}

    public Id(int id) {
        this.id = id;
    }
}
