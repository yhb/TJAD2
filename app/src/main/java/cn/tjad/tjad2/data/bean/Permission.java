package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class Permission extends BaseBean {
    public boolean view;
    public boolean create;
    public boolean download;
    public boolean move;
    public boolean rename;
    public boolean setPermission;
}
