package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class MessageBean extends BaseBean {
    public int id;
    public int type;
    public int subType;
    public int detailId;
    public boolean read;
    public String text;
    public String createTime;
}
