package cn.tjad.tjad2.data.event;

import cn.tjad.tjad2.data.bean.Link;

public class DocLinkDeleteEvent {
    //实际应该是LinkBean, 但是现在link返回和file相同
    private Link linkBean;

    public Link getLinkBean() {
        return linkBean;
    }

    public void setLinkBean(Link linkBean) {
        this.linkBean = linkBean;
    }
}
