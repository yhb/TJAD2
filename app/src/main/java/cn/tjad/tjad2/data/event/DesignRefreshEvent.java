package cn.tjad.tjad2.data.event;

//此类用来给设计页面的两个tab相互发送事件
public class DesignRefreshEvent {

    private int fromType;

    public DesignRefreshEvent(int fromType) {
        this.fromType = fromType;
    }

    public int getFromType() {
        return fromType;
    }
}
