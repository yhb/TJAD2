package cn.tjad.tjad2.data.event;

import cn.tjad.tjad2.data.bean.FileBean;

public class FileCheckedChangeEvent {

    private boolean checked;
    private FileBean fileBean;

    public void setFileBean(FileBean fileBean) {
        this.fileBean = fileBean;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public FileBean getFileBean() {
        return fileBean;
    }

    public boolean isChecked() {
        return checked;
    }
}
