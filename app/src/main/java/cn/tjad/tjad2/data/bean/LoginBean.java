package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.utils.JsonUtils;

/**
 * Created by yanghongbing on 17/10/18.
 */

public class LoginBean extends BaseBean {
    public int id;
    public String phone;
    public String name;
    public String email;
    public boolean admin;
    public Department department;
    public Token token;

    @Override
    public String toString() {
        return JsonUtils.toJSONString(this);
    }
}
