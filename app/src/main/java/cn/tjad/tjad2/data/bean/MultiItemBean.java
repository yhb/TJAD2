package cn.tjad.tjad2.data.bean;

public class MultiItemBean<T> {
    private int type;
    private T data;

    public MultiItemBean(int type, T data) {
        this.type = type;
        this.data = data;
    }

    public int getType() {
        return type;
    }

    public T getData() {
        return data;
    }
}
