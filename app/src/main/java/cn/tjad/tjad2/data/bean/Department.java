package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class Department extends BaseBean {
    public int id;
    public String name;
    public User admin;
    public int personNumber;
    public String responsibility;
    public String area;
    public ArrayList<User> userList;
}
