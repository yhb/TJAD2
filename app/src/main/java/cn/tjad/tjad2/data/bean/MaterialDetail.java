package cn.tjad.tjad2.data.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class MaterialDetail extends MaterialBean {

    public ArrayList<FileBean> relateFiles;
    public ArrayList<Step> steps;
    public ArrayList<Item> items;

    public static class Step implements Serializable {
        public int id;
        public Data data;
        public Company company;
        public Department department;
        public boolean createQR;

        public User user;
    }

    public static class Data implements Serializable {
        public int id;
        public int type;
        public String name;
        public String color;
    }

    public static class Item implements Serializable {
        public int id;
        public String name;
        public String code;
        public int modelId;
        public String dbid;
        public Step step;
        public Step nextStep;
        public boolean finished;
        public String modifyTime;
        public User resposibleUser;

        //辅助字段
        public transient boolean isChecked;
    }
}
