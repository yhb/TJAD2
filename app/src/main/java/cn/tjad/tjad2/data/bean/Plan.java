package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class Plan extends BaseBean {
    public int id;
    public String name;
    public User createUser;
    public ArrayList<FileBean> fileItems;
    public String createTime;
    public int status;
}
