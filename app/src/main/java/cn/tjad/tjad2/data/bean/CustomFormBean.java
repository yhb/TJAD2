package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class CustomFormBean extends BaseBean {
    public int id;
    public String name;
    public User createUser;
    public boolean published;
    public String menu;
    public boolean modelRelated;
    public String createTime;
}
