package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class MaterialBean extends BaseBean {
    public int id;
    public String name;
    public User createUser;
    public String createTime;
    public String modifyTime;
    public ArrayList<String> relateFileNames;
    public ArrayList<Detail> details;

    public static class Detail {
        public String name;
        public int number;
        public float percentage;
    }

}
