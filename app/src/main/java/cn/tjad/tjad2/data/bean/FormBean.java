package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.io.Serializable;

public class FormBean extends BaseBean {
    public int id;
    public String name;
    public int consStatus;
    public String createTime;
    public String modifyTime;
    public User createUser;
    public User currentUser;
    public FormStep step;

    public static class FormStep implements Serializable {
        public int id;
        public User createUser;
        public Step step;
        public int type;
        public int rejectType;

//        //作为入参时有的参数
//        public int consStatus;
    }

    public static class Step implements Serializable {
        public int id;
        public String name;
        public int participant;
    }
}
