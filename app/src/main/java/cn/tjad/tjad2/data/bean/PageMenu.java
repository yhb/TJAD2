package cn.tjad.tjad2.data.bean;

import java.io.Serializable;

public class PageMenu implements Serializable {
    public int id;
    public String pageId;
    public String name;
}
