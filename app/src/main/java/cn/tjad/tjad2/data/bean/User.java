package cn.tjad.tjad2.data.bean;


public class User extends CommonEntity {
//    public int id;
    public String phone;
//    public String name;
    public String email;
    public String avatar;
    public boolean admin;
    public Company company;
    public Department department;

    //权限里面多出的字段
    public String area;
    public String title;

    //辅助字段
    public transient boolean isChecked;
}
