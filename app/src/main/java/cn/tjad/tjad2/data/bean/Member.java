package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class Member extends BaseBean {

    public int id;
    public String name;
    public String property;
    public String area;
    public int personNumber;
    public User admin;
    public ArrayList<Department> departmentList;

}
