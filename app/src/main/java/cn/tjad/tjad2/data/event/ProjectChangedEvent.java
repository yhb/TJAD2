package cn.tjad.tjad2.data.event;

public class ProjectChangedEvent {

    private int projectId;

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getProjectId() {
        return projectId;
    }
}
