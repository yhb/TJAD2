package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class ShareLink extends BaseBean {
    public int id;
    public boolean encrypt;
    public String password;
    public int expireIn;
    public String topic;
    public String url;
}
