package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class Document extends BaseBean {
    public int id;
    public String name;
    public String urn;
    public String filePath;
    public String forgeStatus;
    public String createTime;
    public String percentage;
    public ArrayList<String> models; //😳确认List中的类型
    public boolean selected;
}
