package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class Token extends BaseBean {
    public String token;
    public long expires;
}
