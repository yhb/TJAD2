package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class DesignItem extends BaseBean {
    public int id;
    public User createUser;
    public String code;
    public Floor floor;
    public Area area;
    public Type type;
    public ArrayList<String> domains;
    public Document document;
    public String position;
    public int priority;
    public String planFinishTime;
    public String content;
    public int consStatus;
    public ArrayList<String> finishPhotos;
    public String createTime;
    public boolean allowFinish;
    public String qrUrl;
    public String snapshot;
}
