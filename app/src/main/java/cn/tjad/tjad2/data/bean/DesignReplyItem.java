package cn.tjad.tjad2.data.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class DesignReplyItem implements Serializable {
    public int id;
    public User createUser;
    public String content;
    public ArrayList<String> photos;
    public String createTime;
    public ArrayList<User> relateUsers;
}
