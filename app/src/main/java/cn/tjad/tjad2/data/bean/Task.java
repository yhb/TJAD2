package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class Task extends BaseBean {
    public int id;
    public String name;
    public String planStartTime;
    public String planEndTime;
    public String actualStartTime;
    public String actualEndTime;
    public int level;
    public int planDay;
    public int actualDay;
    public int status;
    public boolean delay;
    public ArrayList<PlanTaskRelate> relates;
    public ArrayList<Task> children;
    public User user;
}
