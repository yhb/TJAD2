package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class Floor extends BaseBean {
    public int id;
    public String name;
    public String path;
    public String height;
    public Document document;
}
