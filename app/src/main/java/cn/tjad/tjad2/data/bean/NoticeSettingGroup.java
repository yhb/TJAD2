package cn.tjad.tjad2.data.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class NoticeSettingGroup implements Serializable {
    public String name;
    public int type;
    public int unread;
    public ArrayList<NoticeSettingElement> settings;

    //本地标识用，非服务端返回字段
    public int noticeType;
}
