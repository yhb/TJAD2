package cn.tjad.tjad2.data.bean;

import android.support.annotation.NonNull;

import com.qiandian.android.base.common.bean.BaseBean;

public class CommonEntity extends BaseBean {

    public int id;
    public String name;

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
