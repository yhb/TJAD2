package cn.tjad.tjad2.data.event;

public class PlanTaskEditEvent {
    private boolean taskEdit;

    public PlanTaskEditEvent(boolean taskEdit) {
        this.taskEdit = taskEdit;
    }

    public boolean isTaskEdit() {
        return taskEdit;
    }

    public void setTaskEdit(boolean taskEdit) {
        this.taskEdit = taskEdit;
    }
}
