package cn.tjad.tjad2.data.event;

public class SimpleEvent {
    private int id;

    public SimpleEvent(int id) {
        this.id = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
