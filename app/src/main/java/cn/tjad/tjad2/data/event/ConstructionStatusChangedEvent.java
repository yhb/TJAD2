package cn.tjad.tjad2.data.event;

public class ConstructionStatusChangedEvent {
    private boolean changed;

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public boolean isChanged() {
        return changed;
    }
}
