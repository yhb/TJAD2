package cn.tjad.tjad2.data.session;

import cn.tjad.tjad2.data.bean.LoginBean;
import cn.tjad.tjad2.data.bean.Project;
import cn.tjad.tjad2.data.bean.User;

public class Session {
    private LoginBean loginInfo;

    private Project project;
    private User userInfo;

    private String account;
    private String password;

    public void setLoginInfo(LoginBean loginInfo) {
        this.loginInfo = loginInfo;
    }

    public LoginBean getLoginInfo() {
        return loginInfo;
    }

    public int getProjectId() {
        if (project != null) {
            return project.id;
        }
        return -1;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Project getProject() {
        return project;
    }

    public String getToken() {
        if (loginInfo != null && loginInfo.token != null) {
            return loginInfo.token.token;
        }
        return null;
    }

    public User getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(User userInfo) {
        this.userInfo = userInfo;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
