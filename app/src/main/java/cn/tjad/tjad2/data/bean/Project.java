package cn.tjad.tjad2.data.bean;

import java.util.ArrayList;

public class Project {
    public int id;
    public String name;
    public String description;
    public ArrayList<String> picture;
    public User admin;
}
