package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class Construction extends BaseBean {
    public int id;
    public User createUser;
    public String code;
    public Floor floor;
    public Type type;
    public Document document;
    public String content;
    public int consStatus;
    public ArrayList<String> finishPhotos;
    public String createTime;
    public String viewState;
    public boolean allowFinish;
}
