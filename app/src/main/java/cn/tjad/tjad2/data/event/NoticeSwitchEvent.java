package cn.tjad.tjad2.data.event;

import cn.tjad.tjad2.data.bean.NoticeSettingElement;

//消息设置开关事件
public class NoticeSwitchEvent {
    private int position;
    private NoticeSettingElement noticeSettingElement;
    private boolean isChecked;

    public void setNoticeSettingElement(NoticeSettingElement noticeSettingElement) {
        this.noticeSettingElement = noticeSettingElement;
    }

    public NoticeSettingElement getNoticeSettingElement() {
        return noticeSettingElement;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChecked() {
        return isChecked;
    }
}
