package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

import java.util.ArrayList;

public class DesignDetail extends BaseBean {
    public int id;
    public User createUser;
    public String code;
    public Floor floor;
    public Area area;
    public Type type;
    public ArrayList<String> domains;
    public Document document;
    public String position;
    public int priority;
    public String planFinishTime;
    public ArrayList<User> relateUsers;
    public String content;
    public int consStatus;
    public User finishUser;
    public String finishTime;
    public String finishContent;
    public ArrayList<String> finishPhotos;
    public ArrayList<Attach> attaches;
    public String createTime;
    public String viewState;
    public boolean allowFinish;
    public String qrUrl;
    public ArrayList<DesignReplyItem> replies;
}
