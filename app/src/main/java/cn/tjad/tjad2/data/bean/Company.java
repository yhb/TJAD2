package cn.tjad.tjad2.data.bean;

import com.qiandian.android.base.common.bean.BaseBean;

public class Company extends BaseBean {
    public int id;
    public String name;
    public User admin;

    //权限里面多出来的字段
    public String property;
    public String area;
    public int personNumber;
}
