package cn.tjad.tjad2.module.form.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.widget.TJADTTitleView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.event.SimpleEvent;
import cn.tjad.tjad2.module.form.entity.RejectFormRequestBean;
import cn.tjad.tjad2.module.form.views.AttachmentSelectWidget;
import cn.tjad.tjad2.module.form.views.MultiLineTextView;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.ProgressDialogUtils;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.FORM_REJECT)
public class FormDetailRejectFragment extends BaseFragment {

    @BindView(R.id.form_reject_spinner)
    Spinner spinner;
    @BindView(R.id.form_reject_reason)
    MultiLineTextView reasonText;
    @BindView(R.id.form_reject_attachment)
    AttachmentSelectWidget attachmentSelectWidget;

    private int formId = -1;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.form_reject_fragment, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            formId = bundle.getInt(Keys.FORM_ID, -1);
        }
        if (formId == -1) {
            Tools.showToast("参数错误，可能会引起操作异常！");
            return;
        }
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("驳回表单");
    }

    @OnClick(R.id.form_reject_submit)
    public void onSubmit(View view) {
        doRejectAction();
    }

    private void doRejectAction() {
        ProgressDialogUtils.show(getContext());
        RejectFormRequestBean requestBean = new RejectFormRequestBean();
        requestBean.rejectType = spinner.getSelectedItemPosition();
        requestBean.description = reasonText.getValue();
        requestBean.rejectFileIds = attachmentSelectWidget.getValue();
        RetrofitManager.getInstance().getApiService()
                .rejectForm(Tools.getProjectId(), formId, requestBean)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<BaseBean>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<BaseBean> t) {
                        ProgressDialogUtils.dismiss();
                        new AlertDialog.Builder(getContext())
                                .setTitle("提示")
                                .setMessage("操作成功")
                                .setCancelable(false)
                                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        EventBus.getDefault().register(new SimpleEvent(7001));
                                        getActivity().finish();
                                    }
                                })
                                .show();

                    }

                    @Override
                    public void onFailed(String message) {
                        ProgressDialogUtils.dismiss();
                        Tools.showToast(message);
                    }
                });
    }
}
