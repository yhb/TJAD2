package cn.tjad.tjad2.module.document.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;
import com.qiandian.android.base.common.bean.CommonBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.document.adapter.FileListAdapter;
import cn.tjad.tjad2.module.document.widget.FileDirectoryIndicator;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.FILE_SELECT)
public class FileSelectActivity extends BaseActivity implements OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, FileDirectoryIndicator.OnIndicatorClickListener {

    @BindView(R.id.recyclerview)
    RecyclerView fileList;
    @BindView(R.id.file_dir_ind)
    FileDirectoryIndicator fileDirectoryIndicator;

    private FileListAdapter adapter;
    private List<FileBean> fileListData;
    private int projectId;

    private String suffix;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_select_layout);
        suffix = getIntent().getStringExtra(Keys.SUFFIX);
        if (suffix == null) {
            suffix = "";
        }
        init();
    }

    private void init() {
        projectId = TjadApplication.getInstance().getSession().getProjectId();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        fileDirectoryIndicator.setOnIndicatorClickListener(this);
        fileList.setLayoutManager(layoutManager);
        fileList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new FileListAdapter(null);
        adapter.setOnItemClickListener(this);
        fileList.setAdapter(adapter);
        requestFile();
    }

    private void requestFile() {
        RetrofitManager.getInstance().getApiService()
                .getFileList(Tools.getProjectId(), suffix)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<FileBean>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<FileBean> t) {
                        FileBean root = t.result;
                        fileListData = root.children;
                        adapter.updateData(fileListData);
                        adapter.notifyDataSetChanged();
                        root.directory = true;
                        root.name = "ROOT";
                        fileDirectoryIndicator.addFileDirectory(root);
                    }

                    @Override
                    public void onFailed(String message) {
                        Tools.showToast(message);
                    }
                });
//        if (fileHandler == null) {
//            fileHandler = new FileHandler(this);
//            fileHandler.requestFileList(projectId, fileHandler.getCurrentDisplayDirectoryId(), this);
//        }
    }

//    @Override
//    public void onFileListGet(List<FileBean> fileList) {
//        fileListData = fileList;
//        filterRvtFile(fileListData);
//        adapter.updateData(fileListData);
//        adapter.notifyDataSetChanged();
//        swipeRefreshLayout.setRefreshing(false);
//    }
//
//    @Override
//    public void onFileListFailed(String message) {
//        Tools.showToast(message);
//        swipeRefreshLayout.setRefreshing(false);
//    }
//
//    private void filterRvtFile(List<FileBean> fileList) {
//        if (fileList != null && fileList.size() > 0) {
//            Iterator<FileBean> iterator = fileList.iterator();
//            while (iterator.hasNext()) {
//                FileBean file = iterator.next();
//                if (!file.directory) {
//                    if (!file.name.endsWith(".rvt") && !file.name.endsWith(".RVT")) {
//                        iterator.remove();
//                    }
//                }
//            }
//        }
//    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        FileBean fileBean = adapter.getItemData(position);
        if (fileBean.directory) {
            if (fileBean.children == null) {
                fileBean.children = new ArrayList<>();
            }
            adapter.updateData(fileBean.children);
            adapter.notifyDataSetChanged();
            fileDirectoryIndicator.addFileDirectory(fileBean);
//            fileHandler.requestFileList(projectId, fileBean.id, this);
        } else {
            Intent intent = new Intent();
            intent.putExtra(Keys.FILE, fileBean);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (!fileDirectoryIndicator.goBack()) {
            super.onBackPressed();
        }
    }

    @Override
    public void onRefresh() {
        requestFile();
    }

    @Override
    public void onIndicatorClick(FileBean directory) {
        if (directory != null && directory.directory) {
            adapter.updateData(directory.children);
            adapter.notifyDataSetChanged();
        }
    }
}
