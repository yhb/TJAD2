package cn.tjad.tjad2.module.common.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.pictureselect.TJBaseAdapter;
import cn.tjad.tjad2.module.common.pictureselect.ViewHolder;

public class ImageDisplayAdapter extends TJBaseAdapter {

    public ImageDisplayAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public ViewHolder getViewHolder(int position) {
        return new ImageDisplayViewHolder(context);
    }

    public static class ImageDisplayViewHolder extends ViewHolder<String> {
        @BindView(R.id.image_item_image)
        ImageView imageView;
        @BindView(R.id.image_item_delete)
        ImageView deleteIV;
        public ImageDisplayViewHolder(Context context) {
            super(context);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setValue(String s, int position) {
            deleteIV.setVisibility(View.GONE);
            Glide.with(itemView.getContext())
                    .load(s)
                    .into(imageView);
        }

        @Override
        public int getLayoutId() {
            return R.layout.image_display_item;
        }
    }
}
