package cn.tjad.tjad2.module.user.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.qiandian.android.base.common.bean.CommonListBean;
import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.net.http.HttpConstants;
import com.qiandian.android.base.net.http.HttpId;
import com.qiandian.android.base.net.http.IDataCallBack;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.user.adapter.MemberListAdapter;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.business.RemoteDataService;
import cn.tjad.tjad2.data.bean.Department;
import cn.tjad.tjad2.data.bean.Member;
import cn.tjad.tjad2.data.session.Session;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.Tools;

public class MemberFragment extends BaseFragment implements IDataCallBack, OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private int projectId;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private MemberListAdapter adapter;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.common_recylerview, null);
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        titleView.setCenterText("成员");
        titleView.showLeft(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MemberListAdapter(null);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        adapter.setOnItemClickListener(this);
        Session session = TjadApplication.getInstance().getSession();
        if (session != null) {
            projectId = session.getProjectId();
        }
        requestMemberList();
    }

    private void requestMemberList() {
        RemoteDataService.getMemberList(projectId, this);
    }

    @Override
    public boolean onReceiveError(int httpId, int errorCode, String errorMsg) {
        if (httpId == HttpId.MEMBER_LIST) {
            Tools.showToast(errorMsg);
            swipeRefreshLayout.setRefreshing(false);
        }
        return false;
    }

    @Override
    public void onDataRefresh(int httpId, Object responseBean) {
        if (httpId == HttpId.MEMBER_LIST) {
            CommonListBean<Member> bean = (CommonListBean<Member>) responseBean;
            if (bean.resCode == HttpConstants.CODE_SUCCESS) {
                onMemberListGet(bean.result);
            } else {
                Tools.showToast(bean.respMsg);
            }
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void onMemberListGet(ArrayList<Member> result) {
        adapter.updateData(result);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        Member member = adapter.getItemData(position);
        ArrayList<Serializable> data = new ArrayList<>();
        if (member.departmentList != null) {
            for (Department dep : member.departmentList) {
                data.add(dep);
            }
        }
        if (member.admin != null) {
            data.add(member.admin);
        }
        Bundle bundle = null;
        if (member != null) {
            bundle = new Bundle();
            bundle.putSerializable("displayList", data);
        }
        RouteUtils.toPage(getContext(), PageId.MEMBER_DEPARTMENT, bundle);
    }

    @Override
    public void onRefresh() {
        requestMemberList();
    }
}
