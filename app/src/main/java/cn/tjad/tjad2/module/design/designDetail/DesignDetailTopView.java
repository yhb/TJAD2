package cn.tjad.tjad2.module.design.designDetail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.DesignDetail;
import cn.tjad.tjad2.utils.Tools;

public class DesignDetailTopView extends FrameLayout {
    @BindView(R.id.design_detail_code)
    TextView codeTV;
    @BindView(R.id.design_detail_type)
    TextView typeTV;
    @BindView(R.id.design_detail_belong_scope)
    TextView scopeTV;
    @BindView(R.id.design_detail_belong_field)
    TextView fieldTV;
    @BindView(R.id.design_detail_related_model)
    TextView relatedModelTV;
    @BindView(R.id.design_detail_floor)
    TextView floorTV;
    @BindView(R.id.design_detail_position)
    TextView positionTV;
    @BindView(R.id.design_detail_priority)
    TextView priorityTV;
    @BindView(R.id.design_detail_deadline)
    TextView deadLineTV;
    @BindView(R.id.design_detail_attach_view)
    AttachmentView attachmentView;
    @BindView(R.id.design_detail_content)
    TextView contentTV;
    @BindView(R.id.design_detail_member_list)
    MemberListView memberListView;

    public DesignDetailTopView(@NonNull Context context) {
        this(context, null);
    }

    public DesignDetailTopView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DesignDetailTopView( @NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.design_detail_head, this);
        ButterKnife.bind(this);
    }

    public void setData(DesignDetail data) {
        if (data == null) {
            return;
        }
        codeTV.setText(data.code);
        if (data.type != null) {
            typeTV.setText(data.type.name);
        } else {
            typeTV.setText("");
        }
        if (data.area != null) {
            scopeTV.setText(data.area.name);
        } else {
            scopeTV.setText("");
        }
        if (data.domains != null && data.domains.size() > 0) {
            String domainText = "";
            for (int i = 0; i < data.domains.size(); i++) {
                domainText += data.domains.get(i);
                if (i < data.domains.size() - 1) {
                    domainText += "  ";
                }
            }
            fieldTV.setText(domainText);
        } else {
            fieldTV.setText("");
        }
        if (data.document != null) {
            relatedModelTV.setText(data.document.name);
        } else {
            relatedModelTV.setText("");
        }
        if (data.floor != null) {
            floorTV.setText(data.floor.name);
        }
        positionTV.setText(data.position);
        priorityTV.setText(Tools.getDesignPriority(data.priority));
        deadLineTV.setText(data.planFinishTime);
        contentTV.setText(data.content);
        memberListView.setData(data.relateUsers);
        attachmentView.setAttachList(data.attaches);
    }
}
