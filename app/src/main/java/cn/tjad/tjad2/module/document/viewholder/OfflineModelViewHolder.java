package cn.tjad.tjad2.module.document.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class OfflineModelViewHolder extends SimpleBaseViewHolder<String> {

    @BindView(R.id.file_icon)
    ImageView iconIV;
    @BindView(R.id.file_name)
    TextView fileNameTV;

    public OfflineModelViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(String data, int position) {
        File file = new File(data);
        String fileName = file.getName();
        int icon = Tools.getFileIcon(fileName.substring(fileName.lastIndexOf(".") + 1));
        iconIV.setImageResource(icon);
        fileNameTV.setText(fileName);
    }
}
