package cn.tjad.tjad2.module.web;

import com.alibaba.android.arouter.facade.annotation.Route;

import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.LAND_WEBVIEW)
public class LandScapeWebViewActivity extends WebViewActivity {
}
