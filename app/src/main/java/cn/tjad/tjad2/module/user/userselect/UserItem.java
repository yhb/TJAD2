package cn.tjad.tjad2.module.user.userselect;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.baozi.treerecyclerview.base.ViewHolder;
import com.baozi.treerecyclerview.item.TreeItem;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.User;

public class UserItem extends TreeItem<User> {

    @Override
    public int getLayoutId() {
        return R.layout.user_select_item_user;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.setText(R.id.member_item_name, data.name);
        ImageView icon = viewHolder.getImageView(R.id.member_item_icon);
        if (!TextUtils.isEmpty(data.avatar)) {
            Glide.with(icon.getContext())
                    .load(data.avatar)
                    .into(icon);
        }
        if (SelectMode.getSelectMode() == SelectMode.MULTIPLE_CHOICE) {
            viewHolder.setVisible(R.id.rb_radio_check, false);
            viewHolder.setVisible(R.id.rb_check, true);
            viewHolder.setChecked(R.id.rb_check, data.isChecked);
        } else {
            viewHolder.setVisible(R.id.rb_radio_check, true);
            viewHolder.setVisible(R.id.rb_check, false);
            viewHolder.setChecked(R.id.rb_radio_check, data.isChecked);
        }
        viewHolder.setVisible(R.id.member_item_expand_ind, false);
    }

    @Override
    public void onClick(ViewHolder viewHolder) {
        if (SelectMode.getSelectMode() == SelectMode.MULTIPLE_CHOICE) {
            CheckBox checkBox = viewHolder.getView(R.id.rb_check);
            checkBox.toggle();
            data.isChecked = checkBox.isChecked();
        } else {
            RadioButton rb = viewHolder.getView(R.id.rb_radio_check);
            rb.setChecked(true);
            data.isChecked = true;
            EventBus.getDefault().post(new UserCheckedEvent(data));
        }
    }
}
