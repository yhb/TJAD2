package cn.tjad.tjad2.module.main.activity;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.qiandian.android.base.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.Tools;
import io.reactivex.Observable;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

public class SplashActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        if (EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            addSubscribe(Observable.just("")
                    .delay(1, TimeUnit.SECONDS)
                    .doOnComplete(() -> {
                        RouteUtils.toPage(SplashActivity.this, PageId.LOGIN, null);
                        finish();
                    })
                    .subscribe());
        } else {
            EasyPermissions.requestPermissions(
                    new PermissionRequest.Builder(this, 1001, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .build());
        }
    }

    @AfterPermissionGranted(1001)
    public void onPermissionFinished() {
        List<String> permissions = new ArrayList<>();
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            RouteUtils.toPage(SplashActivity.this, PageId.LOGIN, null);
            finish();
        } else if (EasyPermissions.somePermissionPermanentlyDenied(this, permissions)){
            Tools.showToast("您拒绝了该权限，可能引起APP无法正常使用，如果需要，请去设置中打开");
//            new AppSettingsDialog.Builder(this).build().show();
        } else {
            finish();
        }
    }




    @Override
    protected boolean showTitle() {
        return false;
    }
}
