package cn.tjad.tjad2.module.form.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import cn.tjad.tjad2.BuildConfig;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.module.design.designDetail.MemberListView;
import cn.tjad.tjad2.module.user.userselect.SelectMode;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;

public class UserSelectWidget extends FormBaseLayout implements View.OnClickListener {

    private MemberListView memberListView;

    public UserSelectWidget(Context context) {
        super(context);
    }

    public UserSelectWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void init() {
        memberListView = new MemberListView(getContext());
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        addView(memberListView, lp);
        this.setOnClickListener(this);
    }

    @Override
    public void setFormStepDetail(FormDetail.FormStepDetail formStepDetail) {
        super.setFormStepDetail(formStepDetail);
        setInitData(formStepDetail.userList);
    }

    private void setInitData(List<User> userList) {
        memberListView.setData(userList);
        notifyUserChanged();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }

    @Override
    public void onClick(View v) {
        SelectMode.setSelectMode(SelectMode.MULTIPLE_CHOICE);
        RouteUtils.toPageForResult((Activity) getContext(), PageId.USER_SELECT_2, null, 1006);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1006 && resultCode == Activity.RESULT_OK) {
            ArrayList<User> selectedUsers = (ArrayList<User>) data.getSerializableExtra(Keys.SELECTED_RESULTS);
            memberListView.setData(selectedUsers);
            notifyUserChanged();
        }
    }

    private void notifyUserChanged() {
        List<User> userList = memberListView.getData();
        String value = "";
        if (userList != null && userList.size() > 0) {
            for (int i = 0; i < userList.size(); i++) {
                value += userList.get(i).id;
                if (i < userList.size() -1) {
                    value += ",";
                }
            }
        }
        formStepDetail.value = value;
        if (onEditListener != null) {
            onEditListener.onEdit(formStepDetail.detail.id, value);
        }
        if (BuildConfig.DEBUG) {
            Log.d("TJAD", "用户列表：" + userList);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            setOnClickListener(this);
        } else {
            setOnClickListener(null);
        }
    }
}
