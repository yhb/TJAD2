package cn.tjad.tjad2.module.material.bean.request;

import java.io.Serializable;
import java.util.ArrayList;

import cn.tjad.tjad2.data.bean.Id;

public class ItemRequestBean implements Serializable {
    public ArrayList<Id> items;
}
