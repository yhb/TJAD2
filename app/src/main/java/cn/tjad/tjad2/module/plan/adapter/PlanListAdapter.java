package cn.tjad.tjad2.module.plan.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.Plan;
import cn.tjad.tjad2.module.plan.viewholder.PlanListViewHolder;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class PlanListAdapter extends SimpleRecyclerViewAdapter<Plan> {


    public PlanListAdapter(List<Plan> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<Plan> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.plan_list_item, parent, false);
        return new PlanListViewHolder(view);
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("暂无进度计划", 0);
    }
}
