package cn.tjad.tjad2.module.form.viewHolder;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.form.views.FormBaseLayout;
import cn.tjad.tjad2.module.form.views.ViewFactory;

public class FormDetailItemViewHolder extends SimpleBaseViewHolder<FormDetail.FormStepDetail> {
    @BindView(R.id.form_detail_item_name)
    TextView nameTV;
    @BindView(R.id.form_detail_item_must)
    TextView mustTV;
    @BindView(R.id.form_detail_item_content)
    FrameLayout contentFL;
    @BindView(R.id.form_detail_item_right_arrow)
    ImageView rightArrowIV;

    private boolean editable;
    private FormBaseLayout.IOnEditListener onEditListener;


    public FormDetailItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public void setOnEditListener(FormBaseLayout.IOnEditListener onEditListener) {
        this.onEditListener = onEditListener;
    }

    @Override
    public void setData(FormDetail.FormStepDetail data, int position) {
        if (data.detail != null) {
            nameTV.setText(data.detail.name);
            if (data.detail.mandatory) {
                mustTV.setVisibility(View.VISIBLE);
            } else {
                mustTV.setVisibility(View.GONE);
            }
            int id = -1;
            Object tag = nameTV.getTag();
            if (tag != null) {
                id = (int) tag;
            }
            if (id != data.detail.id) {
                FormBaseLayout contentView = (FormBaseLayout) ViewFactory.createView(itemView.getContext(), data);
                contentView.setEnabled(editable);
                contentView.setOnEditListener(onEditListener);
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) contentView.getLayoutParams();
                if (lp == null) {
                    lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }
                lp.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
                contentFL.removeAllViews();
                contentFL.addView(contentView, lp);
                nameTV.setTag(data.detail.id);
            } else {
                contentFL.getChildAt(0).setEnabled(editable);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contentFL.getChildAt(0) != null && contentFL.getChildAt(0).isEnabled()) {
                        contentFL.getChildAt(0).performClick();
                    }
                }
            });
        }
    }
}
