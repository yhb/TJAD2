package cn.tjad.tjad2.module.construction.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.Construction;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.construction.viewholder.ConstructionListViewHolder;

public class ConstructionListViewAdapter extends SimpleRecyclerViewAdapter<Construction> {

    public ConstructionListViewAdapter(List<Construction> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<Construction> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.construction_list_item, parent, false);
        return new ConstructionListViewHolder(view);
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("暂无施工数据", 0);
    }
}
