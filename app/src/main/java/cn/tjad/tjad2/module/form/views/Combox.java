package cn.tjad.tjad2.module.form.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.tjad.tjad2.BuildConfig;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormDetail;

public class Combox extends FormBaseLayout implements CompoundButton.OnCheckedChangeListener {


    public Combox(Context context) {
        super(context);
    }

    public Combox(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setFormStepDetail(FormDetail.FormStepDetail formStepDetail) {
        super.setFormStepDetail(formStepDetail);
        String[] checkedItems = null;
        if (formStepDetail.value != null) {
            checkedItems = formStepDetail.value.split(",");
        }
        Set<String> checkedList = new HashSet<>();
        if (checkedItems != null) {
            for (String s: checkedItems) {
                checkedList.add(s);
            }
        }
        String selection = formStepDetail.detail.selection;
        String[] selects = selection.split(",");
        setSelects(selects, checkedList);
    }

    private void setSelects(String[] selects, Set<String> checkedList) {
        this.removeAllViews();
        if (selects == null) {
            return;
        }
        for (String s: selects) {
            AppCompatCheckBox checkBox = new AppCompatCheckBox(getContext());
            checkBox.setButtonDrawable(R.drawable.icon_checkbox);
            checkBox.setText(s);
            if (checkedList != null && checkedList.contains(s)) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            checkBox.setOnCheckedChangeListener(this);
            LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp3.weight = 1;
            lp3.leftMargin = 5;
            this.addView(checkBox, lp3);
        }
    }

    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param buttonView The compound button view whose state has changed.
     * @param isChecked  The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int count = getChildCount();
        String value = "";
        for (int i = 0; i < count; i ++) {
            View child = getChildAt(i);
            if (child instanceof CheckBox && ((CheckBox) child).isChecked()) {
                String text = (String) ((CheckBox) child).getText();
                value += text + ",";
            }
        }
        if (value.endsWith(",")) {
            value = value.substring(0, value.length() - 1);
        }
        if (onEditListener != null) {
            onEditListener.onEdit(formStepDetail.detail.id, value);
        }
        formStepDetail.value = value;
        if (BuildConfig.DEBUG) {
            Log.d("TJAD", "ComBox选中项：" + value);
        }
    }
}
