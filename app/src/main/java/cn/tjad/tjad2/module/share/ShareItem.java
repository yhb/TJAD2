package cn.tjad.tjad2.module.share;

public class ShareItem {

    public int id;
    public int imageId;
    public String text;

    public ShareItem(int id, int imageId, String text) {
        this.id = id;
        this.imageId = imageId;
        this.text = text;
    }
}
