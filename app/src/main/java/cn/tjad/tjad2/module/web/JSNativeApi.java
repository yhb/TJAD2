package cn.tjad.tjad2.module.web;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;

public class JSNativeApi {

    private WebView webView;
    private Activity activity;

    public JSNativeApi(WebView webView) {
        this.webView = webView;
        this.activity = (Activity) webView.getContext();
    }

    @JavascriptInterface
    public void finish() {
        activity.finish();
    }

    @JavascriptInterface
    public void openAppDesignQuestion(int id) {
        Bundle args = new Bundle();
        args.putInt(Keys.DESIGN_ID, id);
        RouteUtils.toPage(activity, PageId.DESIGN_DETAIL, args);
    }
}
