package cn.tjad.tjad2.module.document.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.document.viewholder.DocHistoryViewHolder;

public class DocHistoryAdapter extends SimpleRecyclerViewAdapter<FileBean> {


    public DocHistoryAdapter(List<FileBean> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<FileBean> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doc_history_item, parent, false);
        return new DocHistoryViewHolder(view);
    }
}
