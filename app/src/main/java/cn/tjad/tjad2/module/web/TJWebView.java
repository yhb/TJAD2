package cn.tjad.tjad2.module.web;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.widget.ProgressBar;

import butterknife.BindDimen;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;

public class TJWebView extends WebView {

    @BindDimen(R.dimen.webview_progress_height)
    int progressHeight;

    private boolean showProgressWhenLoading;
    private ProgressBar progressBar;
    private TJWebViewClient webViewClient;
    private TJWebChromeClient webChromeClient;

    public TJWebView(Context context) {
        this(context, null);
    }

    public TJWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ButterKnife.bind(this);
        init();
    }

    public void setShowProgressWhenLoading(boolean show) {
        this.showProgressWhenLoading = show;
        if (showProgressWhenLoading) {
            webViewClient.setProgress(true, progressBar);
            webChromeClient.setProgress(true, progressBar);
        } else {
            webViewClient.setProgress(false, progressBar);
            webChromeClient.setProgress(false, progressBar);
        }
    }

    private void init() {
        progressBar = new ProgressBar(getContext(), null, android.R.style.Widget_Material_Light_ProgressBar_Horizontal);
        progressBar.setMax(100);
        progressBar.setProgress(0);
//        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, progressHeight);
//        lp.gravity = Gravity.TOP;
//        addView(progressBar, lp);
//        progressBar.setVisibility(GONE);

        webViewClient = new TJWebViewClient();
        webChromeClient = new TJWebChromeClient();
        setWebViewClient(webViewClient);
        setWebChromeClient(webChromeClient);
        addJavascriptInterface(new JSNativeApi(this), "modelview");
    }

}
