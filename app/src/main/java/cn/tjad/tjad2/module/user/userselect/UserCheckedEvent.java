package cn.tjad.tjad2.module.user.userselect;

import cn.tjad.tjad2.data.bean.User;

public class UserCheckedEvent {

    public UserCheckedEvent(User user) {
        this.checkedUser = user;
    }

    private User checkedUser;

    public void setCheckedUser(User checkedUser) {
        this.checkedUser = checkedUser;
    }

    public User getCheckedUser() {
        return checkedUser;
    }
}
