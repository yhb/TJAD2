package cn.tjad.tjad2.module.notice.settings.bean;

public class UpDateNoticeSettingRequestBean {
    public int type;
    public boolean message;
    public boolean appPush;
    public boolean email;
    public boolean popupWin;
    public boolean sms;

    public void setSwitch(int noticeType, boolean isOn) {
        if (noticeType == 1) {
            appPush = isOn;
        } else if (noticeType == 2) {
            email = isOn;
        } else if (noticeType == 3) {
            sms = isOn;
        } else if (noticeType == 4) {
            message = isOn;
        }
    }
}
