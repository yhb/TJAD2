package cn.tjad.tjad2.module.common.pictureselect;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by yanghongbing on 16/4/19.
 */
public abstract class ViewHolder<T> {

    public View itemView;

    private int itemViewType;

    public ViewHolder(View parentView) {
        this.itemView = parentView;
    }

    public ViewHolder(Context context) {
        if (getLayoutId() > 0) {
            itemView = LayoutInflater.from(context).inflate(getLayoutId(), null);
        }
    }

    public int getItemViewType() {
        return itemViewType;
    }

    public void setItemViewType(int type) {
        this.itemViewType = type;
    }

    public abstract void setValue(T t, int position);

    public abstract int getLayoutId();

    public View getItemView() {
        return itemView;
    }

}
