package cn.tjad.tjad2.module.document.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.document.viewholder.OfflineModelViewHolder;

public class OfflineModelAdapter extends SimpleRecyclerViewAdapter<String> {

    public OfflineModelAdapter(List<String> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<String> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.file_item, parent, false);
        OfflineModelViewHolder viewHolder = new OfflineModelViewHolder(view);
        return viewHolder;
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("没有已下载的离线模型", 0);
    }
}
