package cn.tjad.tjad2.module.material.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.MaterialDetail;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.material.viewholder.MaterialDetailViewHolder;

public class MaterialDetailAdapter extends SimpleRecyclerViewAdapter<MaterialDetail.Item> {


    public MaterialDetailAdapter(List<MaterialDetail.Item> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<MaterialDetail.Item> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.material_detail_item, parent, false);
        return new MaterialDetailViewHolder(view);
    }
}
