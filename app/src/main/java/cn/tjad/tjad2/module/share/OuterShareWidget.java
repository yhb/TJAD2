package cn.tjad.tjad2.module.share;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.net.http.HttpConstants;
import com.qiandian.android.base.net.http.HttpId;
import com.qiandian.android.base.net.http.IDataCallBack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.system.email.Email;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.business.RemoteDataService;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.data.bean.ShareLink;
import cn.tjad.tjad2.utils.Tools;

public class OuterShareWidget extends FrameLayout implements AdapterView.OnItemClickListener, IDataCallBack {

    @BindView(R.id.share_gridview)
    GridView shareGridView;
    @BindView(R.id.share_type_group)
    RadioGroup shareTypeGroup;
    @BindView(R.id.share_valid_group)
    RadioGroup shareValidGroup;
    @BindView(R.id.input_text)
    EditText toplicText;

    private final int SHARE_QQ = 0;
    private final int SHARE_EMAIL = 1;
    private final int SHARE_WECHAT = 2;
    private final int SHARE_WECHAR_MOMENTS = 3;
    private final int SHARE_OTHER = 4;

    private int projectId;
    private List<ShareItem> shareItems;
    private ShareAdapter adapter;

    private FileBean fileBean;

    public OuterShareWidget(@NonNull Context context) {
        this(context, null);
    }

    public OuterShareWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.share_view, this);
        ButterKnife.bind(this);
        projectId = TjadApplication.getInstance().getSession().getProjectId();
        initShareItems();
        adapter = new ShareAdapter(getContext(), shareItems);
        shareGridView.setAdapter(adapter);
        shareGridView.setOnItemClickListener(this);
    }

    private void initShareItems() {
        if (shareItems == null) {
            shareItems = new ArrayList<>();
        }
        shareItems.clear();
        shareItems.add(new ShareItem(SHARE_QQ, R.mipmap.ssdk_oks_classic_qq, "QQ"));
        shareItems.add(new ShareItem(SHARE_EMAIL, R.mipmap.ssdk_oks_classic_email, "电子邮件"));
        shareItems.add(new ShareItem(SHARE_WECHAT, R.mipmap.ssdk_oks_classic_wechat, "微信好友"));
        shareItems.add(new ShareItem(SHARE_WECHAR_MOMENTS, R.mipmap.ssdk_oks_classic_wechatmoments, "微信朋友圈"));
        shareItems.add(new ShareItem(SHARE_OTHER, R.mipmap.ssdk_oks_classic_other, "其它"));
    }

    public void setFileBean(FileBean fileBean) {
        this.fileBean = fileBean;
    }

    private ShareItem shareItem;

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        boolean encrypt = shareTypeGroup.getCheckedRadioButtonId() == R.id.share_type_encrypt;
        int shareValidCheckedId = shareValidGroup.getCheckedRadioButtonId();
        int expireIn = -1;
        if (shareValidCheckedId == R.id.share_valid_1_day) {
            expireIn = 1;
        } else if (shareValidCheckedId == R.id.share_valid_7_days) {
            expireIn = 7;
        }
        String topic = toplicText.getText().toString();
        if (TextUtils.isEmpty(topic)) {
            Tools.showToast("请输入话题");
            return;
        }
        RemoteDataService.getShareData(projectId, fileBean.document.id, encrypt, expireIn, topic, this);
        shareItem = adapter.getItem(i);
    }

    @Override
    public boolean onReceiveError(int httpId, int errorCode, String errorMsg) {
        if (httpId == HttpId.SHARE_CONTENT) {
            if (TextUtils.isEmpty(errorMsg)) {
                Tools.showToast("获取分享链接失败， 请稍后重试");
            } else {
                Tools.showToast(errorMsg);
            }
        }
        return true;
    }

    @Override
    public void onDataRefresh(int httpId, Object responseBean) {
        if (httpId == HttpId.SHARE_CONTENT) {
            CommonBean<ShareLink> bean = (CommonBean<ShareLink>) responseBean;
            if (bean.resCode == HttpConstants.CODE_SUCCESS) {
                onShare(bean.result);
            } else {
                Tools.showToast(bean.respMsg);
            }
        }
    }

    private PlatformActionListener platformActionListener = new PlatformActionListener() {
        @Override
        public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
            Log.d("tjad", "分享成功");
        }

        @Override
        public void onError(Platform platform, int i, Throwable throwable) {
            Log.d("tjad", "分享失败" + throwable.getMessage());
        }

        @Override
        public void onCancel(Platform platform, int i) {
            Log.d("tjad", "分享取消");
        }
    };

    private void onShare(ShareLink result) {
        Platform.ShareParams shareParams = new Platform.ShareParams();
        switch (shareItem.id) {
            case SHARE_QQ:
                shareParams.setTitle(result.topic);
                shareParams.setTitleUrl(result.url);
                shareParams.setShareType(Platform.SHARE_WEBPAGE);
                Platform qq = ShareSDK.getPlatform(QQ.NAME);
                qq.setPlatformActionListener(platformActionListener);
                qq.share(shareParams);
                break;
            case SHARE_EMAIL:
                shareParams.setTitle(result.topic);
                shareParams.setText(result.url);
                shareParams.setImageUrl("");
                Platform email = ShareSDK.getPlatform(Email.NAME);
                email.setPlatformActionListener(platformActionListener);
                email.share(shareParams);
                break;
            case SHARE_WECHAT:
                shareParams.setShareType(Platform.SHARE_WEBPAGE);
                shareParams.setText(result.topic);
                shareParams.setTitle(result.topic);
                shareParams.setUrl(result.url);
                shareParams.setImageUrl("http://www.tjad.cn/img/logo-complex.png");
                Wechat wechat = (Wechat) ShareSDK.getPlatform(Wechat.NAME);
                wechat.setPlatformActionListener(platformActionListener);
                wechat.share(shareParams);
                break;
            case SHARE_WECHAR_MOMENTS:
                shareParams.setShareType(Platform.SHARE_WEBPAGE);
                shareParams.setText(result.topic);
                shareParams.setTitle(result.topic);
                shareParams.setUrl(result.url);
                shareParams.setImageUrl("http://www.tjad.cn/img/logo-complex.png");
                Platform wechat2 = ShareSDK.getPlatform(WechatMoments.NAME);
                wechat2.setPlatformActionListener(platformActionListener);
                wechat2.share(shareParams);
                break;
            case SHARE_OTHER:

                break;
        }
    }


    private static class ShareAdapter extends BaseAdapter {

        private Context context;
        private List<ShareItem> data;

        public ShareAdapter(Context context, List<ShareItem> data) {
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public ShareItem getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                view = View.inflate(context, R.layout.share_item, null);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.setData(getItem(i));
            return view;
        }

        private class ViewHolder {
            private ImageView imageView;
            private TextView textView;

            public ViewHolder(View itemView) {
                imageView = itemView.findViewById(R.id.share_image);
                textView = itemView.findViewById(R.id.share_text);
            }

            public void setData(ShareItem shareItem) {
                imageView.setImageResource(shareItem.imageId);
                textView.setText(shareItem.text);
            }
        }
    }


}
