package cn.tjad.tjad2.module.common.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.utils.Tools;

public class FileRenameDialog extends TjDialogFragment {

    private EditText editText;
    private OnConfirmListener onConfirmListener;
    private String originName = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRenameView();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void setRenameView() {
        editText = (EditText) getLayoutInflater().inflate(R.layout.file_operate_input_text, null);
        editText.setSelectAllOnFocus(true);
        editText.setText(originName);
        this.setTitle("重命名")
                .setView(editText)
                .setPositiveButton("确认", new OnDialogClickListener() {
                    @Override
                    public boolean onClick(Dialog dialog, int which) {
                        String text = editText.getText().toString().trim();
                        if (TextUtils.isEmpty(text)) {
                            Tools.showToast("文件名称不能为空！");
                            return false;
                        } else {
                            if (onConfirmListener != null) {
                                onConfirmListener.onConfirm(text);
                            }
                        }
                        return true;
                    }
                })
                .setNegativeButton("取消", null);
    }

    public interface OnConfirmListener {
        void onConfirm(String text);
    }

    public void setOriginName(String name) {
        this.originName = name;
    }

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }

}
