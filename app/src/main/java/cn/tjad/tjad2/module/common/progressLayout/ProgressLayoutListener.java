package cn.tjad.tjad2.module.common.progressLayout;

public interface ProgressLayoutListener {
    void onProgressCompleted();
    void onProgressChanged(int seconds);
}
