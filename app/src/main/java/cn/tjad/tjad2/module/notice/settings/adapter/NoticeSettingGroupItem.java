package cn.tjad.tjad2.module.notice.settings.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;

import com.baozi.treerecyclerview.base.ViewHolder;
import com.baozi.treerecyclerview.factory.ItemHelperFactory;
import com.baozi.treerecyclerview.item.TreeItem;
import com.baozi.treerecyclerview.item.TreeItemGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.NoticeSettingElement;
import cn.tjad.tjad2.data.bean.NoticeSettingGroup;

public class NoticeSettingGroupItem extends TreeItemGroup<NoticeSettingGroup> implements CompoundButton.OnCheckedChangeListener {
    @Nullable
    @Override
    protected List<TreeItem> initChildList(NoticeSettingGroup data) {
        return ItemHelperFactory.createItems(data.settings, NoticeElementItem.class, this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.notice_setting_item_1;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        SwitchCompat switchCompat = viewHolder.getView(R.id.setting_item_1_switch);
        switchCompat.setOnCheckedChangeListener(null);
        viewHolder.setText(R.id.setting_item_1_name, data.name);
        //开关
        boolean isOn = true;
        if (data.settings != null && data.settings.size() > 0) {
            for (NoticeSettingElement element: data.settings) {
                if (!element.isChecked()) {
                    isOn = false;
                    break;
                }
            }
        }
        viewHolder.setChecked(R.id.setting_item_1_switch, isOn);
        switchCompat.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        List<TreeItem> childs = getChild();
        if (childs != null && childs.size() > 0) {
            for (TreeItem item: childs) {
                if (item instanceof NoticeElementItem) {
                    ((NoticeElementItem) item).setChecked(isChecked);
                }
            }
        }
    }
}
