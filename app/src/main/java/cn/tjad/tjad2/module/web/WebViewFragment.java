package cn.tjad.tjad2.module.web;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.widget.TJADTTitleView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.tjad.tjad2.R;

public class WebViewFragment extends BaseFragment {

    @BindView(R.id.webview)
    protected TJWebView webView;
    @BindView(R.id.close)
    ImageView closeBtn;

    @Override
    public View getContentView(LayoutInflater inflater) {
        View rootView = inflater.inflate(R.layout.webview_layout, null);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        loadPage();
    }

    private void initViews() {
        webView.setShowProgressWhenLoading(true);
        WebSettings webSetting = webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(true);
        // webSetting.setLoadWithOverviewMode(true);
        webSetting.setAppCacheEnabled(true);
        // webSetting.setDatabaseEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setGeolocationEnabled(true);
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);
        // webSetting.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
        webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
        // webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSetting.setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSetting.setAllowUniversalAccessFromFileURLs(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    protected void loadPage() {
        Bundle args = getArguments();
        String url = "http://bim.tjad.cn";
        if (args != null) {
            url = args.getString("url");
        }
        webView.loadUrl(url);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.showRight(true);
        titleView.setRightText("刷新");
    }

    @Override
    public void onTitleClick(int which) {
        super.onTitleClick(which);
        if (which == TJADTTitleView.TITLE_RIGHT) {
            webView.reload();
        }
    }

    @OnClick(R.id.close)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close:
                getActivity().finish();
                break;
        }
    }
}
