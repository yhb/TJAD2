package cn.tjad.tjad2.module.construction.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Construction;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class ConstructionListViewHolder extends SimpleBaseViewHolder<Construction> {

    @BindView(R.id.construction_status)
    ImageView statusIV;
    @BindView(R.id.construction_description)
    TextView descriptionTV;
    @BindView(R.id.construction_type)
    TextView typeTV;
    @BindView(R.id.constrution_floor)
    TextView floorTV;
    @BindView(R.id.construction_code)
    TextView codeTV;

    public ConstructionListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(Construction data, int position) {
        int statusIconId = Tools.getConsStatusIconId(data.consStatus);
        statusIV.setImageResource(statusIconId);
        descriptionTV.setText(data.content);
        if (data.type != null) {
            typeTV.setText(data.type.name);
        } else {
            typeTV.setText("");
        }
        if (data.floor != null) {
            floorTV.setText(data.floor.name);
        } else {
            floorTV.setText("");
        }
        codeTV.setText(data.code);
    }
}
