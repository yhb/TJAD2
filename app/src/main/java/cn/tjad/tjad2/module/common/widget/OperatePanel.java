package cn.tjad.tjad2.module.common.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.tjad.tjad2.R;

public class OperatePanel extends LinearLayout {
    public OperatePanel(Context context) {
        this(context, null);
    }

    public OperatePanel(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
    }

    public void addButton(CharSequence text, OnClickListener onClickListener) {
        if (getChildCount() > 0) {
            addDivideLine();
        }
        TextView textView = new TextView(getContext());
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(14);
        textView.setTextColor(Color.parseColor("#11BEFB"));
        textView.setText(text);
        textView.setBackgroundResource(R.drawable.item_click_color);
        textView.setOnClickListener(onClickListener);
        LayoutParams layoutParams = new LayoutParams(0, LayoutParams.MATCH_PARENT);
        layoutParams.weight = 1;
        this.addView(textView, layoutParams);
    }

    private void addDivideLine() {
        View view = new View(getContext());
        view.setBackgroundColor(Color.parseColor("#E5E5E5"));
        LayoutParams layoutParams = new LayoutParams(1, LayoutParams.MATCH_PARENT);
        this.addView(view, layoutParams);
    }
}
