package cn.tjad.tjad2.module.user.viewholder;

import android.view.View;
import android.widget.TextView;

import cn.tjad.tjad2.data.bean.Department;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class DepartmentViewHolder extends SimpleBaseViewHolder<Department> {

    private TextView textView;

    public DepartmentViewHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView;
    }

    @Override
    public void setData(Department data, int position) {
        textView.setText(data.name);
    }
}
