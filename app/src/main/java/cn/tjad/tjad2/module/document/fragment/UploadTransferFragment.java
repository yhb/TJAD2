package cn.tjad.tjad2.module.document.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.hjy.http.upload.FileUploadManager;
import com.hjy.http.upload.FileUploadTask;

import java.util.List;

import cn.tjad.tjad2.module.common.fragment.BaseTransferFragment;
import cn.tjad.tjad2.business.SettingManager;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.document.adapter.UploadListAdapter;
import cn.tjad.tjad2.utils.NetworkUtils;
import cn.tjad.tjad2.module.common.dialog.OnDialogClickListener;
import cn.tjad.tjad2.module.common.dialog.TjDialogFragment;

public class UploadTransferFragment extends BaseTransferFragment implements OnItemClickListener {

    private UploadListAdapter adapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<FileUploadTask> uploadTaskList = FileUploadManager.getInstance().getAllTasks();
        adapter = new UploadListAdapter(uploadTaskList);
        adapter.setOnItemClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        final FileUploadTask uploadTask = adapter.getItemData(position);
        int status = uploadTask.getUploadStatus();
        if (status == FileUploadTask.UPLOAD_FAILED) {
            TjDialogFragment.Builder builder = new TjDialogFragment.Builder(getContext());
            builder.setTitle("提示")
                    .setMessage("文件没有上传成功, 是否重新上传?")
                    .setNeutralButton("取消", null)
                    .setPositiveButton("重新上传", new OnDialogClickListener() {
                        @Override
                        public boolean onClick(Dialog dialogInterface, int i) {
                            executeUpload(uploadTask);
                            return true;
                        }
                    })
                    .show(getChildFragmentManager());
        }
    }

    private void executeUpload(final FileUploadTask uploadTask) {
        boolean transferWithWifi = SettingManager.getTransferWithWifi();
        boolean isWifi = NetworkUtils.isWifi(getContext());
        if (isWifi || !transferWithWifi) {
            FileUploadManager.getInstance().executeTask(uploadTask);
        } else {
            TjDialogFragment.Builder builder = new TjDialogFragment.Builder(getContext());
            builder.setTitle("提示")
                    .setMessage("当前为非WIFI环境，继续上传可能会消耗较多流量，确认是否继续上传？")
                    .setNegativeButton("取消", null)
                    .setPositiveButton("继续上传", new OnDialogClickListener() {
                        @Override
                        public boolean onClick(Dialog dialogInterface, int i) {
                            FileUploadManager.getInstance().executeTask(uploadTask);
                            return true;
                        }
                    })
                    .show(getChildFragmentManager());
        }
    }
}
