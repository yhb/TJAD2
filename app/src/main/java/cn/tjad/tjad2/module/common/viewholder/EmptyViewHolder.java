package cn.tjad.tjad2.module.common.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;

public class EmptyViewHolder extends SimpleBaseViewHolder<String> {

    @BindView(R.id.text)
    TextView textView;

    public EmptyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(String data, int position) {
        textView.setText(data);
    }
}
