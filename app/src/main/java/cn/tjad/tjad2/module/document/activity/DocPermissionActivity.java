package cn.tjad.tjad2.module.document.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.document.fragment.PermissionFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.DOC_PERMISSION)
public class DocPermissionActivity extends BaseActivity {

    private PermissionFragment permissionFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissionFragment = new PermissionFragment();
        Bundle args = getIntent().getExtras();
        if (args != null) {
            permissionFragment.setArguments(args);
        }
        setContentView(permissionFragment);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
