package cn.tjad.tjad2.module.design.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.MultiItemBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleViewHolder;
import cn.tjad.tjad2.module.design.viewholder.DesignFinishViewHolder;
import cn.tjad.tjad2.module.design.viewholder.DesignReplyViewHolder;
import cn.tjad.tjad2.module.design.designDetail.DesignDetailTopView;
import cn.tjad.tjad2.module.design.designDetail.DesignHandleView;

public class DesignDetailAdapter extends SimpleRecyclerViewAdapter<MultiItemBean> {

    public static final int ITEM_TYPE_TOP = 10;
    public static final int ITEM_TYPE_REPLY_ITEM = 11;
    public static final int ITEM_TYPE_FINISH = 12;
    public static final int ITEM_TYPE_HANDLE = 13;

    private DesignDetailTopView topView;
    private DesignHandleView handleView;

    public DesignDetailAdapter(List<MultiItemBean> data) {
        super(data);
    }

    @Override
    public int getItemViewType(int position) {
        return getItemData(position).getType();
    }

    public void setTopView(DesignDetailTopView topView) {
        this.topView = topView;
    }

    public void setHandleView(DesignHandleView handleView) {
        this.handleView = handleView;
    }

    @Override
    protected SimpleBaseViewHolder<MultiItemBean> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        SimpleBaseViewHolder<MultiItemBean> viewHolder = null;
        if (viewType == ITEM_TYPE_TOP) {
            viewHolder = new SimpleViewHolder<MultiItemBean>(topView);
        } else if (viewType == ITEM_TYPE_REPLY_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.design_detail_reply_item, parent, false);
            viewHolder = new DesignReplyViewHolder(view);
        } else if (viewType == ITEM_TYPE_FINISH) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.design_detail_finish_item, parent, false);
            viewHolder = new DesignFinishViewHolder(view);
        } else if (viewType == ITEM_TYPE_HANDLE) {
            viewHolder = new SimpleViewHolder<>(handleView);
        }
        return viewHolder;
    }
}
