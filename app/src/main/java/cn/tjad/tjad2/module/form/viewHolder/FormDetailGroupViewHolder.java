package cn.tjad.tjad2.module.form.viewHolder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class FormDetailGroupViewHolder extends SimpleBaseViewHolder<FormDetail.FormDetailStep> {

    @BindView(R.id.form_detail_group_name)
    TextView nameTV;

    public FormDetailGroupViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(FormDetail.FormDetailStep data, int position) {
        if (data.step != null) {
            nameTV.setText(data.step.name);
        } else {
            nameTV.setText("--");
        }
    }
}
