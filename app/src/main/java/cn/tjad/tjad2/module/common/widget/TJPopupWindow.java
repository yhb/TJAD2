package cn.tjad.tjad2.module.common.widget;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import cn.tjad.tjad2.R;


/**
 * Created by YANGHONGBING615 on 2015-09-10.
 */
public class TJPopupWindow extends PopupWindow implements PopupWindow.OnDismissListener {

    private Activity activity;
    private boolean cancelable;

    public TJPopupWindow(Activity activity) {
        super(activity);
        this.activity = activity;
        this.cancelable = true;
        init();
    }

    private void init() {
        this.setFocusable(true);
        this.setOnDismissListener(this);
        this.setOutsideTouchable(true);
        Drawable d = activity.getResources().getDrawable(R.mipmap.ic_launcher);
        d.setAlpha(1);
        this.setBackgroundDrawable(d);
    }

    @Override
    public void showAsDropDown(View anchor) {
        super.showAsDropDown(anchor);
        backgroundAlpha(0.5f);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        super.showAsDropDown(anchor, xoff, yoff);
        backgroundAlpha(0.5f);
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        super.showAtLocation(parent, gravity, x, y);
        backgroundAlpha(0.5f);
    }

    @Override
    public void onDismiss() {
        backgroundAlpha(1f);
    }

    public void backgroundAlpha(float bgAlpha){
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        activity.getWindow().setAttributes(lp);
    }
}
