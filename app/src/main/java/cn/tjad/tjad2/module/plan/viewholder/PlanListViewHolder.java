package cn.tjad.tjad2.module.plan.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Plan;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class PlanListViewHolder extends SimpleBaseViewHolder<Plan> {

    @BindView(R.id.plan_item_name)
    TextView planNameTV;

    public PlanListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(Plan data, int position) {
        planNameTV.setText(data.name);
    }
}
