package cn.tjad.tjad2.module.document.viewholder;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hjy.http.upload.ErrorType;
import com.hjy.http.upload.FileUploadInfo;
import com.hjy.http.upload.FileUploadManager;
import com.hjy.http.upload.FileUploadTask;
import com.hjy.http.upload.listener.OnUploadListener;
import com.hjy.http.upload.listener.OnUploadProgressListener;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class UploadViewHolder extends SimpleBaseViewHolder<FileUploadTask> implements OnUploadProgressListener, OnUploadListener {

    @BindView(R.id.file_name)
    TextView fileName;
    @BindView(R.id.file_size)
    TextView fileSize;
    @BindView(R.id.file_time)
    TextView time;
    @BindView(R.id.file_path)
    TextView path;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.file_status)
    TextView statusTV;

    public UploadViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(FileUploadTask data, int position) {
        FileUploadInfo uploadInfo = data.getFileUploadInfo();
        String filePath = uploadInfo.getOriginalFilePath();
        File file = new File(filePath);
        fileName.setText(file.getName());
        fileSize.setText(Tools.getDisplayedFileSize(data.getFileSize()));
        long timeStamp = data.getStartTime();
        time.setText(Tools.getFormatTime(timeStamp, "yyyy-MM-dd HH:mm"));
        path.setText(uploadInfo.getUploadFilePath());
        int status = data.getUploadStatus();
        if (status == 0) {
            statusTV.setText("上传中");
        } else if (status == 1) {
            statusTV.setText("上传成功");
        } else if (status == 2) {
            statusTV.setText("上传失败");
        } else {
            statusTV.setText("");
        }
        int progress = data.getCurrProgress();
        progressBar.setProgress(progress);
        FileUploadManager.getInstance().addUploadingListener(data, this);
        FileUploadManager.getInstance().addUploadingProgressListener(data, this);
    }

    /**
     * @param uploadTask
     * @param totalSize  总大小
     * @param currSize   当前已上传的大小
     * @param progress   进度 0-100
     */
    @Override
    public void onProgress(FileUploadTask uploadTask, long totalSize, long currSize, int progress) {
        if (progress != progressBar.getProgress()) {
            progressBar.setProgress(progress);
        }
        statusTV.setText("正在上传");
        fileSize.setText(Tools.getDisplayedFileSize(totalSize));
    }

    /**
     * 上传失败
     *
     * @param uploadTask
     * @param errorType  {@link ErrorType}
     * @param msg
     */
    @Override
    public void onError(FileUploadTask uploadTask, int errorType, String msg) {
        statusTV.setText("上传失败");
    }

    /**
     * 上传成功
     *
     * @param uploadTask
     * @param data       数据返回的解析结果
     */
    @Override
    public void onSuccess(FileUploadTask uploadTask, Object data) {
        statusTV.setText("上传成功");
    }
}
