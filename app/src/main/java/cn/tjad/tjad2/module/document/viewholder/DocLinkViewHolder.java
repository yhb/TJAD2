package cn.tjad.tjad2.module.document.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Link;
import cn.tjad.tjad2.data.event.DocLinkDeleteEvent;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class DocLinkViewHolder extends SimpleBaseViewHolder<Link> {

    @BindView(R.id.file_name)
    TextView fileName;
    @BindView(R.id.creator)
    TextView creator;
    @BindView(R.id.create_time)
    TextView createTime;
    @BindView(R.id.icon_delete)
    ImageView deleteIcon;

    private Link data;

    public DocLinkViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(Link data, int position) {
        this.data = data;
        if (data != null) {
            fileName.setText(data.name);
            if (data.createUser != null) {
                creator.setText(data.createUser.name);
            }
            createTime.setText(data.createTime);
        }
    }

    @OnClick(R.id.icon_delete)
    public void onDeleteClick(View view) {
        DocLinkDeleteEvent event = new DocLinkDeleteEvent();
        event.setLinkBean(data);
        EventBus.getDefault().post(event);
    }
}
