package cn.tjad.tjad2.module.common.viewholder;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.pictureselect.ViewHolder;

/**
 * Created by yanghongbing on 17/11/5.
 */

public class PicturePreviewViewHolder extends ViewHolder<String> {

    @BindView(R.id.imageview)
    ImageView imageView;

    public PicturePreviewViewHolder(Context context) {
        super(context);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setValue(String s, int position) {
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(imageView.getContext())
                .load(s)
                .apply(options)
                .into(imageView);
    }

    @Override
    public int getLayoutId() {
        return R.layout.simple_image_view;
    }
}
