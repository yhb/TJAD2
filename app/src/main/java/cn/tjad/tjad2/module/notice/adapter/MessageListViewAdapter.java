package cn.tjad.tjad2.module.notice.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.MessageBean;
import cn.tjad.tjad2.module.notice.viewholder.MessageListViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class MessageListViewAdapter extends SimpleRecyclerViewAdapter<MessageBean> {

    public MessageListViewAdapter(List<MessageBean> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<MessageBean> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_list_item, parent, false);
        return new MessageListViewHolder(view);
    }
}
