package cn.tjad.tjad2.module.common.adapter;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.listener.OnItemLongClickListener;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public abstract class SimpleRecyclerViewAdapter<T> extends RecyclerView.Adapter<SimpleBaseViewHolder<T>> {

    private List<T> data;
    private OnItemClickListener onItemClickListener;
    private OnItemLongClickListener onItemLongClickListener;
    private final HashMap<Integer, View.OnClickListener> elementClickListenerMap = new HashMap<>();

    private final int ITEM_EMPTY = 0;
    private final int ITEM_NORMAL = 1;

    public SimpleRecyclerViewAdapter(List<T> data) {
        if (data == null) {
            this.data = new ArrayList<>();
        } else {
            this.data = data;
        }
    }

    public T getItemData(int position) {
        if (position < 0 || position >= data.size()) {
            return null;
        }
        return data.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public void updateData(List<T> data) {
        this.data.clear();
        this.data.addAll(data);
    }

    public void updateData(List<T> data, boolean append) {
        if (!append) {
            this.data.clear();
        }
        this.data.addAll(data);
    }

    @NonNull
    @Override
    public SimpleBaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_EMPTY) {
            return onCreateEmptyViewHolder(parent, viewType);
        } else {
            return onCreateNormalViewHolder(parent, viewType);
        }
    }

    protected SimpleBaseViewHolder onCreateEmptyViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_view_layout, parent, false);
        EmptyViewHolder emptyViewHolder = new EmptyViewHolder(view);
        return emptyViewHolder;
    }

    protected abstract SimpleBaseViewHolder<T> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType);

    protected  void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("列表为空", 0);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleBaseViewHolder<T> holder, int position) {
        if (getItemViewType(position) != ITEM_EMPTY) {
            holder.fillData(data.get(position), position);
            holder.setOnItemClickListener(onItemClickListener);
            holder.setOnItemLongClickListener(onItemLongClickListener);
            holder.setItemClickListenerMap(elementClickListenerMap, data.get(position));
        } else {
            fillEmptyViewData((EmptyViewHolder)holder);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (data == null || data.size() == 0) {
            return ITEM_EMPTY;
        }
        return ITEM_NORMAL;
    }

    @Override
    public int getItemCount() {
        if (data == null || data.size() == 0) {
            return 1;
        }
        return data.size();
    }

    public List<T> getData() {
        return data;
    }

    public void addElementClickListener(@IdRes int resId, View.OnClickListener onClickListener) {
        this.elementClickListenerMap.put(resId, onClickListener);
    }
}
