package cn.tjad.tjad2.module.document.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RadioButton;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.utils.FileSort;

public class FileSortDialog {

    @BindViews({R.id.sort_by_name_row,  R.id.sort_by_time_row, R.id.sort_by_size_row, R.id.sort_by_type_row})
    List<View> sortBys;

    @BindViews({R.id.sort_by_name_cb, R.id.sort_by_time_cb, R.id.sort_by_size_cb, R.id.sort_by_type_cb})
    List<RadioButton> sortCBs;

    @BindView(R.id.sort_ascend)
    View sortAscend;
    @BindView(R.id.sort_descend)
    View sortDescend;

    private Context context;
    private PopupWindow popupWindow;
    private View contentView;
    private OnSortListener onSortListener;

    private int sortBy = FileSort.SORT_BY_NAME;

    public FileSortDialog(Context context) {
        this.context = context;
        initPopupWindow();
    }

    private void initPopupWindow() {
        popupWindow = new PopupWindow(context);
        popupWindow.setWidth(500);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable());
    }

    private View createView() {
        View view = LayoutInflater.from(context).inflate(R.layout.file_sort_layout, null);
        ButterKnife.bind(this, view);
        sortCBs.get(sortBy).setChecked(true);
        return view;
    }

    public void showAsDropDown(View anchor) {
        if (contentView == null) {
            contentView = createView();
            popupWindow.setContentView(contentView);
        }
        popupWindow.showAsDropDown(anchor);

    }

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        View view = LayoutInflater.from(getContext()).inflate(R.layout.file_sort_layout, null);
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setTitle("排序")
//                .setView(view);
//        ButterKnife.bind(this, view);
//        sortCBs.get(sortBy).setChecked(true);
//        return builder.create();
//    }

    public void setSortBy(int sortBy) {
        this.sortBy = sortBy;
//        if (sortBy >= 0 && sortBy < sortCBs.size()) {
//            for (int i = 0; i < sortBys.size(); i++) {
//                RadioButton rb = sortCBs.get(i);
//                rb.setChecked(i == sortBy);
//                this.sortBy = sortBy;
//            }
//        }
    }

    @OnClick({R.id.sort_by_name_row, R.id.sort_by_size_row, R.id.sort_by_time_row, R.id.sort_by_type_row})
    public void onLineClick(View view) {
        int index = sortBys.indexOf(view);
        if (index >= 0 && index <= sortCBs.size()) {
            sortCBs.get(index).toggle();
        }
    }

    @OnClick({R.id.sort_ascend, R.id.sort_descend})
    public void onClick(View view) {
        int order = FileSort.SORT_ASCEND;
        switch (view.getId()) {
            case R.id.sort_ascend:
                order = FileSort.SORT_ASCEND;
                popupWindow.dismiss();
                break;
            case R.id.sort_descend:
                order = FileSort.SORT_DESCEND;
                popupWindow.dismiss();
                break;
        }
        if (onSortListener != null) {
            onSortListener.onSort(sortBy, order);
        }
    }

    public void setOnSortListener(OnSortListener onSortListener) {
        this.onSortListener = onSortListener;
    }

    public interface OnSortListener {
        void onSort(int sortBy, int order);
    }

    @OnCheckedChanged({R.id.sort_by_name_cb, R.id.sort_by_size_cb, R.id.sort_by_time_cb, R.id.sort_by_type_cb})
    public void onCheckedChanged(RadioButton button, boolean checked) {
        if (checked) {
            for (int i = 0; i < sortCBs.size(); i++) {
                RadioButton rb = sortCBs.get(i);
                rb.setChecked(rb == button);
                if (rb == button) {
                    sortBy = i;
                }
            }
        }
    }
}
