package cn.tjad.tjad2.module.material.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.MaterialBean;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.material.viewholder.MaterialTrackingViewHolder;

public class MaterialTrackingListAdapter extends SimpleRecyclerViewAdapter<MaterialBean> {
    public MaterialTrackingListAdapter(List<MaterialBean> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<MaterialBean> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.material_tracking_list_item, parent, false);
        return new MaterialTrackingViewHolder(view);
    }
}
