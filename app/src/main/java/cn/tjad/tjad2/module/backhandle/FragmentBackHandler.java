package cn.tjad.tjad2.module.backhandle;

public interface FragmentBackHandler {
    boolean onBackPressed();
}