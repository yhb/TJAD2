package cn.tjad.tjad2.module.plan;

public class TaskEditRequestBean {
    public int id;
    public String name;
    public String planStartTime;
    public String planEndTime;
    public String actualStartTime;
    public String actualEndTime;
    public User user;

    public static class User {
        public int id;
    }
}
