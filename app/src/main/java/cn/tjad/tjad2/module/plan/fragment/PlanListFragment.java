package cn.tjad.tjad2.module.plan.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.bean.CommonListBean;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.util.ArrayList;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.MessageCount;
import cn.tjad.tjad2.data.bean.Plan;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.fragment.RefreshableListFragment;
import cn.tjad.tjad2.module.plan.adapter.PlanListAdapter;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;
import q.rorbin.badgeview.QBadgeView;

@Route(path = PageId.PLAN_LIST)
public class PlanListFragment extends RefreshableListFragment implements OnItemClickListener {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter.setOnItemClickListener(this::onItemClick);
        requestData();
    }

    private QBadgeView badgeView;

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("进度计划");
        titleView.showRight(true);
        titleView.setRightImage(R.drawable.icon_message);
        badgeView = new QBadgeView(getContext());
        badgeView.bindTarget(titleView.getRightImageView());
        badgeView.setBadgeGravity(Gravity.END | Gravity.TOP);
        badgeView.setBadgeNumber(0);
    }

    @Override
    public void onTitleClick(int which) {
        super.onTitleClick(which);
        if (which == TJADTTitleView.TITLE_RIGHT) {
            RouteUtils.toFragmentActivity(getContext(), PageId.MESSAGE_MENU, null);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            requestMessageCount();
        }
    }

    @Override
    protected void requestData() {
        RetrofitManager.getInstance().getApiService()
                .getPlanList(Tools.getProjectId(), startIndex, PAGE_COUNT)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<ArrayList<Plan>>(compositeDisposable) {

                    @Override
                    public void onSuccess(CommonBean<ArrayList<Plan>> t) {
                        CommonListBean<Plan> list = (CommonListBean<Plan>) t;
                        adapter.updateData(list.result, !isRefresh);
                        adapter.notifyDataSetChanged();
                        onDataLoadSuccess(list.pagination);
                    }

                    @Override
                    public void onFailed(String message) {
                        Tools.showToast(message);
                        onDataLoadFailed();
                    }
                });

    }

    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        return new PlanListAdapter(null);
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        Plan plan = (Plan) adapter.getItemData(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Keys.PLAN, plan);
        bundle.putSerializable(Keys.TITLE, plan.name);
        RouteUtils.toFragmentActivity(getActivity(), PageId.PLAN_TASK_LIST, bundle);

//        RetrofitManager.getInstance().getApiService()
//                .getPlanDetail(Tools.getProjectId(), plan.id)
//                .compose(RxUtils.networkTransformer())
//                .subscribe(new ResponseObserver<PlanTask>(compositeDisposable) {
//                    @Override
//                    public void onSuccess(CommonBean<PlanTask> t) {
//                        Bundle bundle = new Bundle();
//                        ArrayList<Task> tasks = null;
//                        if (t.result != null) {
//                            tasks = t.result.tasks;
//                        }
//                        bundle.putSerializable(Keys.PLAN_TASKS, tasks);
//                        bundle.putSerializable(Keys.PLAN_TASK, t.result);
//                        bundle.putString(Keys.TITLE, t.result.name);
//                        RouteUtils.toFragment(getParentFragment(), PageId.PLAN_TASK_LIST, bundle);
//                        ProgressDialogUtils.dismiss();
//                    }
//
//                    @Override
//                    public void onFailed(String message) {
//                        Tools.showToast(message);
//                        ProgressDialogUtils.dismiss();
//                    }
//                });
    }

    @Override
    public void onResume() {
        super.onResume();
        requestMessageCount();
    }

    //用于更新消息图标是否显示红点
    private void requestMessageCount() {
        RetrofitManager.getInstance().getApiService()
                .getMessageCount(Tools.getProjectId(), false)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<MessageCount>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<MessageCount> t) {
                        if (t.result != null) {
                            if (t.result.total > 0) {
                                badgeView.setBadgeNumber(-1); //显示圆点
                            } else {
                                badgeView.setBadgeNumber(0); //不显示
                            }
                        }
                    }

                    @Override
                    public void onFailed(String message) {

                    }
                });
    }
}
