package cn.tjad.tjad2.module.user.userselect;

public class SelectMode {

    public static final int SINGGLE_CHOICE = 1;

    public static final int MULTIPLE_CHOICE = -1;

    private static int selectMode = MULTIPLE_CHOICE;

    public static void setSelectMode(int selectMode) {
        SelectMode.selectMode = selectMode;
    }

    public static int getSelectMode() {
        return selectMode;
    }
}
