package cn.tjad.tjad2.module.common.pictureselect;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.luck.picture.lib.photoview.PhotoView;

import java.util.Arrays;
import java.util.List;

import cn.tjad.tjad2.R;

/**
 * Created by yanghongbing on 17/12/27.
 */

public class ImagePreviewWindow {

    private Context context;
    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;
    private ViewPager viewPager;

    private List<String> urlList;

    public ImagePreviewWindow(Context context) {
        this.context = context;
        builder = new AlertDialog.Builder(context, R.style.plane_dialog);
        builder.setCancelable(true);

        viewPager = new ViewPager(context);
        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog = builder.show();
    }

    public void setImageUrl(String url) {
        setImageUrlList(Arrays.asList(url));
    }

    public void setImageUrlList(List<String> urlList) {
        this.urlList = urlList;
        ImageViewPagerAdapter adapter = new ImageViewPagerAdapter(context, urlList);
        adapter.setOnImageClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });
        viewPager.setAdapter(adapter);
    }

    public void show(int position) {
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.getDecorView().setPadding(0, 0, 0, 0);
        window.setGravity(Gravity.CENTER);
        window.setContentView(viewPager);
        viewPager.setCurrentItem(position);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }

    private static class ImageViewPagerAdapter extends PagerAdapter {

        private Context context;
        private List<String> urlList;
        private View.OnClickListener onClickListener;

        public ImageViewPagerAdapter(Context context, List<String> urlList) {
            this.context = context;
            this.urlList  = urlList;
        }

        @Override
        public int getCount() {
            return urlList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public void setOnImageClickListener(View.OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            String url = urlList.get(position);
            PhotoView photoView = new PhotoView(context);
            photoView.setOnClickListener(onClickListener);
            photoView.setMaximumScale(10);
            photoView.setMinimumScale(0.5f);
            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .placeholder(R.drawable.ic_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(context)
                    .load(url)
                    .apply(options)
                    .into(photoView);
            container.addView(photoView);
            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
