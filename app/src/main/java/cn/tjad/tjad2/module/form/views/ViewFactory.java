package cn.tjad.tjad2.module.form.views;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.module.common.widget.DatePickTextView;
import cn.tjad.tjad2.module.form.ViewType;

public class ViewFactory {

    public static View createView(Context context, FormDetail.FormStepDetail formStepDetail) {
        FormDetail.Detail detail = formStepDetail.detail;
        switch (detail.control) {
            case ViewType.TEXT:
                FormText formText = new FormText(context);
                formText.setFormStepDetail(formStepDetail);
                return formText;
            case ViewType.SINGLE_LINE_TEXT:
                SingleLineTextView editText = new SingleLineTextView(context);
                editText.setFormStepDetail(formStepDetail);
                return editText;
            case ViewType.MULTI_LINE_TEXT:
                MultiLineTextView editText2 = new MultiLineTextView(context);
                editText2.setFormStepDetail(formStepDetail);
                return editText2;
            case ViewType.SPINNER:
                SpinnerView spinner = new SpinnerView(context);
                spinner.setFormStepDetail(formStepDetail);
                return spinner;
            case ViewType.COMBOX:
                Combox combox = new Combox(context);
                combox.setFormStepDetail(formStepDetail);
                return combox;
            case ViewType.VIEWPORT:
                ViewPortView viewPortView = new ViewPortView(context);
                viewPortView.setId(R.id.view_port);
                viewPortView.setFormStepDetail(formStepDetail);
                return viewPortView;
            case ViewType.ATTACHMENT:
                AttachmentSelectWidget attachmentSelectWidget = new AttachmentSelectWidget(context);
                attachmentSelectWidget.setId(R.id.attach_select_widget);
                attachmentSelectWidget.setFormStepDetail(formStepDetail);
                return attachmentSelectWidget;
            case ViewType.SELECT_USER:
                UserSelectWidget userSelectWidget = new UserSelectWidget(context);
                userSelectWidget.setId(R.id.user_select_widget);
                userSelectWidget.setFormStepDetail(formStepDetail);
                return userSelectWidget;
            case ViewType.DATE_PICKER:
                DatePickView datePickView = new DatePickView(context);
                datePickView.setFormStepDetail(formStepDetail);
                return datePickView;
            default:
                return null;
        }
    }
}
