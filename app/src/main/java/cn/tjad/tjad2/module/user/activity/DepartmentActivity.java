package cn.tjad.tjad2.module.user.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.user.fragment.MemberDepartmentFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.MEMBER_DEPARTMENT)
public class DepartmentActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MemberDepartmentFragment fragment = new MemberDepartmentFragment();
        Bundle args = getIntent().getExtras();
        fragment.setArguments(args);
        setContentView(fragment);
    }
}
