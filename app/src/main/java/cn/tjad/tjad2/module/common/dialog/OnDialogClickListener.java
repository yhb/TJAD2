package cn.tjad.tjad2.module.common.dialog;

import android.app.Dialog;

public interface OnDialogClickListener {
    /**
     *
     * @param dialog
     * @param which
     * @return true：关闭dialog, false:不关闭
     */
    boolean onClick(Dialog dialog, int which);
}
