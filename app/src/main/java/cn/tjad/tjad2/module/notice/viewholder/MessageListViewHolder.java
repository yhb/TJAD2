package cn.tjad.tjad2.module.notice.viewholder;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.MessageBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class MessageListViewHolder extends SimpleBaseViewHolder<MessageBean> {

    @BindView(R.id.message_content)
    TextView messageContentTV;

    public MessageListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(MessageBean data, int position) {
        messageContentTV.setText(data.text);
        if (data.read) {
            messageContentTV.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        } else {
            messageContentTV.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }
    }
}
