package cn.tjad.tjad2.module.design.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.DesignDetail;
import cn.tjad.tjad2.data.bean.MultiItemBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.common.widget.PictureLoadView;

public class DesignFinishViewHolder extends SimpleBaseViewHolder<MultiItemBean> {
    @BindView(R.id.design_detail_finish_user)
    TextView userTV;
    @BindView(R.id.design_detail_item_finish_time)
    TextView timeTV;
    @BindView(R.id.design_detail_item_finish_content)
    TextView contentTV;
    @BindView(R.id.design_detail_finish_item_picture_load_view)
    PictureLoadView pictureLoadView;

    public DesignFinishViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(MultiItemBean data, int position) {
        DesignDetail item = (DesignDetail) data.getData();
        if (item.finishUser != null) {
            userTV.setText(item.finishUser.name);
        } else {
            userTV.setText("");
        }
        timeTV.setText(item.finishTime);
        contentTV.setText(item.finishContent);
        pictureLoadView.setImageUrls(item.finishPhotos);
    }
}
