package cn.tjad.tjad2.module.common.fragment;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;

import com.qiandian.android.base.fragment.BaseFragment;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.Pagination;

public abstract class RefreshableListFragment extends BaseFragment implements OnRefreshLoadMoreListener {

    @BindView(R.id.refresh_layout)
    protected SmartRefreshLayout refreshLayout;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.headview)
    ViewStub headView;

    protected int startIndex;
    protected int PAGE_COUNT = 10;
    protected boolean isRefresh;
    protected SimpleRecyclerViewAdapter adapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = getAdapter();
//        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        refreshLayout.setOnRefreshLoadMoreListener(this);
    }

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.refreshable_list_layout, null);
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        isRefresh = false;
        requestData();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        isRefresh = true;
        startIndex = 0;
        requestData();
    }

    public void onDataLoadSuccess(Pagination pagination) {
        if (pagination == null) {
            refreshLayout.finishRefresh(true);
            return;
        }
        startIndex = startIndex + pagination.limit;
        if (getListSize() >= pagination.totalItems) {
            refreshLayout.finishLoadMoreWithNoMoreData();
        } else {
            refreshLayout.finishLoadMore(true);
        }
        refreshLayout.finishRefresh(true);
    }

    public void onDataLoadFailed() {
        refreshLayout.finishRefresh(false);
        refreshLayout.finishLoadMore(false);
    }

    protected abstract void requestData();

    protected int getListSize() {
        return adapter.getItemCount();
    }

    protected abstract SimpleRecyclerViewAdapter getAdapter();

    protected void addHeadView(@LayoutRes int layoutId) {
        headView.setLayoutResource(layoutId);
        headView.inflate();
    }
}
