package cn.tjad.tjad2.module.web;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;


public class TJWebChromeClient extends WebChromeClient {

    private boolean showProgress;
    private ProgressBar progressBar;

    public void setProgress(boolean showProgress, ProgressBar progressBar) {
        this.showProgress = showProgress;
        this.progressBar = progressBar;
        if (showProgress && progressBar == null) {
            throw new RuntimeException("showProgress为true的时候progressBar不能为空");
        }
    }

    @Override
    public void onProgressChanged(WebView webView, int i) {
        super.onProgressChanged(webView, i);
        if (showProgress) {
            progressBar.setProgress(i);
        }
    }
}
