package cn.tjad.tjad2.module.common.viewholder;

import android.view.View;

public class SimpleViewHolder<T> extends SimpleBaseViewHolder<T> {

    public SimpleViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setData(Object data, int position) {

    }
}
