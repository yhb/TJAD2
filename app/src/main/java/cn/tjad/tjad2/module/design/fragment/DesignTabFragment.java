package cn.tjad.tjad2.module.design.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.bean.CommonListBean;
import com.qiandian.android.base.fragment.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.DesignItem;
import cn.tjad.tjad2.data.event.DesignRefreshEvent;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

public class DesignTabFragment extends BaseFragment {

    @BindView(R.id.tab_layout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private List<Fragment> fragmentList;

    private final String[] tabStrings = new String[] {"与我相关", "所有问题"};

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.tab_layout, null);
    }

    @Override
    protected boolean showTitle() {
        return true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        titleView.setCenterText("问题销项");
        titleView.showLeft(false);
        fragmentList = new ArrayList<>();
        fragmentList.add(DesignFragment.newInstance(0));
        fragmentList.add(DesignFragment.newInstance(1));
        tabLayout.setViewPager(viewPager, tabStrings, getChildFragmentManager(), fragmentList);
    }

    private int count0, count1;

    public void updateCount(int tabType, int count, boolean isTotal) {
        String tabText = tabStrings[tabType];
        tabText = tabText + "(" + count + ")";
        tabLayout.getTitleView(tabType).setText(tabText);
        if (tabType == 0) {
            count0 = count;
        } else if (tabType == 1) {
            count1 = count;
        }
        if (tabType == 1 && isTotal) {
            setTitleCount(count1);
        }
    }

    public void setTitleCount(int count) {
        String title = "问题销项(" + count + ")";
        titleView.setCenterText(title);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onRefreshCount(DesignRefreshEvent event) {
        Log.d("yanghongbing", "刷新所有问题个数");
        RetrofitManager.getInstance().getApiService().
                getDesignList(Tools.getProjectId(), 0, 1, "", "", "", "")
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<ArrayList<DesignItem>>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<ArrayList<DesignItem>> t) {
                        if (t.resCode == 0) {
                            CommonListBean listBean = (CommonListBean) t;
                            if (listBean.pagination != null) {
                                setTitleCount(listBean.pagination.totalItems);
                            }
                        } else {
                            Tools.showToast(t.respMsg);
                        }
                    }

                    @Override
                    public void onFailed(String message) {
                        Tools.showToast(message);
                    }
                });
    }
}
