package cn.tjad.tjad2.module.common.widget;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.AppUtils;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.Tools;

public class PictureLoadView extends LinearLayout implements View.OnClickListener {

    private List<String> imageUrls;

    public PictureLoadView(Context context) {
        super(context);
        init();
    }

    public PictureLoadView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setOnClickListener(this);
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
        loadPictures();
    }

    private void loadPictures() {
        removeAllViews();
        if (this.imageUrls != null && this.imageUrls.size() > 0) {
            for (String url : imageUrls) {
                ImageView imageView = new ImageView(getContext());
                imageView.setImageResource(R.drawable.ic_placeholder);
                imageView.setScaleType(ImageView.ScaleType.CENTER);
                int height = getMinimumHeight();
                LayoutParams layoutParams = new LayoutParams(height, height);
                layoutParams.leftMargin = AppUtils.getDimens(R.dimen.dp_2);
                Glide.with(this).load(url).into(imageView);
                this.addView(imageView, layoutParams);
            }
        }
    }


    @Override
    public void onClick(View view) {
        if (imageUrls != null && imageUrls.size() > 0) {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(Keys.PICTURE_URL_LIST, (ArrayList<String>) imageUrls);
            RouteUtils.toPage(getContext(), PageId.PICTURE_PREVIEW, bundle);
        } else {
            Tools.showToast("没有照片");
        }
    }
}
