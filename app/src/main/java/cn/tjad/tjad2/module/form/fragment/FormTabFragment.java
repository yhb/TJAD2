package cn.tjad.tjad2.module.form.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;

@Route(path = PageId.FORM_TAB)
public class FormTabFragment extends BaseFragment {

    @BindView(R.id.tab_layout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.float_action_bar)
    FloatingActionButton floatingActionButton;

    private final String[] tabStrings = {"与我相关", "所有"};
    private List<Fragment> fragmentList;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.tab_layout, null);
    }

    @Override
    protected boolean showTitle() {
        return true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        floatingActionButton.setVisibility(View.VISIBLE);
        fragmentList = new ArrayList<>();
        fragmentList.add(FormListFragment.newInstance(0));
        fragmentList.add(FormListFragment.newInstance(1));
        tabLayout.setViewPager(viewPager, tabStrings, getChildFragmentManager(), fragmentList);
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("流程表单");
        titleView.showLeft(true);
    }

    @OnClick(R.id.float_action_bar)
    public void onCreateForm(View view) {
        RouteUtils.toFragmentActivity(getContext(), PageId.FORM_CREATE, null);
    }
}
