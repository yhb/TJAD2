package cn.tjad.tjad2.module.form.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.bean.CommonListBean;

import java.util.ArrayList;

import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.FormBean;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.fragment.RefreshableListFragment;
import cn.tjad.tjad2.module.form.adapter.FormListAdapter;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

public class FormListFragment extends RefreshableListFragment implements OnItemClickListener {

    private int type = 0;
    private FormListAdapter formAdapter;

    public static FormListFragment newInstance(int type) {
        Bundle bundle = new Bundle();
        bundle.putInt("tabType", type);
        FormListFragment fragment = new FormListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected void requestData() {
        RetrofitManager.getInstance()
                .getApiService()
                .getFormList(Tools.getProjectId(), startIndex, PAGE_COUNT)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<ArrayList<FormBean>>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<ArrayList<FormBean>> t) {
                        CommonListBean<FormBean> bean = (CommonListBean<FormBean>) t;
                        formAdapter.updateData(t.result, !isRefresh);
                        formAdapter.notifyDataSetChanged();
                        onDataLoadSuccess(bean.pagination);
                    }

                    @Override
                    public void onFailed(String message) {
                        onDataLoadFailed();
                    }
                });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshLayout.autoRefresh();
    }

    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        formAdapter = new FormListAdapter(new ArrayList());
        formAdapter.setOnItemClickListener(this);
        return formAdapter;
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        FormBean formBean = (FormBean) formAdapter.getItemData(position);
        Bundle bundle = new Bundle();
        bundle.putInt(Keys.FORM_ID, formBean.id);
        RouteUtils.toFragmentActivity(getContext(), PageId.FORM_DETAIL, bundle);
    }
}
