package cn.tjad.tjad2.module.form.views;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.module.common.widget.DatePickTextView;

import static android.util.TypedValue.COMPLEX_UNIT_SP;

public class FormText extends FormBaseLayout {

    private FormDetail.Detail detail;

    public FormText(Context context) {
        super(context);
    }

    public FormText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void init() {
        super.init();
        setOrientation(HORIZONTAL);
    }

    @Override
    public void setFormStepDetail(FormDetail.FormStepDetail formStepDetail) {
        super.setFormStepDetail(formStepDetail);
        this.setDetail(formStepDetail.detail);
    }

    private void setDetail(FormDetail.Detail detail) {
        this.detail = detail;
        createViews();
    }

    private void createViews() {
        this.removeAllViews();
        if (detail == null) {
            return;
        }
        if (!detail.textType) {
            TextView textView = genTextView();
            textView.setText(detail.text);
            this.addView(textView);
        } else {
            View part1 = getViewByType(detail.textPart1, detail.text1);
            if (part1 != null) {
                this.addView(part1);
                View part2 = getViewByType(detail.textPart2, detail.text2);
                if (part2 != null) {
                    this.addView(genConcatView());
                    this.addView(part2);
                    View par3 = getViewByType(detail.textPart3, detail.text3);
                    if (par3 != null) {
                        this.addView(genConcatView());
                        this.addView(par3);
                    }
                }
            }
        }
    }

    private View getViewByType(int type, String texts) {
        if (type == 1) {
            TextView textView = genTextView();
            textView.setText(texts);
            return textView;
        } else if (type == 2) {
            TextView datePickTextView = new TextView(getContext());
            datePickTextView.setTextColor(Color.parseColor("#101010"));
            datePickTextView.setTextSize(COMPLEX_UNIT_SP, 14);
            datePickTextView.setGravity(Gravity.CENTER);
            datePickTextView.setEnabled(false);
            datePickTextView.setText(texts);
            return datePickTextView;
        } else if (type > 2) {
            Spinner spinner = new Spinner(getContext());
            spinner.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            spinner.setEnabled(this.detail.textType);
            String[] elements = texts.split(",");
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, elements);
            spinner.setAdapter(spinnerAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String value = "";
                    for (int i = 0; i < getChildCount(); i ++) {
                        View child = getChildAt(i);
                        if (child instanceof TextView) {
                            if (!" -- ".equals(((TextView) child).getText())) {
                                value += ((TextView) child).getText() + "-";
                            }
                        } else if (child instanceof Spinner) {
                            ArrayAdapter<String> adapter = (ArrayAdapter<String>) ((Spinner) child).getAdapter();
                            value += adapter.getItem(position) + "-";
                        }
                    }
                    if (value.endsWith("-")) {
                        value = value.substring(0, value.length() - 2);
                    }
                    formStepDetail.value = value;
                    if (onEditListener != null) {
                        onEditListener.onEdit(formStepDetail.detail.id, value);
                    }
                    Log.d("TJAD", "FormText: " + value);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            String select = null;
            if (formStepDetail.value != null) {
                String[] parts = formStepDetail.value.split("-");
                if (parts.length >= 3) {
                    select = parts[2];
                }
            }
            int index = 0;
            for (int i = 0; i < elements.length; i++) {
                if ((select + "F").equals(elements[i])) {
                    index = i;
                    break;
                }
            }
            spinner.setSelection(index);
            return spinner;
        }
        return null;
    }

    private TextView genTextView() {
        TextView textView = new TextView(getContext());
        textView.setTextColor(Color.parseColor("#101010"));
        textView.setTextSize(COMPLEX_UNIT_SP, 14);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    private View genConcatView() {
        TextView textView = genTextView();
        textView.setText(" -- ");
        return textView;
    }


}
