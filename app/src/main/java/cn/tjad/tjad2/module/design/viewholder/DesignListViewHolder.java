package cn.tjad.tjad2.module.design.viewholder;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.DesignItem;
import cn.tjad.tjad2.utils.AppUtils;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import io.reactivex.Observable;

public class DesignListViewHolder extends SimpleBaseViewHolder<DesignItem> {

    @BindView(R.id.design_item_image)
    ImageView imageView;
    @BindView(R.id.design_item_content)
    TextView contentTV;
    @BindView(R.id.design_item_emergency)
    TextView emergencyTV;
    @BindView(R.id.design_item_type)
    TextView typeTV;
    @BindView(R.id.design_item_floor)
    TextView floorTV;
    @BindView(R.id.design_item_status)
    TextView statusTV;
    @BindView(R.id.design_item_field_container)
    LinearLayout fieldContainer;
    @BindView(R.id.design_item_delay)
    TextView delayTV;

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public DesignListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(DesignItem data, int position) {
        String photoUrl = null;
        if (!TextUtils.isEmpty(data.snapshot)) {
            photoUrl = data.snapshot;
        } else if (data.finishPhotos != null && data.finishPhotos.size() > 0) {
            photoUrl = data.finishPhotos.get(0);
        }
        if (!TextUtils.isEmpty(photoUrl)) {
            Glide.with(itemView)
                    .load(photoUrl)
                    .into(imageView);
        } else {
            imageView.setImageResource(R.drawable.design_item_defalt_icon);
        }
        contentTV.setText(data.content);
        String emergencyText = Tools.getDesignPriority(data.priority);
        emergencyTV.setText(emergencyText);
        emergencyTV.setTextColor(Tools.getPriorityColor(data.priority));
        if (data.type != null) {
            typeTV.setText(data.type.name);
        } else {
            typeTV.setText("");
        }
        if (data.floor != null) {
            floorTV.setText(data.floor.name);
        } else {
            floorTV.setText("");
        }
        String status = Tools.getConsStatusByCode(data.consStatus);
        int backgroundRes = Tools.getDesignStatusBackground(data.consStatus);
        statusTV.setText(status);
        statusTV.setBackgroundResource(backgroundRes);
        fieldContainer.removeAllViews();
        if (data.domains != null && data.domains.size() >0) {
            Observable.fromIterable(data.domains)
                    .map(s -> s.substring(0, 1))
                    .doOnNext(s -> {
                        TextView textView = new TextView(itemView.getContext());
                        textView.setTextSize(11);
                        textView.setGravity(Gravity.CENTER);
                        textView.setBackground(AppUtils.getDrawable(R.drawable.de_sign_item_label));
                        textView.setTextColor(AppUtils.getColor(R.color.design_blue));
                        int horizontalPadding = AppUtils.getDimens(R.dimen.dp_8);
                        int verticalPadding = AppUtils.getDimens(R.dimen.dp_1);
                        textView.setPadding(horizontalPadding,verticalPadding, horizontalPadding, verticalPadding);
                        textView.setText(s);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                        lp.rightMargin = 5;
                        fieldContainer.addView(textView, lp);
                    })
                    .subscribe();
        }
        String delay = null;
        int color = Color.TRANSPARENT;
        try {
            if (data.planFinishTime != null && data.consStatus == 1) {
                Calendar today = Calendar.getInstance();
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);
                Calendar planFinishDate = Calendar.getInstance();
                planFinishDate.setTime(dateFormat.parse(data.planFinishTime));
                long offset = planFinishDate.getTimeInMillis() - today.getTimeInMillis();
                int days = (int)(offset / (1000 * 3600 * 24));
                if (days < 0) {
                    delay = "已延期" + (-days) + "天";
                    color = AppUtils.getColor(R.color.design_delay);
                } else if (days > 0){
                    delay = "剩余" + days + "天";
                    color = AppUtils.getColor(R.color.design_not_delay);
                } else {
                    delay = "剩余0天";
                    color = AppUtils.getColor(R.color.design_not_delay);
                }
            }
        } catch (ParseException e) {
            Log.d("designItem", "返回的计划完成日期不正确");
        }
        delayTV.setText(delay);
        delayTV.setTextColor(color);
    }
}
