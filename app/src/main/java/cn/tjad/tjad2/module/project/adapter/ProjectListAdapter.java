package cn.tjad.tjad2.module.project.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.Project;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.project.viewholder.ProjectListViewHolder;

public class ProjectListAdapter extends SimpleRecyclerViewAdapter<Project> {


    public ProjectListAdapter(List<Project> data) {
        super(data);
    }
    private int checkedPosision = -1;

    @Override
    protected SimpleBaseViewHolder<Project> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_list_item, new FrameLayout(parent.getContext()), false);
        return new ProjectListViewHolder(view);
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("项目列表为空", 0);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleBaseViewHolder<Project> holder, int position) {
        if (holder instanceof ProjectListViewHolder) {
            ((ProjectListViewHolder) holder).setCheckedPosition(checkedPosision);
        }
        super.onBindViewHolder(holder, position);
    }

    public void setCheckedPosition(int position) {
        this.checkedPosision = position;
        notifyDataSetChanged();
    }

    public int getCheckedPosision() {
        return checkedPosision;
    }
}
