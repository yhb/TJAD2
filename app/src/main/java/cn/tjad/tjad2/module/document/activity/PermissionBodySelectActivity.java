package cn.tjad.tjad2.module.document.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.document.fragment.PermissionBodySelectFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.PERMISSION_BODY_SELECT)
public class PermissionBodySelectActivity extends BaseActivity {

    private PermissionBodySelectFragment permissionBodySelectFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissionBodySelectFragment = new PermissionBodySelectFragment();
        permissionBodySelectFragment.setArguments(getIntent().getExtras());
        setContentView(permissionBodySelectFragment);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
