package cn.tjad.tjad2.module.document.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hjy.http.upload.FileUploadTask;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.document.viewholder.UploadViewHolder;

public class UploadListAdapter extends SimpleRecyclerViewAdapter<FileUploadTask> {
    public UploadListAdapter(List<FileUploadTask> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<FileUploadTask> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_transfer_list_item, parent, false);
        UploadViewHolder viewHolder = new UploadViewHolder(view);
        return viewHolder;
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("上传列表为空", 0);
    }
}
