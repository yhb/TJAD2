package cn.tjad.tjad2.module.form.entity;

public class RejectFormRequestBean {
    public String description;
    public int rejectType;
    public String rejectFileIds;
}
