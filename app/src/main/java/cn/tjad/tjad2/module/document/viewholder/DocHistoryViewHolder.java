package cn.tjad.tjad2.module.document.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.utils.AppUtils;

public class DocHistoryViewHolder extends SimpleBaseViewHolder<FileBean> {

    @BindView(R.id.doc_history_item_version)
    TextView versionTV;
    @BindView(R.id.doc_history_item_creator)
    TextView creatorTV;
    @BindView(R.id.doc_history_item_create_time)
    TextView createTimeTV;
    @BindView(R.id.doc_history_item_operate)
    TextView operateTV;

    public DocHistoryViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(FileBean data, int position) {
        if (data == null) {
            return;
        }
        versionTV.setText(String.valueOf(data.version));
        if (data.createUser != null) {
            creatorTV.setText(data.createUser.name);
        } else {
            creatorTV.setText("");
        }
        if (data.createTime != null) {
            createTimeTV.setText(data.createTime);
        } else {
            createTimeTV.setText("");
        }
        operateTV.setText("下载");
        operateTV.setTextColor(AppUtils.getColor(R.color.blue));
    }
}
