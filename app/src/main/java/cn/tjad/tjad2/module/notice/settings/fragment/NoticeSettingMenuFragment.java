package cn.tjad.tjad2.module.notice.settings.fragment;

import android.os.Bundle;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.utils.JsonUtils;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.CommonEntity;
import cn.tjad.tjad2.module.common.fragment.SimpleRefreshRecyclerViewFragment;
import cn.tjad.tjad2.module.notice.settings.adapter.NoticeSettingMenuAdapter;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.ConfigReader;
import cn.tjad.tjad2.utils.RouteUtils;

@Route(path = PageId.MESSAGE_SETTING_MENU)
public class NoticeSettingMenuFragment extends SimpleRefreshRecyclerViewFragment {

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("消息设置");
    }

    private List<CommonEntity> getMenu() {
        String message = ConfigReader.read(R.raw.notice_setting_menu);
        List<CommonEntity> entityList = JsonUtils.parseArray(message, CommonEntity.class);
        return entityList;
    }

    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        return new NoticeSettingMenuAdapter(getMenu());
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        CommonEntity commonEntity = (CommonEntity) adapter.getItemData(position);
        Bundle bundle = new Bundle();
        bundle.putInt(Keys.MESSAGE_TYPE, commonEntity.id);
        bundle.putString(Keys.MESSAGE_TYPE_NAME, commonEntity.name);
        RouteUtils.toFragmentActivity(getContext(), PageId.MESSAGE_SETTINGS, bundle);
    }
}
