package cn.tjad.tjad2.module.user.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Member;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class MemberListViewHolder extends SimpleBaseViewHolder<Member> {

    @BindView(R.id.member_name)
    TextView memberName;
    public MemberListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(Member data, int position) {
        memberName.setText(data.name);
    }
}
