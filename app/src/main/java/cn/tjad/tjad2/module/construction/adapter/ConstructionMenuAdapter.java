package cn.tjad.tjad2.module.construction.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.PageMenu;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class ConstructionMenuAdapter extends SimpleRecyclerViewAdapter<PageMenu> {

    public ConstructionMenuAdapter(List<PageMenu> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<PageMenu> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.page_menu, parent, false);
        return new ConstructionMenuViewHolder(view);
    }

    static class ConstructionMenuViewHolder extends SimpleBaseViewHolder<PageMenu> {

        @BindView(R.id.page_menu_name)
        TextView nameTV;

        public ConstructionMenuViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setData(PageMenu data, int position) {
            nameTV.setText(data.name);
        }


    }
}
