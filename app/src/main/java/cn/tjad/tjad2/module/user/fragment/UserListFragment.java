package cn.tjad.tjad2.module.user.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.qiandian.android.base.fragment.BaseFragment;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.StringListAdapter;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.Tools;

public class UserListFragment extends BaseFragment implements OnItemClickListener {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    private StringListAdapter adapter;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.common_recylerview, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new StringListAdapter(null);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        adapter.setOnItemClickListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            List<Serializable> userList = (List<Serializable>) bundle.getSerializable(Keys.BODY);
            if (userList == null) {
                Tools.showToast("没有传入有效的参数");
                getActivity().finish();
            }
            adapter.updateData(userList);
        }
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        Serializable item = adapter.getItemData(position);
        if (item instanceof User) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("user", item);
            RouteUtils.toPage(getContext(), PageId.MEMBER_USER_DETAIL, bundle);
        }
    }
}
