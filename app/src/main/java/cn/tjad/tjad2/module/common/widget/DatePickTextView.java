package cn.tjad.tjad2.module.common.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;

import com.bruce.pickerview.popwindow.DatePickerPopWin;

import cn.tjad.tjad2.utils.DatePickUtils;

public class DatePickTextView extends AppCompatTextView implements View.OnClickListener, DatePickerPopWin.OnDatePickedListener {
    public DatePickTextView(Context context) {
        this(context, null);
    }

    public DatePickTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        DatePickUtils.showDatePicker(getContext(), getText().toString(), this);
    }

    @Override
    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
        setText(dateDesc);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            setOnClickListener(this);
        } else {
            setOnClickListener(null);
        }
    }

}
