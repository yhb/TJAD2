package cn.tjad.tjad2.module.plan.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Task;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.utils.AppUtils;
import cn.tjad.tjad2.utils.Tools;

public class PlanTaskListViewHolder extends SimpleBaseViewHolder<Task> {

    @BindView(R.id.plan_task_item_name)
    TextView nameTV;
    @BindView(R.id.plan_task_item_plan_time)
    TextView planTimeTV;
    @BindView(R.id.plan_task_item_actual_time)
    TextView actualTimeTV;
    @BindView(R.id.plan_task_item_related)
    TextView relatedTV;
    @BindView(R.id.plan_task_item_status)
    TextView statusTV;
    @BindView(R.id.plan_task_item_delay)
    TextView delayTV;
    @BindView(R.id.plan_task_item_right_icon)
    ImageView rightIcon;

    public PlanTaskListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(Task data, int position) {
        nameTV.setText(data.name);
        if (data.planStartTime == null) {
            data.planStartTime = "";
        }
        if (data.planEndTime == null) {
            data.planEndTime = "";
        }
        if (data.actualStartTime == null) {
            data.actualEndTime = "";
        }
        if (data.actualEndTime == null) {
            data.actualEndTime = "";
        }
        planTimeTV.setText(AppUtils.getString(R.string.plan_time, data.planStartTime, data.planEndTime));
        actualTimeTV.setText(AppUtils.getString(R.string.actual_time, data.actualStartTime, data.actualEndTime));
        if (data.relates != null && data.relates.size() > 0) {
            relatedTV.setText("已关联构件");
        } else {
            relatedTV.setText("未关联构件");
        }
        statusTV.setText(Tools.getTaskStatus(data.status));
        statusTV.setBackgroundResource(Tools.getTaskStatusDrawable(data.status));
        if (data.delay) {
            delayTV.setText("已滞后");
            delayTV.setTextColor(AppUtils.getColor(R.color.plan_task_delay_color));
            delayTV.setBackgroundResource(R.drawable.plan_task_delay);
        } else {
            delayTV.setText("未滞后");
            delayTV.setTextColor(AppUtils.getColor(R.color.plan_task_no_delay_color));
            delayTV.setBackgroundResource(R.drawable.plan_task_no_delay);
        }
        if (data.children != null && data.children.size() > 0) {
            rightIcon.setImageResource(R.mipmap.icon_right);
            rightIcon.setVisibility(View.VISIBLE);
        } else {
            rightIcon.setImageBitmap(null);
            rightIcon.setVisibility(View.GONE);
        }
    }
}
