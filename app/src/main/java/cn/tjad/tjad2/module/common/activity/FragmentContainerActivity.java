package cn.tjad.tjad2.module.common.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.FRAGMENT_CONTAINER_ACTIVITY)
public class FragmentContainerActivity extends BaseActivity {

    private FrameLayout container;

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        container = new FrameLayout(this);
        container.setId(container.hashCode());
        setContentView(container);
        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        String fragmentId = intent.getStringExtra(Keys.FRAGMENT_ID);
        Bundle args = intent.getBundleExtra(Keys.PAGE_ARGS);
        Fragment fragment = (Fragment) ARouter.getInstance().build(fragmentId)
                .with(args).navigation();
        if (fragment == null) {
            Tools.showToast("没有找到对应的Fragment");
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(container.getId(), fragment)
                    .commit();
        }
    }
}
