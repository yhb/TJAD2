package cn.tjad.tjad2.module.notice.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.bean.CommonListBean;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.util.ArrayList;

import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.MessageBean;
import cn.tjad.tjad2.module.common.fragment.RefreshableListFragment;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.notice.adapter.MessageListViewAdapter;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.MESSAGE_LIST)
public class MessageListFragment extends RefreshableListFragment implements OnItemClickListener {

    private int type = -1;
    private String typeName;

    private int updatePosition; //记录需要更新的条目

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            type = bundle.getInt(Keys.MESSAGE_TYPE, -1);
            typeName = bundle.getString(Keys.MESSAGE_TYPE_NAME);
            if (type == -1) {
                Tools.showToast("缺少消息类型");
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter.setOnItemClickListener(this);
        requestData();
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        if (TextUtils.isEmpty(typeName)) {
            titleView.setCenterText("消息列表");
        } else {
            titleView.setCenterText(typeName);
        }
    }

    @Override
    protected void requestData() {
        RetrofitManager.getInstance().getApiService()
                .getMessageList(Tools.getProjectId(), type, startIndex, PAGE_COUNT)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<ArrayList<MessageBean>>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<ArrayList<MessageBean>> t) {
                        CommonListBean<MessageBean> list = (CommonListBean<MessageBean>) t;
                        adapter.updateData(t.result, !isRefresh);
                        adapter.notifyDataSetChanged();
                        onDataLoadSuccess(list.pagination);

                    }

                    @Override
                    public void onFailed(String message) {
                        onDataLoadFailed();
                    }
                });
    }

    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        return new MessageListViewAdapter(null);
    }

    private void setAsRead(int messageId) {
        RetrofitManager.getInstance().getApiService()
                .readMessage(Tools.getProjectId(), messageId)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<BaseBean>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<BaseBean> t) {
                        MessageBean messageBean = (MessageBean) adapter.getItemData(updatePosition);
                        if (messageBean != null) {
                            messageBean.read = true;
                            adapter.notifyItemChanged(updatePosition);
                            updatePosition = -1;
                        }
                    }

                    @Override
                    public void onFailed(String message) {
                        Tools.showToast("设置已读失败");
                        updatePosition = -1;
                    }
                });
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        updatePosition = position;
        MessageBean messageBean = (MessageBean) adapter.getItemData(position);
        setAsRead(messageBean.id);
    }
}
