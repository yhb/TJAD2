package cn.tjad.tjad2.module.notice.settings.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.CommonEntity;
import cn.tjad.tjad2.module.notice.settings.viewholder.NoticeSettingMenuViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class NoticeSettingMenuAdapter extends SimpleRecyclerViewAdapter<CommonEntity> {
    public NoticeSettingMenuAdapter(List<CommonEntity> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<CommonEntity> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_setting_menu_item, parent, false);
        return new NoticeSettingMenuViewHolder(view);
    }
}
