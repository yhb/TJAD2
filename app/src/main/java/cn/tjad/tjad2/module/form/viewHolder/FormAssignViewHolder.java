package cn.tjad.tjad2.module.form.viewHolder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;


//type = 3
public class FormAssignViewHolder extends SimpleBaseViewHolder<FormDetail.FormDetailStep> {

    @BindView(R.id.form_assign_name)
    TextView nameTV;

    public FormAssignViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(FormDetail.FormDetailStep data, int position) {
        if (data.assignUser != null) {
            nameTV.setText(data.assignUser.name);
        } else {
            nameTV.setText("--");
        }
    }
}
