package cn.tjad.tjad2.module.material.viewholder;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.MaterialBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class MaterialTrackingViewHolder extends SimpleBaseViewHolder<MaterialBean> {

    @BindView(R.id.material_item_name)
    TextView nameTV;
    @BindView(R.id.material_item_detail)
    TextView detailTV;
    @BindView(R.id.material_item_related_models)
    TextView relatedModelsTV;
    @BindView(R.id.material_item_latest_update_time)
    TextView lastUpdateTimeTV;

    private final int[] colors = {
            Color.parseColor("#16B7F7"),
            Color.parseColor("#FF9800")
    };

    public MaterialTrackingViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(MaterialBean data, int position) {
        nameTV.setText(data.name);
        lastUpdateTimeTV.setText(data.modifyTime);
        StringBuilder sb = new StringBuilder();
        if (data.relateFileNames != null && data.relateFileNames.size() > 0) {
            for (int i = 0; i < data.relateFileNames.size(); i++) {
                sb.append(data.relateFileNames.get(i));
                if (i < data.relateFileNames.size() - 1) {
                    sb.append(" ");
                }
            }
        }
        relatedModelsTV.setText(sb.toString());
        detailTV.setText("");
        if (data.details != null && data.details.size() > 0) {
            for (int i = 0; i < data.details.size(); i++) {
                MaterialBean.Detail detail = data.details.get(i);
                String text = detail.name + " " + detail.percentage;
                SpannableString spannableString = new SpannableString(text);
                spannableString.setSpan(new ForegroundColorSpan(colors[i % 2]), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                detailTV.append(spannableString);
                if (i < data.details.size() - 1) {
                    detailTV.append(" | ");
                }
            }
        }
    }
}
