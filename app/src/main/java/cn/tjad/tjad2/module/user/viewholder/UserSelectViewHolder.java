package cn.tjad.tjad2.module.user.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.lang.reflect.Field;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.data.event.OnCheckedEvent;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class UserSelectViewHolder extends SimpleBaseViewHolder<Serializable> {

    @BindView(R.id.name)
    TextView nameTV;
    @BindView(R.id.rb_check)
    CheckBox checkCB;

    private int position;
    private Serializable data;

    public UserSelectViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setChecked(boolean checked) {
        checkCB.setChecked(checked);
    }

    @OnCheckedChanged(R.id.rb_check)
    public void onCheckedChanged(CheckBox radioButton, boolean isChecked) {
        OnCheckedEvent event = new OnCheckedEvent();
        event.setCheckedPosition(position);
        event.setChecked(isChecked);
        event.setObject(data);
        EventBus.getDefault().post(event);
    }

    @Override
    public void setData(Serializable data, int position) {
        this.position = position;
        this.data = data;
        if (data instanceof String) {
            nameTV.setText((CharSequence) data);
        } else {
            String name = null;
            try {
                Field field = data.getClass().getField("name");
                field.setAccessible(true);
                name = (String) field.get(data);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            nameTV.setText(name);
        }
        if (data instanceof User) {
            checkCB.setVisibility(View.VISIBLE);
        } else {
            checkCB.setVisibility(View.GONE);
        }
    }
}
