package cn.tjad.tjad2.module.form.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import cn.tjad.tjad2.data.bean.FormDetail;

public class FormBaseLayout extends LinearLayout {

    protected IOnEditListener onEditListener;
    protected FormDetail.FormStepDetail formStepDetail;

    public FormBaseLayout(Context context) {
        this(context, null);
    }

    public FormBaseLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    protected void init() {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);
    }

    public void setOnEditListener(IOnEditListener onEditListener) {
        this.onEditListener = onEditListener;
        if (onEditListener != null) {
            onEditListener.onEdit(formStepDetail.detail.id, formStepDetail.value);
        }
    }

    public void setFormStepDetail(FormDetail.FormStepDetail formStepDetail) {
        this.formStepDetail = formStepDetail;
    }

    public String getValue() {
        return null;
    }

    public interface IOnEditListener {
        void onEdit(int id, String value);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (getChildCount() > 0) {
            for (int i = 0; i < getChildCount(); i++) {
                View child = getChildAt(i);
                child.setEnabled(enabled);
            }
        }
    }
}
