package cn.tjad.tjad2.module.form.viewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.utils.Tools;

public class FormListViewHolder extends SimpleBaseViewHolder<FormBean> {

    @BindView(R.id.form_item_icon)
    ImageView imageView;
    @BindView(R.id.form_item_name)
    TextView nameTV;
    @BindView(R.id.form_item_step)
    TextView stepTV;
    @BindView(R.id.form_item_status)
    TextView statusTV;
    @BindView(R.id.form_item_create)
    TextView createTV;
    @BindView(R.id.form_item_update)
    TextView updateTV;

    public FormListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(FormBean data, int position) {
        nameTV.setText(data.name);
        statusTV.setText(Tools.getFormStatus(data.consStatus));
        statusTV.setBackgroundResource(Tools.getFormStatusBackgroundRes(data.consStatus));
        String stepText = "当前节点: ";
        if (data.step != null) {
            if (data.step.step != null) {
                stepText += data.step.step.name;
            } else {
                stepText +="  ";
            }
            if (data.step.createUser != null) {
                stepText += " - " + data.step.createUser.name;
            }
        } else {
            stepText += "--";
        }
        stepTV.setText(stepText);
        String createInfo = "";
        if (data.createUser != null) {
            createInfo += data.createUser.name + " ";
        }
        createInfo += "创建于";
        if (data.createTime != null) {
            createInfo += data.createTime;
        } else {
            createInfo += "--";
        }
        createTV.setText(createInfo);
        String updateInfo = "最近更新：";
        if (data.modifyTime != null) {
            updateInfo += data.modifyTime;
        } else if (data.createTime != null) {
            updateInfo += data.createTime;
        } else {
            updateInfo += "--";
        }
        updateTV.setText(updateInfo);
    }
}
