package cn.tjad.tjad2.module.common.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.CommonEntity;
import cn.tjad.tjad2.module.common.pictureselect.TJBaseAdapter;
import cn.tjad.tjad2.module.common.pictureselect.ViewHolder;


public class SingleChoicePopupWindow implements AdapterView.OnItemClickListener {

    private PopupWindow popupMenu;
    private PopMenuAdapter popMenuAdapter;
    private List<? extends CommonEntity> data = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private Context context;
    private int checkedPosition = 0;

    public SingleChoicePopupWindow(Context context, List<? extends CommonEntity> data) {
        this.context = context;
        this.data = data;
    }

    private void initPopMenu() {
        popupMenu = new PopupWindow(context);
        popupMenu.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popupMenu.setOutsideTouchable(true);
        popupMenu.setTouchable(true);
        popupMenu.setBackgroundDrawable(new ColorDrawable());
        ListView listView = new ListView(context);
        listView.setOnItemClickListener(this);
        popMenuAdapter = new PopMenuAdapter(context, data);
        listView.setAdapter(popMenuAdapter);
        listView.setBackgroundResource(R.color.white);
        popupMenu.setContentView(listView);
    }

    public void setData(List<CommonEntity> data) {
        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setCheckedPosition(int position) {
        popMenuAdapter.setCheckPosition(position);
        popMenuAdapter.notifyDataSetChanged();
    }

    public CommonEntity getCheckedEntity() {
        return (CommonEntity) popMenuAdapter.getItem(checkedPosition);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        checkedPosition = i;
        popMenuAdapter.setCheckPosition(checkedPosition);
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(this, (CommonEntity) popMenuAdapter.getItem(i));
        }
        if (popupMenu != null) {
            popupMenu.dismiss();
        }
    }

    public interface OnItemClickListener {
        void onItemClick(SingleChoicePopupWindow window, CommonEntity entity);
    }

    public void showAsDropDown(View anchor) {
        if (popupMenu == null) {
            initPopMenu();
        }
        popMenuAdapter.setCheckPosition(checkedPosition);
        popupMenu.showAsDropDown(anchor);
    }

    static class PopMenuAdapter extends TJBaseAdapter {


        private int checkedPosition = 0;

        public PopMenuAdapter(Context context, List data) {
            super(context, data);
        }

        @Override
        public ViewHolder getViewHolder(int position) {
            return new PopMenuViewHolder(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            PopMenuViewHolder popMenuViewHolder = (PopMenuViewHolder) view.getTag();
            popMenuViewHolder.setChecked(position == checkedPosition);
            return view;
        }

        public void setCheckPosition(int position) {
            this.checkedPosition = position;
        }

         static class PopMenuViewHolder extends ViewHolder<CommonEntity> {

            @BindView(R.id.checked_textview)
             CheckedTextView textView;

             public PopMenuViewHolder(Context context) {
                 super(context);
                 ButterKnife.bind(this, itemView);
             }

             public void setChecked(boolean checked) {
                 textView.setChecked(checked);
             }

             @Override
             public void setValue(CommonEntity entity, int position) {
                 textView.setText(entity.name);
             }

             @Override
             public int getLayoutId() {
                 return R.layout.single_choice_item;
             }
         }
    }


}
