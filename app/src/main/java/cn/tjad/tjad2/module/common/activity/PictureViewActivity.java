package cn.tjad.tjad2.module.common.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.luck.picture.lib.photoview.PhotoView;
import com.qiandian.android.base.activity.BaseActivity;

import java.util.List;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.module.common.adapter.PicturePreviewAdapter;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.pictureselect.ImagePreviewWindow;

/**
 * Created by yanghongbing on 17/11/5.
 */

@Route(path = PageId.PICTURE_PREVIEW)
public class PictureViewActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    @BindView(R.id.picture_gridview)
    GridView gridView;
    @BindView(R.id.picture_viewer)
    PhotoView pictureViewer;
    private List<String> pictureUrlList;
    private PicturePreviewAdapter adapter;

    private ImagePreviewWindow imagePreviewWindow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_layout);
        pictureViewer.setOnClickListener(this);
        handleIntent(getIntent());
    }

    protected void handleIntent(Intent intent) {
        pictureUrlList = intent.getStringArrayListExtra(Keys.PICTURE_URL_LIST);
        if (pictureUrlList == null || pictureUrlList.size() == 0) {
            Tools.showToast("没有有效的图片地址");
            return;
        }
        adapter = new PicturePreviewAdapter(this, pictureUrlList);
        gridView.setVisibility(View.VISIBLE);
        pictureViewer.setVisibility(View.GONE);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (imagePreviewWindow == null) {
            imagePreviewWindow = new ImagePreviewWindow(this);
        }
        imagePreviewWindow.setImageUrlList(pictureUrlList);
        imagePreviewWindow.show(position);
//        String url = pictureUrlList.get(position);
//        pictureViewer.setVisibility(View.VISIBLE);
//        RequestOptions options = new RequestOptions()
//                .centerCrop()
//                .placeholder(R.drawable.ic_placeholder)
//                .diskCacheStrategy(DiskCacheStrategy.ALL);
//        Glide.with(this)
//                .load(url)
//                .apply(options)
//                .into(pictureViewer);
    }

    @Override
    public void onClick(View view) {
        if (view == pictureViewer) {
            pictureViewer.setVisibility(View.GONE);
        }
    }
}
