package cn.tjad.tjad2.module.construction.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.fragment.BaseFragment;

import java.util.ArrayList;

import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.PageMenu;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.construction.adapter.ConstructionMenuAdapter;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;

@Route(path = PageId.CONSTRUCTION_MENU)
public class SimpleMenuFragment extends BaseFragment implements OnItemClickListener {

    private RecyclerView recyclerView;
    private ConstructionMenuAdapter adapter;

    @Override
    public View getContentView(LayoutInflater inflater) {
        recyclerView = new RecyclerView(getContext());
        return recyclerView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        Bundle args = getArguments();
        ArrayList<PageMenu> pageMenus = null;
        String title = null;
        if (args != null) {
            pageMenus = (ArrayList<PageMenu>) args.getSerializable(Keys.PAGE_MEMU);
            title = (String) args.getSerializable(Keys.TITLE);
        }
        if (title != null) {
            titleView.setCenterText(title);
        }
        adapter = new ConstructionMenuAdapter(pageMenus);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        PageMenu pageMenu = adapter.getItemData(position);
        RouteUtils.toFragment(getParentFragment(), pageMenu.pageId, null);
//        FragmentContainer fragmentContainer = (FragmentContainer) getParentFragment();
//        if (fragmentContainer != null) {
//            fragmentContainer.addFragment(pageMenu.pageId, null);
//        }
    }
}
