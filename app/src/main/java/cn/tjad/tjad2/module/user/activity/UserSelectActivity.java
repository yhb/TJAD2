package cn.tjad.tjad2.module.user.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.user.fragment.UserSelectedFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.USER_SELECT)
public class UserSelectActivity extends BaseActivity {

    private UserSelectedFragment userSelectedFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userSelectedFragment = new UserSelectedFragment();
        Bundle bundle = getIntent().getExtras();
        userSelectedFragment.setArguments(bundle);
        setContentView(userSelectedFragment);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
