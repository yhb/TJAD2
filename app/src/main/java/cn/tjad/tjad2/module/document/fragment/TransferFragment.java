package cn.tjad.tjad2.module.document.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.qiandian.android.base.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.tjad.tjad2.R;

public class TransferFragment extends BaseFragment {

    @BindView(R.id.tab_layout)
    SlidingTabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private final String[] tabStrings = new String[] {"上传", "下载"};

    private List<Fragment> fragments;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.tab_layout, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragments = new ArrayList<>();
        fragments.add(new UploadTransferFragment());
        fragments.add(new DownloadTransferFragment());
        tabLayout.setViewPager(viewPager, tabStrings, getChildFragmentManager(), fragments);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
