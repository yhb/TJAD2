package cn.tjad.tjad2.module.form.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.CustomFormBean;
import cn.tjad.tjad2.data.bean.Id;
import cn.tjad.tjad2.data.session.ProjectCustomForms;
import cn.tjad.tjad2.module.form.entity.CreateFormRequestBean;
import cn.tjad.tjad2.module.form.views.SingleLineTextView;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.ProgressDialogUtils;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.FORM_CREATE)
public class FormCreateFragment extends BaseFragment implements ProjectCustomForms.OnProjectCustomFormListener {

    @BindView(R.id.form_create_name)
    SingleLineTextView nameET;
    @BindView(R.id.form_create_spinner)
    Spinner spinner;

    private List<CustomFormBean> formList;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.create_form_layout, null);
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("创建表单");
    }

    @Override
    public void onResume() {
        super.onResume();
        ProjectCustomForms.getInstance().getCustomForms(this);
    }


    @Override
    public void onProjectCustomFormGet(List<CustomFormBean> formList) {
        this.formList = formList;
        List<String> displayNames = new ArrayList<>();
        if (formList != null && formList.size() > 0) {
            for (CustomFormBean bean: formList) {
                displayNames.add(bean.name);
            }
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, displayNames);
        spinner.setAdapter(spinnerAdapter);
    }

    @OnClick(R.id.form_create_submit)
    public void onSubmit(View view) {
        String name = nameET.getValue();
        if (TextUtils.isEmpty(name)) {
            Tools.showToast("表单名称不能为空！");
            return;
        }
        if (formList == null || formList.size() == 0) {
            Tools.showToast("没有可用的表单模板！");
            return;
        }
        int selectIndex = spinner.getSelectedItemPosition();
        CustomFormBean selectForm = formList.get(selectIndex);
        createForm(selectForm.id, name);
    }

    private void createForm(int id, String name) {
        ProgressDialogUtils.show(getContext(), "正在创建表单");
        CreateFormRequestBean requestBean = new CreateFormRequestBean();
        requestBean.template = new Id(id);
        requestBean.name = name;
        RetrofitManager.getInstance().getApiService()
                .createForm(Tools.getProjectId(), requestBean)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<BaseBean>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<BaseBean> t) {
                        ProgressDialogUtils.dismiss();
                        new AlertDialog.Builder(getContext())
                                .setTitle("提示")
                                .setMessage("表单创建成功！")
                                .setCancelable(false)
                                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getActivity().finish();
                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onFailed(String message) {
                        ProgressDialogUtils.dismiss();
                        Tools.showToast(message);
                    }
                });
    }
}
