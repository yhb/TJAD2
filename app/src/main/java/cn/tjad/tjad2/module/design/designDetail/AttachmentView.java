package cn.tjad.tjad2.module.design.designDetail;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Attach;
import cn.tjad.tjad2.utils.AppUtils;
import cn.tjad.tjad2.utils.DocumentHelper;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.pictureselect.ImagePreviewWindow;

public class AttachmentView extends LinearLayout implements View.OnClickListener {

    private List<Attach> attachList;
    public AttachmentView(Context context) {
        this(context, null);
    }
    private ImagePreviewWindow imagePreviewWindow;

    public AttachmentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
    }

    public void setAttachList(List<Attach> attachList) {
        this.attachList = attachList;
        layoutAttachments();
    }

    private void layoutAttachments() {
        removeAllViews();
        if (attachList != null && !attachList.isEmpty()) {
            for (int i = 0; i < attachList.size(); i++) {
                TextView textView = getAttachElement();
                Attach attach = attachList.get(i);
                textView.setText(attach.relateFile.name);
                textView.setSingleLine();
                textView.setEllipsize(TextUtils.TruncateAt.END);
                int indexOfDot = attach.relateFile.name.lastIndexOf(".");
                String surfix = attach.relateFile.name.substring(indexOfDot + 1);
                int iconResId = Tools.getFileSmallIcon(surfix);
                textView.setCompoundDrawablesWithIntrinsicBounds(iconResId, 0, 0, 0);
                textView.setTag(attach);
                textView.setOnClickListener(this);
                LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                if (i < attachList.size() - 1) {
                    lp.topMargin = 5;
                }
                addView(textView, lp);
            }
        }
    }

    private TextView getAttachElement() {
        TextView textView = new TextView(getContext());
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTextSize(12);
        textView.setCompoundDrawablePadding(5);
        textView.setTextColor(AppUtils.getColor(R.color.blue));
        return textView;
    }

    @Override
    public void onClick(View v) {
        Attach attach = (Attach) v.getTag();
        if (attach != null) {
            if (Tools.isImageFile(attach.relateFile.name)) {
                if (imagePreviewWindow == null) {
                    imagePreviewWindow = new ImagePreviewWindow(getContext());
                }
                imagePreviewWindow.setImageUrl(attach.relateFile.path);
                imagePreviewWindow.show(0);
            } else {
                if (attach.relateFile.document != null) {
                    DocumentHelper.viewOnlineFileWithNetStatus(getContext(), attach.relateFile.document);
                }
            }
        }
    }
}
