package cn.tjad.tjad2.module.user.userselect;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.baozi.treerecyclerview.base.ViewHolder;
import com.baozi.treerecyclerview.factory.ItemHelperFactory;
import com.baozi.treerecyclerview.item.TreeItem;
import com.baozi.treerecyclerview.item.TreeItemGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Department;

public class DepartmentItemParent extends TreeItemGroup<Department> {
    @Nullable
    @Override
    protected List<TreeItem> initChildList(Department data) {
        return ItemHelperFactory.createTreeItemList(data.userList, UserItem.class, this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.user_select_item_department;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.setText(R.id.member_item_name, data.name);
        viewHolder.setVisible(R.id.member_item_icon, false);
        viewHolder.setVisible(R.id.rb_check, false);
        viewHolder.setVisible(R.id.member_item_expand_ind, true);
        viewHolder.setChecked(R.id.member_item_expand_ind, isExpand());
    }
}
