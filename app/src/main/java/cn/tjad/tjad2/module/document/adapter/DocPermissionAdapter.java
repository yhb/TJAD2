package cn.tjad.tjad2.module.document.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.PermissionBean;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.document.viewholder.PermissionViewHolder;

public class DocPermissionAdapter extends SimpleRecyclerViewAdapter<PermissionBean> {

    public DocPermissionAdapter(List<PermissionBean> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<PermissionBean> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doc_permission_item, parent, false);
        PermissionViewHolder viewHolder = new PermissionViewHolder(view);
        return viewHolder;
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("该文件夹暂无权限", 0);
    }
}
