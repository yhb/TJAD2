package cn.tjad.tjad2.module.web;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.WEBVIEW)
public class WebViewActivity extends BaseActivity {

    WebViewFragment webViewFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        webViewFragment = new WebViewFragment();
        Intent intent = getIntent();
        if (intent != null) {
            webViewFragment.setArguments(intent.getExtras());
        }
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setId(frameLayout.hashCode());
        setContentView(frameLayout);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(frameLayout.getId(), webViewFragment);
        fragmentTransaction.commit();
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
