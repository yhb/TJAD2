package cn.tjad.tjad2.module.material.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.widget.TJADTTitleView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.MATERIAL_TAB)
public class MaterialTabFragment extends BaseFragment {

    @BindView(R.id.tab_layout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private List<Fragment> fragmentList;

    private final String[] tabStrings = new String[] {"与我相关", "所有问题"};

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.tab_layout, null);
    }

    @Override
    protected boolean showTitle() {
        return true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentList = new ArrayList<>();
//        EventBus.getDefault().register(this);
        fragmentList.add(MaterialTrackingListFragment.newInstance(0));
        fragmentList.add(MaterialTrackingListFragment.newInstance(1));
        tabLayout.setViewPager(viewPager, tabStrings, getChildFragmentManager(), fragmentList);
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("物料追踪");
        titleView.showLeft(true);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
    }
}