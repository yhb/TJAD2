package cn.tjad.tjad2.module.form.viewHolder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

//type = 1
public class FormRejectViewHolder extends SimpleBaseViewHolder<FormDetail.FormDetailStep> {

    @BindView(R.id.form_reject_step)
    TextView stepTV;
    @BindView(R.id.form_reject_reason)
    TextView reasonTV;
    @BindView(R.id.form_reject_attachment)
    TextView attachmentTV;


    public FormRejectViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(FormDetail.FormDetailStep data, int position) {
        String[] texts = itemView.getContext().getResources().getStringArray(R.array.form_reject_steps);
        String rejectStep = "--";
        if (data.rejectType > 0 && data.rejectType < texts.length) {
            rejectStep = texts[data.rejectType];
        }
        stepTV.setText(rejectStep);
        reasonTV.setText(data.description);
        String attachments = "";
        if (data.rejectFileItems != null && data.rejectFileItems.size() > 0) {
            for (int i = 0; i < data.rejectFileItems.size(); i++) {
                attachments += data.rejectFileItems.get(i).name;
                if (i < data.rejectFileItems.size() - 1) {
                    attachments += "\n";
                }
            }
        }
        attachmentTV.setText(attachments);
    }
}
