package cn.tjad.tjad2.module.notice.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.widget.TJADTTitleView;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.UnReadMessageBean;
import cn.tjad.tjad2.data.bean.UnreadMessage;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.notice.adapter.MessageMenuAdapter;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.ProgressDialogUtils;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.MESSAGE_MENU)
public class MessageMenuFragment extends BaseFragment implements OnItemClickListener {

    private RecyclerView recyclerView;
    private MessageMenuAdapter adapter;

    @Override
    public View getContentView(LayoutInflater inflater) {
        recyclerView = new RecyclerView(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        return recyclerView;
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("消息列表");
        titleView.showRight(true);
        titleView.setRightImage(R.drawable.icon_message_setting);
    }

    @Override
    public void onTitleClick(int which) {
        super.onTitleClick(which);
        switch (which) {
            case TJADTTitleView.TITLE_RIGHT:
                RouteUtils.toFragmentActivity(getContext(), PageId.MESSAGE_SETTING_MENU, null);
                break;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new MessageMenuAdapter(null);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        ProgressDialogUtils.show(getContext());
        requestUnreadMessage();
    }

    private void requestUnreadMessage() {
        RetrofitManager.getInstance().getApiService()
                .getUnreadMessage(Tools.getProjectId())
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<UnReadMessageBean>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<UnReadMessageBean> t) {
                        if (t.result != null) {
                            adapter.updateData(t.result.message);
                            adapter.notifyDataSetChanged();
                        }
                        ProgressDialogUtils.dismiss();
                    }

                    @Override
                    public void onFailed(String message) {
                        Tools.showToast(message);
                        ProgressDialogUtils.dismiss();
                    }
                });
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        UnreadMessage message = adapter.getItemData(position);
        Bundle bundle = new Bundle();
        bundle.putInt(Keys.MESSAGE_TYPE, message.type);
        bundle.putString(Keys.MESSAGE_TYPE_NAME, message.name);
        RouteUtils.toFragmentActivity(getContext(), PageId.MESSAGE_LIST, bundle);
    }
}
