package cn.tjad.tjad2.module.document.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hjy.http.download.FileDownloadTask;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.document.viewholder.DownloadViewHolder;

public class DownLoadListAdapter extends SimpleRecyclerViewAdapter<FileDownloadTask> {

    public DownLoadListAdapter(List<FileDownloadTask> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<FileDownloadTask> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_transfer_list_item, parent, false);
        DownloadViewHolder viewHolder = new DownloadViewHolder(view);
        return viewHolder;
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("下载列表为空", 0);
    }
}
