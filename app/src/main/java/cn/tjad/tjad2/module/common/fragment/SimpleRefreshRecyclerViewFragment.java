package cn.tjad.tjad2.module.common.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qiandian.android.base.fragment.BaseFragment;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.listener.OnItemClickListener;

public abstract class SimpleRefreshRecyclerViewFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, OnItemClickListener {

    @BindView(R.id.swipe_refresh_layout)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerview)
    protected RecyclerView recyclerView;

    protected SimpleRecyclerViewAdapter adapter;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.common_recylerview, null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (enablePullRefresh()) {
            swipeRefreshLayout.setEnabled(true);
            swipeRefreshLayout.setOnRefreshListener(this);
        } else {
            swipeRefreshLayout.setEnabled(false);
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        adapter = getAdapter();
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        initViews();
        return view;
    }

    protected boolean enablePullRefresh() {
        return false;
    }

    @Override
    public void onRefresh() {

    }

    protected void initViews() {

    }

    protected abstract SimpleRecyclerViewAdapter getAdapter();

    @Override
    public void onItemClick(View parentView, View itemView, int position) {

    }
}
