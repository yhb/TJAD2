package cn.tjad.tjad2.module.project.activity;



import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.project.fragment.ProjectListFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.PROJECT_LIST)
public class ProjectListActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new ProjectListFragment());
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
