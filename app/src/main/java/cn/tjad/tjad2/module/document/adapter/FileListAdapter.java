package cn.tjad.tjad2.module.document.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.data.event.FileCheckedChangeEvent;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.document.viewholder.FileListViewHolder;

public class FileListAdapter extends SimpleRecyclerViewAdapter<FileBean> {

    private boolean isEditMode = false;

    private List<FileBean> selectedFileList = new ArrayList<>();

    public FileListAdapter(List<FileBean> data) {
        super(data);
        EventBus.getDefault().register(this);
    }

    public void setIsEditMode(boolean editMode) {
        isEditMode = editMode;
    }

    public boolean isEditMode() {
        return isEditMode;
    }

    @Override
    protected SimpleBaseViewHolder<FileBean> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_list_item, new FrameLayout(parent.getContext()), false);
        FileListViewHolder vh = new FileListViewHolder(itemView);
        return vh;
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("此文件夹为空", 0);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleBaseViewHolder<FileBean> holder, int position) {
        if (holder instanceof FileListViewHolder) {
            ((FileListViewHolder) holder).setEditMode(isEditMode);
        }
        super.onBindViewHolder(holder, position);
        if (holder instanceof FileListViewHolder) {
            if (selectedFileList.contains(getItemData(position))) {
                ((FileListViewHolder) holder).setChecked(true);
            } else {
                ((FileListViewHolder) holder).setChecked(false);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FileCheckedChangeEvent event) {
        if (event.isChecked()) {
            if (!selectedFileList.contains(event.getFileBean())) {
                selectedFileList.add((FileBean) event.getFileBean());
            }
        } else {
            if (selectedFileList.contains(event.getFileBean())) {
                selectedFileList.remove(event.getFileBean());
            }
        }
    }

    public List<FileBean> getSelectedFileList() {
        return selectedFileList;
    }

    public void clearSelectedFiles() {
        selectedFileList.clear();
    }

    public void checkPosition(int position) {
        FileBean data = getItemData(position);
        if (!selectedFileList.contains(data)) {
            selectedFileList.add(data);
        }
        notifyDataSetChanged();
    }

    public void toggleCheckedPosition(int positon) {
        FileBean data = getItemData(positon);
        if (selectedFileList.contains(data)) {
            selectedFileList.remove(data);
        } else {
            selectedFileList.add(data);
        }
        notifyDataSetChanged();
    }

    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }
}
