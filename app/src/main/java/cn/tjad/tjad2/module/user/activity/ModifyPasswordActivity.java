package cn.tjad.tjad2.module.user.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;
import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.net.http.HttpConstants;
import com.qiandian.android.base.net.http.HttpId;
import com.qiandian.android.base.net.http.IDataCallBack;

import butterknife.BindView;
import butterknife.OnClick;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.business.RemoteDataService;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.dialog.OnDialogClickListener;
import cn.tjad.tjad2.module.common.dialog.TjDialogFragment;

@Route(path = PageId.MODIFY_PASSWORD)
public class ModifyPasswordActivity extends BaseActivity implements IDataCallBack {

    @BindView(R.id.old_pwd)
    EditText oldPwdET;
    @BindView(R.id.new_pwd)
    EditText newPwdET;
    @BindView(R.id.new_pwd_2)
    EditText newPwd2ET;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_password);
    }

    @OnClick({R.id.save, R.id.back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save:
                onCommit();
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    private void onCommit() {
        String oldPwd = oldPwdET.getText().toString();
        String newPwd = newPwdET.getText().toString();
        String newPwd2 = newPwd2ET.getText().toString();
        if ("".equals(oldPwd)) {
            Tools.showToast("请输入旧密码!");
            return;
        }
        if ("".equals(newPwd) && "".equals(newPwd2)) {
            Tools.showToast("请输入新密码!");
            return;
        }
        if (!newPwd.equals(newPwd2)) {
            Tools.showToast("两次输入的新密码不一致!");
            return;
        }
        submitChangePassword(oldPwd, newPwd);
    }

    private void submitChangePassword(String oldPwd, String newPwd) {
        RemoteDataService.modifyPassword(oldPwd, newPwd, this);
    }

    @Override
    public boolean onReceiveError(int httpId, int errorCode, String errorMsg) {
        if (httpId == HttpId.MODIFY_PASSWORD) {
            if (TextUtils.isEmpty(errorMsg)) {
                Tools.showToast("修改密码失败");
            } else {
                Tools.showToast(errorMsg);
            }
        }
        return false;
    }

    @Override
    public void onDataRefresh(int httpId, Object responseBean) {
        if (httpId == HttpId.MODIFY_PASSWORD) {
            CommonBean<BaseBean> bean = (CommonBean<BaseBean>) responseBean;
            if (bean.resCode == HttpConstants.CODE_SUCCESS) {
                TjDialogFragment.Builder builder = new TjDialogFragment.Builder(this);
                builder.setTitle("提示")
                        .setMessage("密码修改成功!")
//                        .setCancelable(false)
                        .setPositiveButton("确认", new OnDialogClickListener() {
                            @Override
                            public boolean onClick(Dialog dialogInterface, int i) {
                                finish();
                                return true;
                            }
                        })
                        .show(getSupportFragmentManager());
            } else {
                Tools.showToast(bean.respMsg);
            }
        }
    }
}
