package cn.tjad.tjad2.module.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.tjad.tjad2.R;

public class TjDialogFragment extends AppCompatDialogFragment {

    @BindView(R.id.tj_dialog_title)
    TextView titleTV;
    @BindView(R.id.tj_dialog_content)
    FrameLayout contentView;
    @BindView(R.id.tj_dialog_text)
    TextView messageTV;
    @BindView(R.id.btn_positive)
    TextView positiveBtn;
    @BindView(R.id.btn_negative)
    TextView negativeBtn;
    @BindView(R.id.btn_neutral)
    TextView neutralBtn;

    private OnDialogClickListener onPositiveClickListener;
    private OnDialogClickListener onNegativeClickListener;
    private OnDialogClickListener onNeutralClickListener;

    private Builder builder;



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.tj_dialog_layout, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view);
        ButterKnife.bind(this, view);
        setBuilderData(builder);
        return builder.create();
    }

    private void setBuilderData(AlertDialog.Builder dialogBuilder) {
        if (builder != null) {
            if (builder.titleSet) {
                setTitle(builder.title);
            }
            if (builder.messageSet) {
                setMessage(builder.message);
            }
            if (builder.viewSet) {
                setView(builder.contentView);
            }
            if (builder.positiveSet) {
                setPositiveButton(builder.positiveText, builder.positiveClickListener);
            }
            if (builder.negativeSet) {
                setNegativeButton(builder.negativeText, builder.negativeClickListener);
            }
            if (builder.neutralSet) {
                setNeutralButton(builder.neutralText, builder.neutralClickListener);
            }
            dialogBuilder.setCancelable(builder.cancelable);
        }
    }

    public TjDialogFragment setTitle(String title) {
        titleTV.setText(title);
        return this;
    }

    public TjDialogFragment setTitle(@StringRes int resId) {
        titleTV.setText(resId);
        return this;
    }

    public TjDialogFragment setMessage(String message) {
        messageTV.setText(message);
        return this;
    }

    public TjDialogFragment setMessage(@StringRes int resId) {
        messageTV.setText(resId);
        return this;
    }

    public TjDialogFragment setView(View view) {
        if (view != null) {
            contentView.removeAllViews();
            contentView.addView(view);
        }
        return this;
    }

    public TjDialogFragment setPositiveButton(String text, OnDialogClickListener l) {
        positiveBtn.setText(text);
        this.onPositiveClickListener = l;
        positiveBtn.setVisibility(View.VISIBLE);
        return this;
    }

    public TjDialogFragment setNegativeButton(String text, OnDialogClickListener l) {
        negativeBtn.setText(text);
        this.onNegativeClickListener = l;
        negativeBtn.setVisibility(View.VISIBLE);
        return this;
    }

    public TjDialogFragment setNeutralButton(String text, OnDialogClickListener l) {
        neutralBtn.setText(text);
        this.onNeutralClickListener = l;
        neutralBtn.setVisibility(View.VISIBLE);
        return this;
    }

    public void setCancelable(boolean cancelable) {
        getDialog().setCancelable(cancelable);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            Field mDismissed = DialogFragment.class.getDeclaredField("mDismissed");
            Field mShowByMe = DialogFragment.class.getDeclaredField("mShownByMe");
            mDismissed.setAccessible(true);
            mShowByMe.setAccessible(true);
            mDismissed.setBoolean(this, false);
            mShowByMe.setBoolean(this, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    void setBuilder(Builder builder) {
        this.builder = builder;
    }

    @OnClick({R.id.btn_positive, R.id.btn_negative, R.id.btn_neutral})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_positive:
                boolean close = true;
                if (onPositiveClickListener != null) {
                     close = onPositiveClickListener.onClick(getDialog(), Dialog.BUTTON_POSITIVE);
                }
                if (close) {
                    dismiss();
                }
                break;
            case R.id.btn_negative:
                boolean close2 = true;
                if (onNegativeClickListener != null) {
                    close2 = onNegativeClickListener.onClick(getDialog(), Dialog.BUTTON_NEGATIVE);
                }
                if (close2) {
                    dismiss();
                }
                break;
            case R.id.btn_neutral:
                boolean close3 = true;
                if (onNeutralClickListener != null) {
                    close3 = onNeutralClickListener.onClick(getDialog(), Dialog.BUTTON_NEUTRAL);
                }
                if (close3) {
                    dismiss();
                }
                break;
        }
    }

    public static final class Builder {
        private String title;
        private String message;
        private View contentView;
        private String positiveText;
        private String negativeText;
        private String neutralText;
        private OnDialogClickListener positiveClickListener;
        private OnDialogClickListener negativeClickListener;
        private OnDialogClickListener neutralClickListener;
        private boolean cancelable = true;
        private boolean titleSet;
        private boolean messageSet;
        private boolean viewSet;
        private boolean positiveSet;
        private boolean negativeSet;
        private boolean neutralSet;

        private Context context;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setTitle(String title) {
            titleSet = true;
            this.title = title;
            return this;
        }

        public Builder setTitle(@StringRes int resId) {
            titleSet = true;
            this.title = context.getString(resId);
            return this;
        }

        public String getTitle() {
            return title;
        }

        public boolean isTitleSet() {
            return titleSet;
        }

        public Builder setMessage(String message) {
            messageSet = true;
            this.message = message;
            return this;
        }

        public Builder setMessage(@StringRes int resId) {
            messageSet = true;
            this.message = context.getString(resId);
            return this;
        }

        public boolean isMessageSet() {
            return messageSet;
        }

        public String getMessage() {
            return message;
        }

        public Builder setContentView(View contentView) {
            viewSet = true;
            this.contentView = contentView;
            return this;
        }

        public boolean isViewSet() {
            return viewSet;
        }

        public View getContentView() {
            return contentView;
        }

        public Builder setPositiveButton(String text, OnDialogClickListener l) {
            positiveSet = true;
            this.positiveText = text;
            this.positiveClickListener = l;
            return this;
        }

        public boolean isPositiveSet() {
            return positiveSet;
        }

        public String getPositiveText() {
            return positiveText;
        }

        public OnDialogClickListener getPositiveClickListener() {
            return positiveClickListener;
        }

        public Builder setNegativeButton(String text, OnDialogClickListener l) {
            negativeSet = true;
            this.negativeText = text;
            this.negativeClickListener = l;
            return this;
        }

        public boolean isNegativeSet() {
            return negativeSet;
        }

        public String getNegativeText() {
            return negativeText;
        }

        public OnDialogClickListener getNegativeClickListener() {
            return negativeClickListener;
        }

        public Builder setNeutralButton(String text, OnDialogClickListener l) {
            neutralSet = true;
            this.neutralText = text;
            this.neutralClickListener = l;
            return this;
        }

        public boolean isNeutralSet() {
            return neutralSet;
        }

        public String getNeutralText() {
            return neutralText;
        }

        public OnDialogClickListener getNeutralClickListener() {
            return neutralClickListener;
        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public boolean isCancelable() {
            return cancelable;
        }

        public TjDialogFragment create() {
            TjDialogFragment dialogFragment = new TjDialogFragment();
            dialogFragment.setBuilder(this);
            return dialogFragment;
        }

        public void show(FragmentManager fragmentManager) {
            TjDialogFragment dialogFragment = create();
            dialogFragment.show(fragmentManager, UUID.randomUUID().toString());
        }
    }
}
