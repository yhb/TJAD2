package cn.tjad.tjad2.module.material.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.data.bean.MaterialDetail;
import cn.tjad.tjad2.data.bean.Project;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.data.event.MaterialItemCheckedEvent;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.utils.Tools;

public class MaterialDetailViewHolder extends SimpleBaseViewHolder<MaterialDetail.Item> {

    @BindView(R.id.material_detail_item_checkbox)
    CheckBox checkBox;
    @BindView(R.id.material_detail_item_name)
    TextView nameTV;
    @BindView(R.id.material_detail_item_id)
    TextView idTV;
    @BindView(R.id.material_detail_item_current_step)
    TextView currentStepTV;
    @BindView(R.id.material_detail_item_next_step)
    TextView nextStep;
    @BindView(R.id.material_detail_item_update_time)
    TextView updateTimeTV;
    @BindView(R.id.material_detail_item_position)
    ImageView positionIV;

    private MaterialDetail.Item itemData;

    public MaterialDetailViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(MaterialDetail.Item data, int position) {
        this.itemData = data;
        checkBox.setChecked(false);
        nameTV.setText(data.name);
        idTV.setText(data.code);
        if (data.finished) {
            currentStepTV.setText("已结束");
        } else if (data.step != null && data.step.data != null) {
            currentStepTV.setText(data.step.data.name);
        } else {
            currentStepTV.setText("--");
        }
        if (data.nextStep != null && data.nextStep.data != null) {
            nextStep.setText(data.nextStep.data.name);
        } else {
            nextStep.setText("--");
        }
        updateTimeTV.setText("最近更新： " + data.modifyTime);
    }

    @OnCheckedChanged({R.id.material_detail_item_checkbox})
    public void onCheckedChanged(CheckBox checkBox, boolean isChecked) {
        if (isChecked) {
            if (hasPermission(itemData)) {
                itemData.isChecked = true;
                EventBus.getDefault().post(new MaterialItemCheckedEvent());
            } else {
                Tools.showToast("您没有权限操作该项");
                checkBox.setChecked(false);
            }
        } else {
            itemData.isChecked = false;
            EventBus.getDefault().post(new MaterialItemCheckedEvent());
        }

    }

    private boolean hasPermission(MaterialDetail.Item item) {
        if (item == null) {
            return false;
        }
        User user = TjadApplication.getInstance().getSession().getUserInfo();
        Project project = TjadApplication.getInstance().getSession().getProject();
        if (user.admin) {
            return true;
        }
        if (project.admin != null && user.id == project.admin.id) {
            return true;
        }
        if (item.step != null) {
            if (item.step.user != null && user.id == item.step.user.id) {
                return true;
            }
            if (user.company != null && item.step.company != null && (user.company.id == item.step.company.id)) {
                return true;
            }
            if (user.department != null && item.step.department != null && user.department.id == item.step.department.id) {
                return true;
            }
        }
        return true;
    }

}
