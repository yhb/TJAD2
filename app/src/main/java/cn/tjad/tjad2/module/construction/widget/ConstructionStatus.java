package cn.tjad.tjad2.module.construction.widget;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.net.http.HttpConstants;
import com.qiandian.android.base.net.http.HttpId;
import com.qiandian.android.base.net.http.IDataCallBack;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.business.RemoteDataService;
import cn.tjad.tjad2.data.bean.ConstructionDetail;
import cn.tjad.tjad2.data.event.ConstructionStatusChangedEvent;
import cn.tjad.tjad2.utils.ProgressDialogUtils;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.widget.PictureLoadView;
import cn.tjad.tjad2.module.common.pictureselect.PictureSelectWidget;

/**
 * 施工状态逻辑类
 */
public class ConstructionStatus implements IDataCallBack {

    private Context context;
    private View view;

    private ConstructionDetail constructionDetail;

    @Nullable
    @BindView(R.id.construction_progress_picture)
    PictureSelectWidget pictureSelectWidget;
    @Nullable
    @BindView(R.id.construction_progress_status_select)
    AppCompatSpinner statusSpinner;
    @Nullable
    @BindView(R.id.construction_progress_submit)
    Button statusSubmitBtn;
    @Nullable
    @BindView(R.id.construction_progress_status)
    TextView statusTV;
    @Nullable
    @BindView(R.id.construction_progress_operator)
    TextView operatorNameTV;
    @Nullable
    @BindView(R.id.construction_progress_photo_container)
    PictureLoadView photoGridView;


    public ConstructionStatus(Context context) {
        this.context = context;
    }

    public void setConstructionDetail(ConstructionDetail cd) {
        if (cd == null) {
            return;
        }
        if (this.constructionDetail == null || cd.consStatus != constructionDetail.consStatus) {
            onStatusChanged(cd);
        }
        this.constructionDetail = cd;
    }

    public View getView() {
        return view;
    }

    private void onStatusChanged(ConstructionDetail cd) {
        int status = cd.consStatus;
        if (status == 1) { //进行中
            view = View.inflate(context, R.layout.construction_progressing, null);
            ButterKnife.bind(this, view);
            if (!cd.allowFinish) {
                statusSpinner.setEnabled(false);
                pictureSelectWidget.setEnabled(false);
                statusSubmitBtn.setVisibility(View.GONE);
            } else {
                statusSpinner.setEnabled(true);
                pictureSelectWidget.setEnabled(true);
                statusSubmitBtn.setVisibility(View.VISIBLE);
            }
        } else if (status == 2 || status == 3) { //已完成
            view = View.inflate(context, R.layout.construction_progress_finished, null);
            ButterKnife.bind(this, view);
            String statusStr = Tools.getConsStatusByCode(status);
            statusTV.setText(statusStr);
            if (cd.createUser != null) {
                operatorNameTV.setText(cd.createUser.name);
            }
            if (cd.finishPhotos != null) {
                photoGridView.setImageUrls(cd.finishPhotos);
            }
        } else {
            view = null;
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (pictureSelectWidget != null) {
            pictureSelectWidget.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Optional
    @OnClick({R.id.construction_progress_submit})
    public void onClick(View view) {
        if (view.getId() == R.id.construction_progress_submit) {
            commitStatus();
        }
    }

    private void commitStatus() {
        ProgressDialogUtils.show(context);
        int statusPosition = statusSpinner.getSelectedItemPosition();
        if (statusPosition == 0) {
            statusPosition = 2; //完成
        } else if (statusPosition == 1) {
            statusPosition = 3; //作废
        }
        if (pictureSelectWidget.getSelectedImageList() != null
                && pictureSelectWidget.getSelectedImageList().size() > 0) {
            final int finalStatusPosition = statusPosition;
            pictureSelectWidget.uploadPictures(new PictureSelectWidget.IPictureUploadCallback() {
                @Override
                public void onError(String errorMsg) {
                    if (TextUtils.isEmpty(errorMsg)) {
                        Tools.showToast("提交失败， 请稍后再试！");
                    } else {
                        Tools.showToast(errorMsg);
                    }
                }

                @Override
                public void onSuccess(List<String> urlList) {
                    submitStatus(finalStatusPosition, urlList);
                }
            });
        } else {
            submitStatus(statusPosition, null);
        }
    }

    private void submitStatus(int status, List<String> imageUrlList) {
        int projectId = TjadApplication.getInstance().getSession().getProjectId();
        int constructionId = constructionDetail.id;
        RemoteDataService.constructionFinish(projectId, constructionId, imageUrlList, status, this);
    }

    @Override
    public boolean onReceiveError(int httpId, int errorCode, String errorMsg) {
        if (httpId == HttpId.CONSTRUCTION_FINISH) {
            if (TextUtils.isEmpty(errorMsg)) {
                Tools.showToast("操作失败， 请稍后再试");
            } else {
                Tools.showToast(errorMsg);
            }
        }
        return true;
    }

    @Override
    public void onDataRefresh(int httpId, Object responseBean) {
        if (httpId == HttpId.CONSTRUCTION_FINISH) {
            CommonBean<BaseBean> bean = (CommonBean<BaseBean>) responseBean;
            if (bean.resCode == HttpConstants.CODE_SUCCESS) {
                Tools.showToast("提交成功");
                ConstructionStatusChangedEvent event = new ConstructionStatusChangedEvent();
                event.setChanged(true);
                EventBus.getDefault().post(event);
                ProgressDialogUtils.dismiss();
            } else {
                Tools.showToast(bean.respMsg);
            }
        }
    }
}
