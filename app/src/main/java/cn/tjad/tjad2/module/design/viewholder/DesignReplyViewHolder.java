package cn.tjad.tjad2.module.design.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.DesignReplyItem;
import cn.tjad.tjad2.data.bean.MultiItemBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.common.widget.PictureLoadView;

public class DesignReplyViewHolder extends SimpleBaseViewHolder<MultiItemBean> {
    @BindView(R.id.design_detail_item_replyer)
    TextView replyerTV;
    @BindView(R.id.design_detail_item_reply_time)
    TextView replyTimeTV;
    @BindView(R.id.design_detail_item_reply_content)
    TextView replyContentTV;
    @BindView(R.id.design_detail_item_picture_load_view)
    PictureLoadView pictureLoadView;

    public DesignReplyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(MultiItemBean data, int position) {
        DesignReplyItem item = (DesignReplyItem) data.getData();
        if (item.createUser != null) {
            replyerTV.setText(item.createUser.name);
        } else {
            replyerTV.setText("");
        }
        replyTimeTV.setText(item.createTime);
        replyContentTV.setText(item.content);
        pictureLoadView.setImageUrls(item.photos);
    }
}
