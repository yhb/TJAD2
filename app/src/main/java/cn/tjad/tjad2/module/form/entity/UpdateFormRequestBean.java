package cn.tjad.tjad2.module.form.entity;

import java.util.ArrayList;

import cn.tjad.tjad2.data.bean.Id;

public class UpdateFormRequestBean {
    public Integer id;
    public Id step;
    public int consStatus;
    public ArrayList<Detail> details;

    public static class Detail {
        public Id detail;
        public String value;
    }
}
