package cn.tjad.tjad2.module.user.adapter;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.data.bean.Department;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.user.viewholder.DepartmentViewHolder;

public class DepartmentListAdapter extends SimpleRecyclerViewAdapter<Department> {

    int itemMinHeight;
    int leftRightPadding;

    public DepartmentListAdapter(List<Department> data) {
        super(data);
        Resources res = TjadApplication.getInstance().getResources();
        itemMinHeight = res.getDimensionPixelSize(R.dimen.list_item_min_height);
        leftRightPadding = res.getDimensionPixelSize(R.dimen.left_right_padding);
    }

    @Override
    protected SimpleBaseViewHolder<Department> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView textView = new TextView(parent.getContext());
        textView.setGravity(Gravity.CENTER_VERTICAL);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setMinHeight(itemMinHeight);
        textView.setPadding(leftRightPadding, 0, leftRightPadding, 0);
        return new DepartmentViewHolder(textView);
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("部门列表为空", 0);
    }
}
