package cn.tjad.tjad2.module.user.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.Member;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.user.viewholder.MemberListViewHolder;

public class MemberListAdapter extends SimpleRecyclerViewAdapter<Member> {
    public MemberListAdapter(List<Member> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<Member> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.member_list_item, new FrameLayout(parent.getContext()), false);
        return new MemberListViewHolder(view);
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("无成员", 0);
    }
}
