package cn.tjad.tjad2.module.document.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.widget.FrameLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.document.fragment.TransferFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.FILE_TRANSFER)
public class FileTransferActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setId(frameLayout.hashCode());
        setContentView(frameLayout);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(frameLayout.getId(), new TransferFragment()).commit();
    }
}
