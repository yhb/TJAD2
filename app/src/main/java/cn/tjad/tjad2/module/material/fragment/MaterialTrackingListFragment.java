package cn.tjad.tjad2.module.material.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.bean.CommonListBean;

import java.util.ArrayList;

import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.MaterialBean;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.fragment.RefreshableListFragment;
import cn.tjad.tjad2.module.material.adapter.MaterialTrackingListAdapter;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.MATERIAL_TRAKING)
public class MaterialTrackingListFragment extends RefreshableListFragment implements OnItemClickListener {

    // 0 : 与我相关
    // 1 : 所有
    private int tabType = 0;
    private MaterialTrackingListAdapter adapter;

    public static MaterialTrackingListFragment newInstance(int tabType) {
        Bundle bundle = new Bundle();
        bundle.putInt("tabType", tabType);
        MaterialTrackingListFragment fragment = new MaterialTrackingListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void requestData() {
        RetrofitManager.getInstance()
                .getApiService()
                .getMaterialTrackingList(Tools.getProjectId(),
                        startIndex, PAGE_COUNT)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<ArrayList<MaterialBean>>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<ArrayList<MaterialBean>> t) {
                        CommonListBean<MaterialBean> bean = (CommonListBean<MaterialBean>) t;
                        adapter.updateData(t.result, !isRefresh);
                        adapter.notifyDataSetChanged();
                        onDataLoadSuccess(bean.pagination);
                    }

                    @Override
                    public void onFailed(String message) {
                        onDataLoadFailed();
                    }
                });
    }

    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        adapter = new MaterialTrackingListAdapter(new ArrayList<>());
        adapter.setOnItemClickListener(this);
        return adapter;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshLayout.autoRefresh();
    }

    @Override
    protected boolean showTitle() {
        return false;
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        MaterialBean bean = adapter.getItemData(position);
        Bundle bundle = new Bundle();
        bundle.putInt(Keys.MATERIAL_ID, bean.id);
        RouteUtils.toFragmentActivity(getContext(), PageId.MATERIAL_DETAIL, bundle);
    }
}
