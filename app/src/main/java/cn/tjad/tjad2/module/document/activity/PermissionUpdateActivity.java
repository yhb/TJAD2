package cn.tjad.tjad2.module.document.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.document.fragment.PermissionUpdateFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.PERMISSION_UPDATE)
public class PermissionUpdateActivity extends BaseActivity {

    private PermissionUpdateFragment permissionUpdateFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissionUpdateFragment = new PermissionUpdateFragment();
        Intent intent = getIntent();
        permissionUpdateFragment.setArguments(intent.getExtras());
        setContentView(permissionUpdateFragment);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
