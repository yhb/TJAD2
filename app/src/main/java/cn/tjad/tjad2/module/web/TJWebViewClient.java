package cn.tjad.tjad2.module.web;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;



public class TJWebViewClient extends WebViewClient {


    private boolean showProgress;
    private ProgressBar progressBar;

    public void setProgress(boolean showProgress, ProgressBar progressBar) {
        this.showProgress = showProgress;
        this.progressBar = progressBar;
        if (showProgress && progressBar == null) {
            throw new RuntimeException("showProgress为true的时候progressBar不能为空");
        }
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String s) {
        return true;
    }

    @Override
    public void onPageStarted(WebView webView, String s, Bitmap bitmap) {
        super.onPageStarted(webView, s, bitmap);
        if (showProgress) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
        }
    }

    @Override
    public void onPageFinished(WebView webView, String s) {
        super.onPageFinished(webView, s);
        if (showProgress) {
            progressBar.setProgress(100);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        if (showProgress) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        if (showProgress) {
            progressBar.setVisibility(View.GONE);
        }
    }
}
