package cn.tjad.tjad2.module.document.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.data.event.FileCheckedChangeEvent;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class FileListViewHolder extends SimpleBaseViewHolder<FileBean> implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.file_icon)
    ImageView fileIcon;
    @BindView(R.id.file_name)
    TextView fileName;
    @BindView(R.id.file_size)
    TextView fileSize;
    @BindView(R.id.file_date)
    TextView fileDate;
    @BindView(R.id.file_checked)
    CheckBox checkBox;

    private boolean isEditMode;
    private FileBean data;
//    private int checkedPosition = -1;
    private int position = -1;

    public FileListViewHolder(View itemView) {
        super(itemView);
        itemView.setTag(this);
        ButterKnife.bind(this, itemView);
        checkBox.setOnCheckedChangeListener(this);
//        EventBus.getDefault().register(this);
    }

//    public boolean isChecked() {
//        return checkBox.isChecked();
//    }

//    public FileBean getData() {
//        return data;
//    }

    public void setChecked(boolean checked) {
        checkBox.setChecked(checked);
    }

//    public void toggle() {
//        checkBox.setChecked(!checkBox.isChecked());
//    }


//    public void setCheckedPosition(int checkedPosition) {
//        this.checkedPosition = checkedPosition;
//        checkBox.setChecked(checkedPosition == position);
//    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
    }

    @Override
    public void setData(FileBean data, int position) {
        this.data = data;
        this.position = position;
        fileName.setText(data.name);
        fileSize.setText(Tools.getDisplayedFileSize(data.size));
        fileDate.setText(data.createTime);
        if (data.directory) {
            fileSize.setVisibility(View.GONE);
            fileIcon.setImageResource(R.drawable.icon_folder);
        } else {
            fileSize.setVisibility(View.VISIBLE);
            int icon = R.drawable.file_icon_unknown;
            if (data.name != null) {
                int index = data.name.lastIndexOf(".");
                String subfix = "";
                if (index >= 0) {
                     subfix = data.name.substring(index + 1);
                }
                icon = Tools.getFileIcon(subfix);
            }
            fileIcon.setImageResource(icon);
        }
//        checkBox.setChecked(checkedPosition == position);
        if (isEditMode) {
            checkBox.setVisibility(View.VISIBLE);
        } else {
            checkBox.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton cb, boolean value) {
        FileCheckedChangeEvent event = new FileCheckedChangeEvent();
        event.setChecked(value);
        event.setFileBean(data);
        EventBus.getDefault().post(event);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEditModeChanged(EditModeChangeEvent event) {
//        if (event.isEditMode()) {
//            checkBox.setVisibility(View.VISIBLE);
//        } else {
//            checkBox.setVisibility(View.GONE);
//            checkBox.setChecked(false);
//        }
//    }

}
