package cn.tjad.tjad2.module.form.viewHolder;

import android.view.View;

import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

//type = 2
public class FormCancelViewHolder extends SimpleBaseViewHolder<FormDetail.FormDetailStep> {
    public FormCancelViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setData(FormDetail.FormDetailStep data, int position) {

    }
}
