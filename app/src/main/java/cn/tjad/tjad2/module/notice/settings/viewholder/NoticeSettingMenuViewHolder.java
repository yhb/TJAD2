package cn.tjad.tjad2.module.notice.settings.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.CommonEntity;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class NoticeSettingMenuViewHolder extends SimpleBaseViewHolder<CommonEntity> {

    @BindView(R.id.message_setting_menu_name)
    TextView nameTV;

    public NoticeSettingMenuViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(CommonEntity data, int position) {
        nameTV.setText(data.name);
    }
}
