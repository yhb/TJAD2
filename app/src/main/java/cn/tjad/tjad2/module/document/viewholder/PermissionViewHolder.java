package cn.tjad.tjad2.module.document.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.PermissionBean;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class PermissionViewHolder extends SimpleBaseViewHolder<PermissionBean> {

    @BindView(R.id.permission_name)
    TextView name;
    @BindView(R.id.permission_role)
    TextView role;
    @BindView(R.id.permission_view)
    CheckBox viewCB;
    @BindView(R.id.permission_create)
    CheckBox createCB;
    @BindView(R.id.permission_download)
    CheckBox downloadCB;
    @BindView(R.id.permission_move)
    CheckBox moveCB;
    @BindView(R.id.permission_rename)
    CheckBox renameCB;
    @BindView(R.id.permission_set_permission)
    CheckBox setPermissionCB;


    public PermissionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(PermissionBean data, int position) {
        if (data == null) {
            return;
        }
        if (data.company != null) {
            name.setText(data.company.name);
            role.setText("");
        } else if (data.department != null) {
            name.setText(data.department.name);
            role.setText(data.department.responsibility);
        } else if (data.user != null) {
            name.setText(data.user.name);
            role.setText(data.user.title);
        } else {
            name.setText("");
            role.setText("");
        }
        viewCB.setChecked(data.view);
        createCB.setChecked(data.create);
        downloadCB.setChecked(data.download);
        moveCB.setChecked(data.move);
        renameCB.setChecked(data.rename);
        setPermissionCB.setChecked(data.setPermission);
    }
}
