package cn.tjad.tjad2.module.common.adapter;

import android.content.Context;

import java.util.List;

import cn.tjad.tjad2.module.common.viewholder.PicturePreviewViewHolder;
import cn.tjad.tjad2.module.common.pictureselect.TJBaseAdapter;
import cn.tjad.tjad2.module.common.pictureselect.ViewHolder;

/**
 * Created by yanghongbing on 17/11/5.
 */

public class PicturePreviewAdapter extends TJBaseAdapter {

    public PicturePreviewAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public ViewHolder getViewHolder(int position) {
        return new PicturePreviewViewHolder(context);
    }
}
