package cn.tjad.tjad2.module.document.viewholder;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hjy.http.download.DownloadErrorType;
import com.hjy.http.download.DownloadManager;
import com.hjy.http.download.FileDownloadInfo;
import com.hjy.http.download.FileDownloadTask;
import com.hjy.http.download.listener.OnDownloadProgressListener;
import com.hjy.http.download.listener.OnDownloadingListener;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.utils.Tools;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class DownloadViewHolder extends SimpleBaseViewHolder<FileDownloadTask> implements OnDownloadProgressListener, OnDownloadingListener {

    @BindView(R.id.file_name)
    TextView fileName;
    @BindView(R.id.file_size)
    TextView fileSize;
    @BindView(R.id.file_time)
    TextView time;
    @BindView(R.id.file_path)
    TextView path;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.file_status)
    TextView statusTV;

    private FileDownloadTask data;

    private Handler handler;

    public DownloadViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void setData(FileDownloadTask data, int position) {
        this.data = data;
        FileDownloadInfo downloadInfo = data.getFileDownloadInfo();
        File outFile = downloadInfo.getOutFile();
        fileName.setText(outFile.getName());
        fileSize.setText(Tools.getDisplayedFileSize(outFile.length()));
        long timeStamp = data.getStartTime();
        time.setText(Tools.getFormatTime(timeStamp, "yyyy-MM-dd HH:mm"));
        path.setText(outFile.getPath());
        int status = data.getDownloadStatus();
        if (status == 0) {
            statusTV.setText("下载中");
        } else if (status == 1) {
            statusTV.setText("下载成功");
        } else if (status == 2) {
            statusTV.setText("下载失败");
        } else {
            statusTV.setText("");
        }
//        if (data.isPending()) {
//            statusTV.setText("等待WIFI");
//        }
        int progress = data.getProgress();
        progressBar.setProgress(progress);
        String url = downloadInfo.getUrl();
        if (data.getDownloadingListener() == null) {
            data.setDownloadingListener(this);
        }
        if (data.getProgressListener() == null) {
            data.setProgressListener(this);
        }
        DownloadManager.getInstance().addDownloadingListener(url, this);
        DownloadManager.getInstance().addDownloadingProgressListener(url, this);

    }

    /**
     * @param downloadInfo 下载信息
     * @param current      Downloaded size in bytes.
     * @param totalSize    Total size in bytes.
     */
    @Override
    public void onProgressUpdate(FileDownloadTask downloadInfo, long current, final long totalSize) {
        final int progress = (int) (100.0 * current / totalSize);
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progress != progressBar.getProgress()) {
                    progressBar.setProgress(progress);
                    statusTV.setText("正在下载");
                    fileSize.setText(Tools.getDisplayedFileSize(totalSize));
                    data.setDownloadStatus(FileDownloadTask.STATUS_DOWNLOADING);
                }
            }
        });

//
    }

    /**
     * 下载失败
     *
     * @param task      Downdload task
     * @param errorType {@link DownloadErrorType}
     * @param msg       错误信息
     */
    @Override
    public void onDownloadFailed(FileDownloadTask task, int errorType, String msg) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                data.setDownloadStatus(FileDownloadTask.STATUS_DOWNLOAD_FAILED);
                statusTV.setText("下载失败");
            }
        });

    }

    /**
     * 下载成功
     *
     * @param task    Download task
     * @param outFile 下载成功后的文件
     */
    @Override
    public void onDownloadSucc(FileDownloadTask task, File outFile) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                data.setDownloadStatus(FileDownloadTask.STATUS_DOWNLOAD_SUCCESS);
                statusTV.setText("下载成功");
            }
        });

    }
}
