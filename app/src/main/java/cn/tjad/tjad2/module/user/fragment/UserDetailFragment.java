package cn.tjad.tjad2.module.user.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qiandian.android.base.fragment.BaseFragment;
import com.qiandian.android.base.widget.TJADTTitleView;

import butterknife.BindView;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.utils.Tools;

public class UserDetailFragment extends BaseFragment {

    @BindView(R.id.user_head)
    ImageView userHead;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_email)
    TextView email;
    @BindView(R.id.user_phone)
    TextView phone;
    @BindView(R.id.user_company)
    TextView company;
    @BindView(R.id.user_department)
    TextView department;
    @BindView(R.id.user_title)
    TextView title;
    @BindView(R.id.user_responsible)
    TextView responsible;

    @Override
    public View getContentView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.user_detail, null);
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.setCenterText("详细信息");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            Tools.showToast("无效参数");
        } else {
            User user = (User) bundle.getSerializable("user");
            if (user != null) {
                userName.setText(user.name);
                email.setText(user.email);
                phone.setText(user.phone);
                if (user.company != null) {
                    company.setText(user.company.name);
                }
                if (user.department != null) {
                    department.setText(user.department.name);
                    responsible.setText(user.department.responsibility);
                    title.setText(user.department.area);
                }
                if (user.avatar != null) {
                    Glide.with(this)
                            .load(user.avatar)
                            .into(userHead);
                }

            }
        }
    }
}
