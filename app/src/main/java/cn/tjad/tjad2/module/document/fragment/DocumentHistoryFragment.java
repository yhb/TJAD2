package cn.tjad.tjad2.module.document.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.CommonBean;

import java.util.ArrayList;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.fragment.RefreshableListFragment;
import cn.tjad.tjad2.module.document.adapter.DocHistoryAdapter;
import cn.tjad.tjad2.net.http.ResponseObserver;
import cn.tjad.tjad2.net.http.RetrofitManager;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.FileHandler;
import cn.tjad.tjad2.utils.RxUtils;
import cn.tjad.tjad2.utils.Tools;

@Route(path = PageId.DOC_HISTORY)
public class DocumentHistoryFragment extends RefreshableListFragment implements View.OnClickListener {

    private int fileId = -1;
    private String fileName;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addHeadView(R.layout.doc_history_head);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setEnableRefresh(true);
        if (getArguments() != null && getArguments().containsKey(Keys.FILE_ID)) {
            fileId = getArguments().getInt(Keys.FILE_ID);
            fileName = getArguments().getString(Keys.TITLE);
        } else {
            Tools.showToast("请传入正确的fileId");
            getActivity().finish();
        }
        requestData();
    }

    @Override
    protected void requestData() {
        RetrofitManager.getInstance()
                .getApiService()
                .getDocHistory(Tools.getProjectId(), fileId)
                .compose(RxUtils.networkTransformer())
                .subscribe(new ResponseObserver<ArrayList<FileBean>>(compositeDisposable) {
                    @Override
                    public void onSuccess(CommonBean<ArrayList<FileBean>> t) {
                        adapter.updateData(t.result);
                        adapter.notifyDataSetChanged();
                        onDataLoadSuccess(null);
                    }

                    @Override
                    public void onFailed(String message) {
                        Tools.showToast(message);
                        onDataLoadFailed();
                    }
                });
    }

    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        adapter = new DocHistoryAdapter(null);
        adapter.addElementClickListener(R.id.doc_history_item_operate, this);
        return adapter;
    }


    private FileHandler fileHandler;
    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        FileBean fileBean = (FileBean) v.getTag(R.id.list_item_data);
        if (fileHandler == null) {
            fileHandler = new FileHandler(getContext());
        }
        String name = Tools.addVersionToFileName(fileName, String.valueOf(fileBean.version));
        if (fileBean != null) {
            fileHandler.downloadFileWithCheck(getContext(), fileBean.path, name, null);
        }
    }
}
