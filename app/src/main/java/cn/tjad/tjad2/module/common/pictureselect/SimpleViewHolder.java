package cn.tjad.tjad2.module.common.pictureselect;

import android.view.View;

/**
 * Created by yanghongbing on 17/10/24.
 */

public class SimpleViewHolder extends ViewHolder {

    public SimpleViewHolder(View parentView) {
        super(parentView);
    }

    @Override
    public void setValue(Object o, int position) {

    }

    @Override
    public int getLayoutId() {
        return 0;
    }
}
