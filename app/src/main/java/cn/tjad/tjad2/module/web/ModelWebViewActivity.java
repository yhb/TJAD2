package cn.tjad.tjad2.module.web;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.document.fragment.ModelFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.MODEL_PAGE)
public class ModelWebViewActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ModelFragment fragment = new ModelFragment();
        Bundle args = getIntent().getExtras();
        fragment.setArguments(args);
        setContentView(fragment);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
