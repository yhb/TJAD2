package cn.tjad.tjad2.module.document.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.tjad.tjad2.R;

public class DocumentFiledSelectWidget extends FrameLayout {

    @BindView(R.id.project_doc_row)
    ViewGroup projectDocRow;
    @BindView(R.id.my_doc_row)
    ViewGroup myDocRow;
    @BindView(R.id.rubish_row)
    ViewGroup rubishRow;
    @BindView(R.id.project_doc_checkbox)
    RadioButton projectCB;
    @BindView(R.id.my_doc_checkbox)
    RadioButton myCB;
    @BindView(R.id.rubish_checkbox)
    RadioButton rubishCB;

    public static final int PROJECT_DOC = 0;
    public static final int MY_DOC = 1;
    public static final int RUBISH_DOC = 2;

    private OnDocumentTypeSelectListener onDocumentTypeSelectListener;

    public DocumentFiledSelectWidget(@NonNull Context context) {
        this(context, null);
    }

    public DocumentFiledSelectWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.document_select_layout, this);
        ButterKnife.bind(this);
    }

    public interface OnDocumentTypeSelectListener {
        void onDocumentTypeSelect(int type);
    }

    public void setOnDocumentTypeSelectListener(OnDocumentTypeSelectListener onDocumentTypeSelectListener) {
        this.onDocumentTypeSelectListener = onDocumentTypeSelectListener;
    }

    @OnClick({R.id.project_doc_row, R.id.my_doc_row, R.id.rubish_row})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.project_doc_row:
                check(PROJECT_DOC);
                break;
            case R.id.my_doc_row:
                check(MY_DOC);
                break;
            case R.id.rubish_row:
                check(RUBISH_DOC);
                break;
        }
    }

    public void check(int doc) {
        if (doc == PROJECT_DOC) {
            projectCB.setVisibility(VISIBLE);
            myCB.setVisibility(INVISIBLE);
            rubishCB.setVisibility(INVISIBLE);
        } else if (doc == MY_DOC) {
            projectCB.setVisibility(INVISIBLE);
            myCB.setVisibility(VISIBLE);
            rubishCB.setVisibility(INVISIBLE);
        } else if (doc == RUBISH_DOC) {
            projectCB.setVisibility(INVISIBLE);
            myCB.setVisibility(INVISIBLE);
            rubishCB.setVisibility(VISIBLE);
        } else {
            throw new RuntimeException("无此选项");
        }
        if (onDocumentTypeSelectListener != null) {
            onDocumentTypeSelectListener.onDocumentTypeSelect(doc);
        }
    }
}
