package cn.tjad.tjad2.module.document.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FileBean;

public class FileDirectoryIndicator extends HorizontalScrollView {

    private LinearLayout contentView;
    private OnIndicatorClickListener onIndicatorClickListener;

    public FileDirectoryIndicator(Context context) {
        this(context, null);
    }

    public FileDirectoryIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        contentView = new LinearLayout(getContext());
        contentView.setOrientation(LinearLayout.HORIZONTAL);
        contentView.setGravity(Gravity.CENTER_VERTICAL);
        this.addView(contentView);
    }

    public void addFileDirectory(FileBean fileDirectory) {
        if (fileDirectory == null || !fileDirectory.directory) {
            return;
        }
        addIndicator(fileDirectory);
    }

    public void setOnIndicatorClickListener(OnIndicatorClickListener onIndicatorClickListener) {
        this.onIndicatorClickListener = onIndicatorClickListener;
    }

    private void addIndicator(FileBean fileBean) {
        TextView textView = new TextView(getContext());
        textView.setMaxLines(1);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(12);
        textView.setTextColor(Color.parseColor("#666666"));
        textView.setMaxEms(10);
        textView.setText(fileBean.name);
        textView.setTag(fileBean);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.mipmap.icon_right, 0);
        }
        textView.setPadding(5,3, 5, 3);
        textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeLowerLayer(v);
                FileBean file = (FileBean) v.getTag();
                if (onIndicatorClickListener != null) {
                    onIndicatorClickListener.onIndicatorClick(file);
                }
            }
        });
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        contentView.addView(textView, lp);
    }

    private void removeLowerLayer(View v) {
        int count = contentView.getChildCount();
        if (count <= 1) {
            return;
        }
        for (int i = count - 1; i >= 0; i--) {
            if (v != contentView.getChildAt(i)) {
                contentView.removeViewAt(i);
            } else {
                break;
            }
        }
    }

    public interface OnIndicatorClickListener {
        void onIndicatorClick(FileBean directory);
    }

    public boolean goBack() {
        int count = contentView.getChildCount();
        if (count <= 1) {
            return false;
        } else {
            View view = contentView.getChildAt(count - 2);
            view.performClick();
            return true;
        }
    }

    public void goRoot() {
        int count = contentView.getChildCount();
        if (count > 0) {
            contentView.getChildAt(0).performClick();
        }
    }

}
