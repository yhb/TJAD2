package cn.tjad.tjad2.module.common.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.utils.Tools;

public class CreateFolderDialog extends TjDialogFragment {

    private EditText editText;
    private OnConfirmListener onConfirmListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setCreateFolderView();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void setCreateFolderView() {
        editText = (EditText) getLayoutInflater().inflate(R.layout.file_operate_input_text, null);
        editText.setHint("请输入文件夹名称");
        this.setView(editText)
                .setTitle("新建文件夹")
                .setPositiveButton("确认", new OnDialogClickListener() {
                    @Override
                    public boolean onClick(Dialog dialog, int which) {
                        String text = editText.getText().toString().trim();
                        if (TextUtils.isEmpty(text)) {
                            Tools.showToast("文件夹名称不能为空！");
                            return false;
                        } else {
                            if (onConfirmListener != null) {
                                onConfirmListener.onConfirm(text);
                            }
                        }
                        return true;
                    }
                })
                .setNegativeButton("取消", null);
    }

    public interface OnConfirmListener {
        void onConfirm(String text);
    }

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }

}
