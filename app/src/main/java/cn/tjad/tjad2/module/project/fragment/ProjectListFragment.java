package cn.tjad.tjad2.module.project.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.qiandian.android.base.common.bean.CommonListBean;
import com.qiandian.android.base.net.http.HttpConstants;
import com.qiandian.android.base.net.http.HttpId;
import com.qiandian.android.base.net.http.IDataCallBack;
import com.qiandian.android.base.widget.TJADTTitleView;

import cn.tjad.tjad2.module.common.fragment.RefreshableListFragment;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.business.RemoteDataService;
import cn.tjad.tjad2.data.bean.Project;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.project.adapter.ProjectListAdapter;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.Tools;

public class ProjectListFragment extends RefreshableListFragment implements OnItemClickListener, IDataCallBack {

    private ProjectListAdapter projectListAdapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        projectListAdapter = (ProjectListAdapter) adapter;
        adapter.setOnItemClickListener(this);
        requestData();
    }

    @Override
    protected boolean showTitle() {
        return true;
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        super.setCustomerTitle(titleView);
        titleView.showLeft(false);
        titleView.setCenterText("项目");
        titleView.showRight(false);
    }

    @Override
    protected void requestData() {
        RemoteDataService.getProjectList(this, startIndex, PAGE_COUNT);
    }

    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        return new ProjectListAdapter(null);
    }

    @Override
    public boolean onReceiveError(int httpId, int errorCode, String errorMsg) {
        if (httpId == HttpId.PROJECT_LIST) {
            Tools.showToast(errorMsg);
            onDataLoadFailed();
        }
        return false;
    }

    @Override
    public void onDataRefresh(int httpId, Object responseBean) {
        if (httpId == HttpId.PROJECT_LIST) {
            CommonListBean<Project> bean = (CommonListBean<Project>) responseBean;
            if (bean.resCode == HttpConstants.CODE_SUCCESS) {
                projectListAdapter.updateData(bean.result, !isRefresh);
                projectListAdapter.notifyDataSetChanged();
                onDataLoadSuccess(bean.pagination);
            } else {
                Tools.showToast(bean.respMsg);
            }
        }
    }

//    @Override
//    public void onItemClick(View parentView, View itemView, int position) {
//        if (position != projectListAdapter.getCheckedPosision()) {
//            projectListAdapter.setCheckedPosition(position);
//            Project project = projectListAdapter.getItemData(position);
//            TjadApplication.getInstance().getSession().setProject(project);
//            ProjectChangedEvent event = new ProjectChangedEvent();
//            event.setProjectId(project.id);
//            EventBus.getDefault().post(event);
//        }
//    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        Project project = projectListAdapter.getItemData(position);
        TjadApplication.getInstance().getSession().setProject(project);
        RouteUtils.toPage(getContext(), PageId.MAIN, null);
        Log.d("tjad", "选中项目: " + project.name + ", " + project.id);
        getActivity().finish();
    }

}
