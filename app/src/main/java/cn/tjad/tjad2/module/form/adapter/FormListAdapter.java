package cn.tjad.tjad2.module.form.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.form.viewHolder.FormListViewHolder;

public class FormListAdapter extends SimpleRecyclerViewAdapter {
    public FormListAdapter(List data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.form_list_item, parent, false);
        return new FormListViewHolder(view);
    }
}
