package cn.tjad.tjad2.module.design.designDetail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.module.common.pictureselect.GridViewForScrollView;
import cn.tjad.tjad2.module.common.pictureselect.TJBaseAdapter;
import cn.tjad.tjad2.module.common.pictureselect.ViewHolder;

public class MemberListView extends GridViewForScrollView {

    private final List<User> data = new ArrayList<>();
    private MemberAdapter memberAdapter;

    public MemberListView( @NonNull Context context) {
        this(context, null);
    }

    public MemberListView( @NonNull Context context,  @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setNumColumns(5);
        memberAdapter = new MemberAdapter(getContext(), data);
        setAdapter(memberAdapter);
    }

    public void setData(List<User> data) {
        this.data.clear();
        if (data != null) {
            this.data.addAll(data);
        }
        memberAdapter.notifyDataSetChanged();
    }

    public List<User> getData() {
        return data;
    }

    static class MemberAdapter extends TJBaseAdapter {

        public MemberAdapter(Context context, List data) {
            super(context, data);
        }

        @Override
        public ViewHolder getViewHolder(int position) {
            return new MemberViewHolder(context);
        }
    }

    static class MemberViewHolder extends ViewHolder<User> {
        @BindView(R.id.member_icon)
        ImageView memberIcon;
        @BindView(R.id.member_name)
        TextView memberNameTV;
        public MemberViewHolder(Context context) {
            super(context);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setValue(User user, int position) {
            if (user == null) {
                return;
            }
            if (!TextUtils.isEmpty(user.avatar)) {
                Glide.with(itemView)
                        .load(user.avatar)
                        .into(memberIcon);
            }
            memberNameTV.setText(user.name);
        }

        @Override
        public int getLayoutId() {
            return R.layout.member_layout;
        }
    }

}
