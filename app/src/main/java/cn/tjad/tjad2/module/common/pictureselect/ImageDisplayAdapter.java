package cn.tjad.tjad2.module.common.pictureselect;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import cn.tjad.tjad2.R;

/**
 * Created by yanghongbing on 17/10/24.
 */

public class ImageDisplayAdapter extends TJBaseAdapter {

    private final int ITEM_TYPE_ADD = 0;
    private final int ITEM_TYPE_IMAGE = 1;

    private View.OnClickListener onAddImageClickListener, onDeleteClickListener, onImageClickListener;

    public ImageDisplayAdapter(Context context, List data) {
        super(context, data);
    }

    @Override
    public int getCount() {
        if (data == null)
            return 1;
        return data.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getCount() - 1) {
            return ITEM_TYPE_ADD;
        }
        return ITEM_TYPE_IMAGE;
    }

    @Override
    public Object getItem(int position) {
        if (getItemViewType(position) == ITEM_TYPE_IMAGE) {
            return data.get(position);
        }
        return null;
    }

    @Override
    public ViewHolder getViewHolder(int position) {
        ViewHolder viewHolder;
        if (getItemViewType(position) == ITEM_TYPE_ADD) {
            ImageView imageView = new ImageView(context);
            imageView.setImageResource(R.mipmap.icon_add_image);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setOnClickListener(onAddImageClickListener);
            viewHolder = new SimpleViewHolder(imageView);
        } else {
            viewHolder =  new ImageDisplayViewHolder(context);
            ((ImageDisplayViewHolder) viewHolder).setOnDeleteClickListener(onDeleteClickListener);
            ((ImageDisplayViewHolder) viewHolder).setOnImageClickListener(onImageClickListener);
        }
        viewHolder.setItemViewType(getItemViewType(position));
        return viewHolder;
    }

    public void setOnAddImageClickListener(View.OnClickListener onClickListener) {
        this.onAddImageClickListener = onClickListener;
    }

    public void setOnDeleteClickListener(View.OnClickListener onClickListener) {
        this.onDeleteClickListener = onClickListener;
    }

    public void setOnImageClickListener(View.OnClickListener onClickListener) {
        this.onImageClickListener = onClickListener;
    }
}
