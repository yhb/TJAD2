package cn.tjad.tjad2.module.notice.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.UnreadMessage;
import cn.tjad.tjad2.module.notice.viewholder.MessageMenuViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class MessageMenuAdapter extends SimpleRecyclerViewAdapter<UnreadMessage> {

    public MessageMenuAdapter(List<UnreadMessage> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<UnreadMessage> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_menu_item, parent, false);
        return new MessageMenuViewHolder(view);
    }
}
