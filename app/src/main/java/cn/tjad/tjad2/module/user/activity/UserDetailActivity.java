package cn.tjad.tjad2.module.user.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.user.fragment.UserDetailFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.MEMBER_USER_DETAIL)
public class UserDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserDetailFragment fragment = new UserDetailFragment();
        fragment.setArguments(getIntent().getExtras());
        setContentView(fragment);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
