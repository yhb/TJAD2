package cn.tjad.tjad2.module.construction.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.common.bean.CommonListBean;
import com.qiandian.android.base.net.http.HttpConstants;
import com.qiandian.android.base.net.http.HttpId;
import com.qiandian.android.base.net.http.IDataCallBack;
import com.qiandian.android.base.widget.TJADTTitleView;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.business.RemoteDataService;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.data.bean.Construction;
import cn.tjad.tjad2.module.common.fragment.RefreshableListFragment;
import cn.tjad.tjad2.listener.OnItemClickListener;
import cn.tjad.tjad2.module.construction.adapter.ConstructionListViewAdapter;
import cn.tjad.tjad2.route.PageId;
import cn.tjad.tjad2.utils.RouteUtils;
import cn.tjad.tjad2.utils.Tools;

/**
 * 施工页面
 */

@Route(path = PageId.CONSTRUCTION_LIST)
public class ConstructionFragment extends RefreshableListFragment implements IDataCallBack, OnItemClickListener {

    @Override
    protected boolean showTitle() {
        return true;
    }

    @Override
    protected void setCustomerTitle(TJADTTitleView titleView) {
        titleView.showLeft(true);
        titleView.setLeftImage(R.drawable.icon_back);
        titleView.showRight(false);
        titleView.setCenterText("施工");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter.setOnItemClickListener(this::onItemClick);
        requestData();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        if (isVisible()) {
//            requestData();
//        }
//    }
//
//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (!hidden) {
//            requestData();
//        }
//    }

    @Override
    protected void requestData() {
        RemoteDataService.getConstructionList(Tools.getProjectId(), startIndex, PAGE_COUNT, this);
    }


    @Override
    protected SimpleRecyclerViewAdapter getAdapter() {
        return new ConstructionListViewAdapter(null);
    }

    @Override
    public boolean onReceiveError(int httpId, int errorCode, String errorMsg) {
        if (httpId == HttpId.CONSTRUCTION_LIST) {
            if (TextUtils.isEmpty(errorMsg)) {
                Tools.showToast("获取施工列表失败");
            } else {
                Tools.showToast(errorMsg);
            }
            onDataLoadFailed();
        }
        return false;
    }

    @Override
    public void onDataRefresh(int httpId, Object responseBean) {
        if (httpId == HttpId.CONSTRUCTION_LIST) {
            CommonListBean<Construction> bean = (CommonListBean<Construction>) responseBean;
            if (bean.resCode == HttpConstants.CODE_SUCCESS) {
                adapter.updateData(bean.result, !isRefresh);
                adapter.notifyDataSetChanged();
                onDataLoadSuccess(bean.pagination);
            } else {
                Tools.showToast(bean.respMsg);
            }
        }
    }

    @Override
    public void onItemClick(View parentView, View itemView, int position) {
        Construction construction = (Construction) adapter.getItemData(position);
        if (construction != null) {
            Bundle bundle = new Bundle();
            bundle.putInt(Keys.CONSTRUCTION_ID, construction.id);
            RouteUtils.toPage(getContext(), PageId.CONSTRUCTION_DETAIL, bundle);
        }
    }

}
