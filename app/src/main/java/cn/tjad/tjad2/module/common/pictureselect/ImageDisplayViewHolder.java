package cn.tjad.tjad2.module.common.pictureselect;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.luck.picture.lib.entity.LocalMedia;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;

/**
 * Created by yanghongbing on 17/10/24.
 */

public class ImageDisplayViewHolder extends ViewHolder<LocalMedia> {

    @BindView(R.id.image_item_image)
    ImageView imageView;
    @BindView(R.id.image_item_delete)
    ImageView deleteIV;


    public ImageDisplayViewHolder(Context context) {
        super(context);
        ButterKnife.bind(this, itemView);
    }

    public void setOnDeleteClickListener(View.OnClickListener listener) {
        deleteIV.setOnClickListener(listener);
    }

    public void setOnImageClickListener(View.OnClickListener listener) {
        imageView.setOnClickListener(listener);
    }

    @Override
    public void setValue(LocalMedia localMedia, int position) {
        String path = "";
        if (localMedia.isCompressed()) {
            path = localMedia.getCompressPath();
        } else if (localMedia.isCut()) {
            path = localMedia.getCutPath();
        } else {
            path = localMedia.getPath();
        }
        deleteIV.setTag(position);
        imageView.setTag(R.id.imageview_position, position);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(itemView.getContext())
                .load(path)
                .apply(options)
                .into(imageView);
    }

    @Override
    public int getLayoutId() {
        return R.layout.image_display_item;
    }
}
