package cn.tjad.tjad2.module.notice.viewholder;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.UnreadMessage;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class MessageMenuViewHolder extends SimpleBaseViewHolder<UnreadMessage> {

    @BindView(R.id.message_type)
    TextView messageTypeTV;
    @BindView(R.id.message_count)
    TextView messageCountTV;

    public MessageMenuViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(UnreadMessage data, int position) {
        messageTypeTV.setText(data.name);
        if (data.unread > 0) {
            messageCountTV.setText(String.valueOf(data.unread));
            messageCountTV.setVisibility(View.VISIBLE);
        } else {
            messageCountTV.setText("");
            messageCountTV.setVisibility(View.GONE);
        }
    }
}
