package cn.tjad.tjad2.module.project.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.Project;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class ProjectListViewHolder extends SimpleBaseViewHolder<Project> {

    @BindView(R.id.project_name)
    TextView projectName;
    @BindView(R.id.project_description)
    TextView projectDescription;
    @BindView(R.id.project_checked)
    ImageView checkedImage;

    private int checkedPosition = -1;

    public ProjectListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setCheckedPosition(int checkedPosition) {
        this.checkedPosition = checkedPosition;
    }

    @Override
    public void setData(Project data, int position) {
        projectName.setText(data.name);
        projectDescription.setText(data.description);
        if (checkedPosition == position) {
            checkedImage.setVisibility(View.VISIBLE);
        } else {
            checkedImage.setVisibility(View.GONE);
        }
    }
}
