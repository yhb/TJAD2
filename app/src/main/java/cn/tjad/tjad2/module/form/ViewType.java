package cn.tjad.tjad2.module.form;

public interface ViewType {

    int TEXT = 0;
    int SINGLE_LINE_TEXT = 1;
    int MULTI_LINE_TEXT = 2;
    int SPINNER = 3;
    int COMBOX = 4;
    int VIEWPORT = 5;
    int ATTACHMENT = 6;
    int SELECT_USER = 7;
    int DATE_PICKER = 8;

}
