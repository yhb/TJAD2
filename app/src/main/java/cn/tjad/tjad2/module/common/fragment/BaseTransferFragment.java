package cn.tjad.tjad2.module.common.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.qiandian.android.base.fragment.BaseFragment;

import cn.tjad.tjad2.R;

public class BaseTransferFragment extends BaseFragment {

    protected RecyclerView recyclerView;

    @Override
    public View getContentView(LayoutInflater inflater) {
        View root = inflater.inflate(R.layout.simple_recyclerview, null);
        recyclerView = root.findViewById(R.id.recycler_view);
        return root;
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
