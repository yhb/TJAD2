package cn.tjad.tjad2.module.common.viewholder;

import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.lang.reflect.Field;


public class StringViewHolder extends SimpleBaseViewHolder<Serializable> {

    private TextView textView;

    public StringViewHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView;
    }

    @Override
    public void setData(Serializable data, int position) {
        if (data instanceof String) {
            textView.setText((String)data);
        } else {
            String name = null;
            try {
                Field field = data.getClass().getField("name");
                field.setAccessible(true);
                name = (String) field.get(data);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            textView.setText(name);
        }
    }
}
