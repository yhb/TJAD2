package cn.tjad.tjad2.module.notice.settings.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;

import com.baozi.treerecyclerview.base.ViewHolder;
import com.baozi.treerecyclerview.item.TreeItem;

import org.greenrobot.eventbus.EventBus;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.NoticeSettingElement;
import cn.tjad.tjad2.data.event.NoticeSwitchEvent;

public class NoticeElementItem extends TreeItem<NoticeSettingElement> implements CompoundButton.OnCheckedChangeListener {

    private int position = -1;
    private SwitchCompat switchCompat;
    @Override
    public int getLayoutId() {
        return R.layout.notice_setting_item_2;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        this.position = position;
        switchCompat = viewHolder.getView(R.id.setting_item_2_switch);
        switchCompat.setOnCheckedChangeListener(null);
        viewHolder.setText(R.id.setting_item_2_name, data.name);
        viewHolder.setChecked(R.id.setting_item_2_switch, data.isChecked());
        switchCompat.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        NoticeSwitchEvent event = new NoticeSwitchEvent();
        event.setChecked(isChecked);
        event.setNoticeSettingElement(data);
        event.setPosition(position);
        EventBus.getDefault().post(event);
    }

    public void setChecked(boolean checked) {
        if (switchCompat != null) {
            switchCompat.setChecked(checked);
        } else {
            onCheckedChanged(null, checked);
        }
        data.setChecked(checked);
    }
}
