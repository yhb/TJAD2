package cn.tjad.tjad2.module.form.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.data.bean.ValueId;
import cn.tjad.tjad2.module.form.entity.FormDetailEntity;
import cn.tjad.tjad2.module.form.viewHolder.FormAssignViewHolder;
import cn.tjad.tjad2.module.form.viewHolder.FormCancelViewHolder;
import cn.tjad.tjad2.module.form.viewHolder.FormDetailGroupViewHolder;
import cn.tjad.tjad2.module.form.viewHolder.FormDetailItemViewHolder;
import cn.tjad.tjad2.module.form.viewHolder.FormRejectViewHolder;
import cn.tjad.tjad2.module.form.views.FormBaseLayout;

public class FormDetailAdapter extends RecyclerView.Adapter {

    private final int TYPE_GROUP = 1;
    private final int TYPE_ITEM = 2;
    private final int TYPE_CANCEL = 3;
    private final int TYPE_REJECT = 4;
    private final int TYPE_ASSIGN = 5;

    private boolean editable;

    private boolean showGroup = true; // 是否显示分组标题，默认显示

    private final List<FormDetailEntity> formDetailEntities = new ArrayList<>();

    private final SparseArray<ValueId> changedMap = new SparseArray<>();

    public void setFormStep(List<FormDetail.FormDetailStep> formSteps) {
        changedMap.clear();
        convertToFlatList(formSteps);
        notifyDataSetChanged();
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public void setShowGroup(boolean showGroup) {
        this.showGroup = showGroup;
    }

    public SparseArray<ValueId> getChangedMap() {
        return changedMap;
    }

    private void convertToFlatList(List<FormDetail.FormDetailStep> list) {
        this.formDetailEntities.clear();
        for (FormDetail.FormDetailStep step: list) {
            if (step.type == 1) { //form reject
                FormDetailEntity<FormDetail.FormDetailStep> item1 = new FormDetailEntity<>();
                item1.type = TYPE_REJECT;
                item1.data = step;
                formDetailEntities.add(item1);
            } else if (step.type == 2) { // form cancel
                FormDetailEntity<FormDetail.FormDetailStep> item2 = new FormDetailEntity<>();
                item2.type = TYPE_CANCEL;
                item2.data = step;
                formDetailEntities.add(item2);
            } else if (step.type == 3) { // form assign
                FormDetailEntity<FormDetail.FormDetailStep> item3 = new FormDetailEntity<>();
                item3.type = TYPE_ASSIGN;
                item3.data = step;
                formDetailEntities.add(item3);
            } else {
                if (showGroup) {
                    FormDetailEntity<FormDetail.FormDetailStep> group = new FormDetailEntity<>();
                    group.type = TYPE_GROUP;
                    group.data = step;
                    formDetailEntities.add(group);
                }
                if (step.details != null && step.details.size() > 0) {
                    for (FormDetail.FormStepDetail detail : step.details) {
                        FormDetailEntity<FormDetail.FormStepDetail> item = new FormDetailEntity<>();
                        item.type = TYPE_ITEM;
                        item.data = detail;
                        formDetailEntities.add(item);
                    }
                }
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_GROUP) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.form_detail_item_level1_view, parent, false);
            FormDetailGroupViewHolder vh = new FormDetailGroupViewHolder(view);
            vh.setIsRecyclable(false);
            return vh;
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.form_detail_item_level2_view, parent, false);
            FormDetailItemViewHolder viewHolder = new FormDetailItemViewHolder(view);
            viewHolder.setIsRecyclable(false);
            viewHolder.setEditable(editable);
            viewHolder.setOnEditListener(new FormBaseLayout.IOnEditListener() {
                @Override
                public void onEdit(int id, String value) {
                    ValueId valueId = new ValueId();
                    valueId.id = id;
                    valueId.value = value;
                    changedMap.put(id, valueId);
                }
            });
            return viewHolder;
        } else if (viewType == TYPE_REJECT) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.form_item_reject, parent, false);
            FormRejectViewHolder viewHolder = new FormRejectViewHolder(view);
            viewHolder.setIsRecyclable(false);
            return viewHolder;
        } else if (viewType == TYPE_CANCEL) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.form_item_cancel, parent, false);
            FormCancelViewHolder viewHolder = new FormCancelViewHolder(view);
            viewHolder.setIsRecyclable(false);
            return viewHolder;
        } else if (viewType == TYPE_ASSIGN) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.form_item_assign, parent, false);
            FormAssignViewHolder viewHolder = new FormAssignViewHolder(view);
            viewHolder.setIsRecyclable(false);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        FormDetailEntity entity = formDetailEntities.get(position);
        if (holder instanceof FormDetailGroupViewHolder && entity.data instanceof FormDetail.FormDetailStep) {
            ((FormDetailGroupViewHolder) holder).setData((FormDetail.FormDetailStep) entity.data, position);
        } else if (holder instanceof FormDetailItemViewHolder && entity.data instanceof FormDetail.FormStepDetail) {
            ((FormDetailItemViewHolder) holder).setEditable(editable);
            ((FormDetailItemViewHolder) holder).setData((FormDetail.FormStepDetail) entity.data, position);
        } else if (holder instanceof FormRejectViewHolder) {
            ((FormRejectViewHolder) holder).setData((FormDetail.FormDetailStep) entity.data, position);
        } else if (holder instanceof FormCancelViewHolder) {
            ((FormCancelViewHolder) holder).setData((FormDetail.FormDetailStep) entity.data, position);
        } else if (holder instanceof FormAssignViewHolder) {
            ((FormAssignViewHolder) holder).setData((FormDetail.FormDetailStep) entity.data, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return formDetailEntities.get(position).type;
    }

    @Override
    public int getItemCount() {
        return formDetailEntities.size();
    }
}
