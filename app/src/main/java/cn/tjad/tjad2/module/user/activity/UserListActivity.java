package cn.tjad.tjad2.module.user.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.qiandian.android.base.activity.BaseActivity;

import cn.tjad.tjad2.module.user.fragment.UserListFragment;
import cn.tjad.tjad2.route.PageId;

@Route(path = PageId.USER_LIST)
public class UserListActivity extends BaseActivity {

    private UserListFragment userListFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userListFragment = new UserListFragment();
        userListFragment.setArguments(getIntent().getExtras());
        setContentView(userListFragment);
    }

    @Override
    protected boolean showTitle() {
        return false;
    }
}
