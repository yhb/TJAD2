package cn.tjad.tjad2.module.common.adapter;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.application.TjadApplication;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.common.viewholder.StringViewHolder;

public class StringListAdapter extends SimpleRecyclerViewAdapter<Serializable> {
    int itemMinHeight;
    int leftRightPadding;

    public StringListAdapter(List<Serializable> data) {
        super(data);
        Resources res = TjadApplication.getInstance().getResources();
        itemMinHeight = res.getDimensionPixelSize(R.dimen.list_item_min_height);
        leftRightPadding = res.getDimensionPixelSize(R.dimen.left_right_padding);
    }

    @Override
    protected SimpleBaseViewHolder<Serializable> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView textView = new TextView(parent.getContext());
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setBackgroundResource(R.drawable.item_click_color);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setMinHeight(itemMinHeight);
        textView.setPadding(leftRightPadding, 0, leftRightPadding, 0);
        textView.setLayoutParams(lp);
        return new StringViewHolder(textView);
    }

    protected  void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("列表为空", 0);
    }
}
