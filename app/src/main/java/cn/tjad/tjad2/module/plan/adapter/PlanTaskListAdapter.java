package cn.tjad.tjad2.module.plan.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.Task;
import cn.tjad.tjad2.module.plan.viewholder.PlanTaskListViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;

public class PlanTaskListAdapter extends SimpleRecyclerViewAdapter<Task> {
    public PlanTaskListAdapter(List<Task> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<Task> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.plan_task_list_item, parent, false);
        return new PlanTaskListViewHolder(view);
    }
}
