package cn.tjad.tjad2.module.design.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.common.adapter.SimpleRecyclerViewAdapter;
import cn.tjad.tjad2.data.bean.DesignItem;
import cn.tjad.tjad2.module.common.viewholder.EmptyViewHolder;
import cn.tjad.tjad2.module.common.viewholder.SimpleBaseViewHolder;
import cn.tjad.tjad2.module.design.viewholder.DesignListViewHolder;

public class DesignListAdapter extends SimpleRecyclerViewAdapter<DesignItem> {

    public DesignListAdapter(List<DesignItem> data) {
        super(data);
    }

    @Override
    protected SimpleBaseViewHolder<DesignItem> onCreateNormalViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_list_item, parent, false);
        return new DesignListViewHolder(view);
    }

    @Override
    protected void fillEmptyViewData(EmptyViewHolder holder) {
        holder.setData("暂无设计项目", 0);
    }
}
