package cn.tjad.tjad2.listener;

import android.view.View;

public interface OnItemLongClickListener {
    boolean onItemLongClick(View parentView, View itemView, int position);
}
