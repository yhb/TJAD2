package cn.tjad.tjad2.listener;

import java.util.List;

/**
 * Created by yanghongbing on 17/10/25.
 */

public interface IImageToStringCallback {

    public void onProgress(int index);

    public void onFinish(List<String> imageDataList);
}
