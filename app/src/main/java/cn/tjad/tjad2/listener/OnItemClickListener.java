package cn.tjad.tjad2.listener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View parentView, View itemView, int position);
}
