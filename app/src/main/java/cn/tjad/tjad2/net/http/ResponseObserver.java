package cn.tjad.tjad2.net.http;


import com.qiandian.android.base.common.bean.CommonBean;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class ResponseObserver<T> implements Observer<CommonBean<T>> {

    private CompositeDisposable compositeDisposable;

    public ResponseObserver(CompositeDisposable disposable) {
        this.compositeDisposable = disposable;
    }

    @Override
    public void onSubscribe(Disposable d) {
        if (compositeDisposable != null) {
            compositeDisposable.add(d);
        }
    }

    @Override
    public void onNext(CommonBean<T> t) {
        if (t.resCode == 0) {
            onSuccess(t);
        } else {
            onFailed(t.respMsg);
        }
    }

    @Override
    public void onError(Throwable e) {
        onFailed(e.getMessage());
    }

    @Override
    public void onComplete() {

    }

    public abstract void onSuccess(CommonBean<T> t);

    public abstract void onFailed(String message);
}
