package cn.tjad.tjad2.net.http;

import com.qiandian.android.base.common.bean.BaseBean;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.bean.CommonListBean;

import java.util.ArrayList;
import java.util.List;

import cn.tjad.tjad2.consts.UrlConsts;
import cn.tjad.tjad2.data.bean.CustomFormBean;
import cn.tjad.tjad2.data.bean.DesignDetail;
import cn.tjad.tjad2.data.bean.DesignItem;
import cn.tjad.tjad2.data.bean.FileBean;
import cn.tjad.tjad2.data.bean.FormBean;
import cn.tjad.tjad2.data.bean.FormDetail;
import cn.tjad.tjad2.data.bean.Id;
import cn.tjad.tjad2.data.bean.MaterialBean;
import cn.tjad.tjad2.data.bean.MaterialDetail;
import cn.tjad.tjad2.data.bean.MessageBean;
import cn.tjad.tjad2.data.bean.MessageCount;
import cn.tjad.tjad2.data.bean.NoticeSetting;
import cn.tjad.tjad2.data.bean.Plan;
import cn.tjad.tjad2.data.bean.PlanTask;
import cn.tjad.tjad2.data.bean.Task;
import cn.tjad.tjad2.data.bean.Type;
import cn.tjad.tjad2.data.bean.UnReadMessageBean;
import cn.tjad.tjad2.data.bean.User;
import cn.tjad.tjad2.module.form.entity.CreateFormRequestBean;
import cn.tjad.tjad2.module.form.entity.FormAsignRequestBean;
import cn.tjad.tjad2.module.form.entity.RecallRequestBean;
import cn.tjad.tjad2.module.form.entity.RejectFormRequestBean;
import cn.tjad.tjad2.module.form.entity.UpdateFormRequestBean;
import cn.tjad.tjad2.module.material.bean.request.AsignRequestBean;
import cn.tjad.tjad2.module.material.bean.request.ItemRequestBean;
import cn.tjad.tjad2.module.notice.settings.bean.UpDateNoticeSettingRequestBean;
import cn.tjad.tjad2.module.plan.TaskEditRequestBean;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET(UrlConsts.DESIGN_LIST)
    Observable<CommonListBean<DesignItem>> getDesignList(
            @Path ("projectId")int projectId,
            @Query("start")int start,
            @Query("limit")int limit,
            @Query("consStatus")String consStatus,
            @Query("typeId")String typeId,
            @Query("domain")String domain,
            @Query("userId")String userId);

    @GET(UrlConsts.DESIGN_DETAIL)
    Observable<CommonBean<DesignDetail>> getDesignDetail(
            @Path ("projectId")int projectId,
            @Path("designId")int designId
    );

    @POST(UrlConsts.REPLY_DESIGN)
    Observable<CommonBean<String>> replyDesign(
            @Path ("projectId")int projectId,
            @Path("designId")int designId,
            @Body RequestBody requestBody
            );

    @POST(UrlConsts.FINISH_DESIGN)
    Observable<CommonBean<String>> finishDesign(
            @Path ("projectId")int projectId,
            @Path("designId")int designId,
            @Body RequestBody requestBody
    );

    @GET(UrlConsts.DESIGN_TYPE)
    Observable<CommonListBean<Type>> getDesignType(@Path("projectId") int projectId);

    @GET(UrlConsts.PLAN_LIST)
    Observable<CommonListBean<Plan>> getPlanList(@Path("projectId") int projectId,
                                                 @Query("start") int start,
                                                 @Query("limit") int limit);

    @GET(UrlConsts.PLAN_DETAIL)
    Observable<CommonBean<PlanTask>> getPlanDetail(@Path("projectId") int projectId, @Path("id")int planId);

    @POST(UrlConsts.UPDATE_PLAN_PROGRESS)
    Observable<CommonBean<BaseBean>> updatePlanProgress(
            @Path("projectId") int projectId,
            @Path("id")int planId,
            @Body TaskEditRequestBean requestBean);

    @GET(UrlConsts.USER_SETTINGS)
    Observable<CommonBean<NoticeSetting>> getUserSettings();

    @POST(UrlConsts.UPDATE_SETTINGS)
    Observable<CommonBean<BaseBean>> updateSettings(@Body UpDateNoticeSettingRequestBean bean);

    @GET(UrlConsts.MESSAGE_LIST)
    Observable<CommonListBean<MessageBean>> getMessageList(
            @Path("projectId") int projectId,
            @Query("type") int type,
            @Query("start") int start,
            @Query("limit") int limit);

    @POST(UrlConsts.READ_MESSAGE)
    Observable<CommonBean<BaseBean>> readMessage(@Path("projectId") int projectId, @Path("id") int messageId);

    @GET(UrlConsts.MESSAGE_COUNT)
    Observable<CommonBean<MessageCount>> getMessageCount(@Path("projectId") int projectId, @Query("read") boolean read);

    @GET(UrlConsts.UNREAD_MESSAGE_COUNT)
    Observable<CommonBean<UnReadMessageBean>> getUnreadMessage(@Path("projectId") int projectId);

    @GET(UrlConsts.DOC_HISTORY)
    Observable<CommonBean<ArrayList<FileBean>>> getDocHistory(@Path("projectId") int projectId, @Path("fileId") int fileId);

    @GET(UrlConsts.USERS)
    Observable<CommonBean<ArrayList<User>>> getUsers(@Path("projectId") int projectId);

    @GET(UrlConsts.TASK_LIST)
    Observable<CommonBean<Task>> getTaskDetail(@Path("projectId") int projectId, @Path("planId") int planId, @Path("taskId") int taskId);

    @GET(UrlConsts.MATERIAL_TRACKING_LIST)
    Observable<CommonListBean<MaterialBean>> getMaterialTrackingList(
            @Path("projectId") int projectId,
            @Query("start") int start,
            @Query("limit") int limit);

    @GET(UrlConsts.MATERIAL_TRAKING_DETAIL)
    Observable<CommonBean<MaterialDetail>> getMaterialTrackingDetail(
            @Path("projectId") int projectId,
            @Path("id") int materialId
    );

    @PUT(UrlConsts.MATERIAL_ASIGN)
    Observable<CommonBean<BaseBean>> asignMaterial(
            @Path("projectId") int projectId,
            @Path("id") int materialId,
            @Body AsignRequestBean requestBean);

    @PUT(UrlConsts.UPDATE_MATERIAL_TRAKING_PROGRESS)
    Observable<CommonBean<BaseBean>> updateMaterial(
            @Path("projectId") int projectId,
            @Path("id") int materialId,
            @Body ItemRequestBean requestBean);

    @PUT(UrlConsts.BACK_MATERIAL_TRAKING_PROGRESS)
    Observable<CommonBean<BaseBean>> backMaterial(
            @Path("projectId") int projectId,
            @Path("id") int materialId,
            @Body ItemRequestBean requestBean);

    @GET(UrlConsts.GET_FORM_LIST)
    Observable<CommonListBean<FormBean>> getFormList(
            @Path("projectId") int projectId,
            @Query("start") int start,
            @Query("limit") int limit
    );

    @GET(UrlConsts.GET_FORM_DETAIL)
    Observable<CommonBean<FormDetail>> getFormDetail(
            @Path("projectId") int projectId,
            @Path("id") int formId
    );

    @POST(UrlConsts.UPLOAD_FILE_URL)
    Observable<CommonBean<FileBean>> uploadFile(
            @Path("projectId") int projectId,
            @Body RequestBody body
            );

    @PUT(UrlConsts.UPDATE_FORM)
    Observable<CommonBean<BaseBean>> updateForm(
            @Path("projectId") int projectId,
            @Path("formId") int formId,
            @Body UpdateFormRequestBean requestBean
            );

    @PUT(UrlConsts.FORM_REJECT)
    Observable<CommonBean<BaseBean>> rejectForm(
            @Path("projectId") int projectId,
            @Path("formId") int formId,
            @Body RejectFormRequestBean requestBean
            );

    @PUT(UrlConsts.RECALL_FORM)
    Observable<CommonBean<BaseBean>> recallForm(
            @Path("projectId") int projectId,
            @Path("formId") int formId,
            @Body RecallRequestBean requestBean
            );

    @PUT(UrlConsts.ASSIGN_FORM)
    Observable<CommonBean<BaseBean>> assignForm(
            @Path("projectId") int projectId,
            @Path("formId") int formId,
            @Body FormAsignRequestBean requestBean
    );

    @GET(UrlConsts.CUSTOM_FORMS)
    Observable<CommonListBean<CustomFormBean>> getCustomForms(
            @Path("projectId") int projectId,
            @Query("start") int start,
            @Query("limit") int limit
    );

    @POST(UrlConsts.CREATE_FORM)
    Observable<CommonBean<BaseBean>> createForm(
            @Path("projectId") int projectId,
            @Body CreateFormRequestBean requestBean
            );

    @GET(UrlConsts.FILE_LIST_URL)
    Observable<CommonBean<FileBean>> getFileList(
            @Path("projectId") int projectId,
            @Query("suffix") String suffix
    );
}
