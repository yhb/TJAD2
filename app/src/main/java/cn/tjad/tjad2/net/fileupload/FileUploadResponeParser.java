package cn.tjad.tjad2.net.fileupload;

import com.hjy.http.upload.parser.BaseResponseParser;
import com.hjy.http.upload.parser.ParserResult;
import com.qiandian.android.base.common.bean.CommonBean;
import com.qiandian.android.base.common.utils.JsonUtils;
import com.qiandian.android.base.net.http.HttpConstants;

public class FileUploadResponeParser extends BaseResponseParser {

    @Override
    public ParserResult process(String responseStr) throws Exception {
        CommonBean bean = JsonUtils.parseObject(responseStr, String.class, CommonBean.class);
        ParserResult<CommonBean> result = new ParserResult<CommonBean>(bean) {
            @Override
            public boolean isSuccessful() {
                if (data.resCode == HttpConstants.CODE_SUCCESS) {
                    return true;
                }
                return false;
            }

            @Override
            public String getMsg() {
                return data.respMsg;
            }
        };
        return result;
    }
}
