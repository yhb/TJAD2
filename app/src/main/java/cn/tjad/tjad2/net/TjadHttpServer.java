package cn.tjad.tjad2.net;

import android.util.Log;

import com.koushikdutta.async.http.Headers;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class TjadHttpServer implements HttpServerRequestCallback {

    private static volatile TjadHttpServer instance;
    private AsyncHttpServer httpServer;

    public static final int PORT = 9578;

    private TjadHttpServer() {
//        httpServer = new AsyncHttpServer();
    }

    public static TjadHttpServer getInstance() {
        if (instance == null) {
            synchronized (TjadHttpServer.class) {
                if (instance == null) {
                    instance = new TjadHttpServer();
                }
            }
        }
        return instance;
    }

    @Override
    public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
        Headers headers = response.getHeaders();
        headers.add("Access-Control-Allow-Headers", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, HEAD");
        headers.add("Access-Cont;rol-Allow-Credentials", "true");
        headers.add("Access-Control-Max-Age", "" + 42 * 60 * 60);
        headers.add("Access-Control-Allow-Origin", "*");
        String path = request.getPath();
        path = path.replaceFirst("/", "");
        try {
            path = URLDecoder.decode(path, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//        if ("favicon.ico".equals(path)) {
//            Log.d("tjad", "favicon.ico : " + response.code());
//            response.end();
//        } else {
            Log.d("tjad", "http server path = " + path);
            File file = new File(path);
            if (!file.exists()) {
                response.send("file not exists");
            } else {
                int code = response.code();
                Log.d("tjad", "response code = " + code);
                response.sendFile(file);
            }
//        }
    }

    public static enum Status {
        REQUEST_OK(200, "请求成功"),
        REQUEST_ERROR(500, "请求失败"),
        REQUEST_ERROR_API(501, "无效的请求接口"),
        REQUEST_ERROR_CMD(502, "无效命令"),
        REQUEST_ERROR_DEVICEID(503, "不匹配的设备ID"),
        REQUEST_ERROR_ENV(504, "不匹配的服务环境");

        private final int requestStatus;
        private final String description;

        Status(int requestStatus, String description) {
            this.requestStatus = requestStatus;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public int getRequestStatus() {
            return requestStatus;
        }
    }

    public void startServer() {
        if (httpServer == null) {
            httpServer = new AsyncHttpServer();
            httpServer.addAction("OPTIONS", "[\\d\\D]*", this);
            httpServer.addAction("GET", "[\\d\\D]*", this);
            httpServer.addAction("POST", "[\\d\\D]*", this);
            httpServer.get("[\\d\\D]*", this);
            httpServer.post("[\\d\\D]*", this);
            httpServer.listen(PORT);
        }
    }

    public void stopServer() {
        if (httpServer != null) {
            httpServer.stop();
            httpServer = null;
        }
    }



}
