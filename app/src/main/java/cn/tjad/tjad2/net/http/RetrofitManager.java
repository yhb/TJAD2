package cn.tjad.tjad2.net.http;

import com.qiandian.android.base.net.http.HttpUtils;

import cn.tjad.tjad2.consts.UrlConsts;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitManager {
    private static volatile RetrofitManager instance;
    private Retrofit retrofit;

    private ApiService apiService;

    private RetrofitManager() {
        initRetrofit();
    }

    public static RetrofitManager getInstance() {
        if (instance == null) {
            synchronized (RetrofitManager.class) {
                if (instance == null) {
                    instance = new RetrofitManager();
                }
            }
        }
        return instance;
    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(UrlConsts.HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(HttpUtils.getHttpClient())
                .build();
    }

    public <T> T createReq(Class<T> reqServer) {
        return retrofit.create(reqServer);
    }

    public ApiService getApiService() {
        if (apiService == null) {
            apiService = createReq(ApiService.class);
        }
        return apiService;
    }
}
