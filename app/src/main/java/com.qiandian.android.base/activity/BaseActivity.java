package com.qiandian.android.base.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.qiandian.android.base.common.utils.StatusBarUtil;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.util.List;

import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.module.backhandle.BackHandlerHelper;
import cn.tjad.tjad2.utils.AppUtils;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import pub.devrel.easypermissions.EasyPermissions;


public class BaseActivity extends AppCompatActivity implements TJADTTitleView.OnTitleClickListener {

    private TJADTTitleView titleView;
    private FrameLayout bodyLayout;

    protected CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
    }

    protected boolean showTitle() {
        return true;
    }

    @Override
    public void setContentView(int layoutResID) {
        if (showTitle()) {
            super.setContentView(R.layout.base_activity);
            titleView = findViewById(R.id.title_view);
            bodyLayout = findViewById(R.id.body_layout);
            getLayoutInflater().inflate(layoutResID, bodyLayout);
            setDefaultTitle();
            setCustomerTitle(titleView);
            StatusBarUtil.setGradientColor(this, titleView);
        } else {
            super.setContentView(layoutResID);
        }

        ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view) {
        if (showTitle()) {
            super.setContentView(R.layout.base_activity);
            titleView = findViewById(R.id.title_view);
            bodyLayout = findViewById(R.id.body_layout);
            bodyLayout.removeAllViews();
            bodyLayout.addView(view);
            setDefaultTitle();
            setCustomerTitle(titleView);
            StatusBarUtil.setGradientColor(this, titleView);
        } else {
            super.setContentView(view);
        }
        ButterKnife.bind(this);
    }

    public void setContentView(Fragment fragment) {
        if (fragment == null) {
            throw  new RuntimeException("fragment不能为null");
        }
        int containerId = android.R.id.content;
        if (showTitle()) {
            super.setContentView(R.layout.base_activity);
            StatusBarUtil.setColor(this, AppUtils.getColor(R.color.title_gradient_start));
            titleView = findViewById(R.id.title_view);
            bodyLayout = findViewById(R.id.body_layout);
            bodyLayout.removeAllViews();
            setDefaultTitle();
            setCustomerTitle(titleView);
            containerId = bodyLayout.getId();
            StatusBarUtil.setGradientColor(this, titleView);
        }
        ButterKnife.bind(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(containerId, fragment)
                .commitAllowingStateLoss();
    }

    private void setDefaultTitle() {
        if (titleView != null) {
            titleView.showLeft(true);
            titleView.setLeftImage(R.drawable.icon_back);
            titleView.setLeftText(null);
            titleView.showCenter(true);
            titleView.setCenterImage(0);
            titleView.showRight(false);
            titleView.setOnTitleClickListener(this);
            Intent intent = getIntent();
            String title = intent.getStringExtra("title");
            if (title != null) {
                titleView.setCenterText(title);
            }
        }
    }

    public TJADTTitleView getTitleView() {
        return titleView;
    }

    protected void setCustomerTitle(TJADTTitleView titleView) {

    }

    @Override
    public void onTitleClick(int which) {
        if (which == TJADTTitleView.TITLE_LEFT) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (!BackHandlerHelper.handleBackPress(this)) {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        List<Fragment> fragments = fragmentManager.getFragments();
//        if (fragments != null && fragments.size() > 0) {
//            for (Fragment f : fragments) {
//                f.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            }
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                if (fragment.isVisible()) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }

    public void addSubscribe(Disposable d) {
        compositeDisposable.add(d);
    }
}
