package com.qiandian.android.base.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.tjad.tjad2.R;

public class TJADTTitleView extends FrameLayout {

    @BindView(R.id.title_left_layout)
    ViewGroup leftLayout;
    @BindView(R.id.title_left_image)
    ImageView leftImage;
    @BindView(R.id.title_left_text)
    TextView leftText;

    @BindView(R.id.title_center_layout)
    ViewGroup centerLayout;
    @BindView(R.id.title_center_img)
    ImageView centerImage;
    @BindView(R.id.title_center_text)
    TextView centerText;

    @BindView(R.id.title_right_layout)
    ViewGroup rightLayout;
    @BindView(R.id.title_right_img)
    ImageView rightImage;
    @BindView(R.id.title_right_text)
    TextView rightText;

    @BindView(R.id.title_right_layout_2)
    ViewGroup rightLayout2;
    @BindView(R.id.title_right_img_2)
    ImageView rightImage2;
    @BindView(R.id.title_right_text_2)
    TextView rightText2;

    public static final int TITLE_LEFT = 0;
    public static final int TITLE_CENTER = 1;
    public static final int TITLE_RIGHT = 2;
    public static final int TITLE_RIGHT_2 = 3;

    private OnTitleClickListener onTitleClickListener;

    public TJADTTitleView(Context context) {
        this(context, null);
    }

    public TJADTTitleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TJADTTitleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.title_view, this);
        ButterKnife.bind(this);
    }

    public void showLeft(boolean show) {
        if (show) {
            leftLayout.setVisibility(VISIBLE);
        } else {
            leftLayout.setVisibility(GONE);
        }
    }

    public void setLeftImage(int resId) {
        if (resId <= 0) {
            leftImage.setVisibility(GONE);
        } else {
            leftImage.setVisibility(VISIBLE);
            leftImage.setImageResource(resId);
        }
    }

    public void setLeftText(String text) {
        if (text == null) {
            leftText.setVisibility(GONE);
        } else {
            leftText.setVisibility(VISIBLE);
            leftText.setText(text);
        }
    }

    public void showCenter(boolean show) {
        if (show) {
            centerLayout.setVisibility(VISIBLE);
        } else {
            centerLayout.setVisibility(GONE);
        }
    }

    public void setCenterImage(int resId) {
        if (resId <= 0) {
            centerImage.setVisibility(GONE);
        } else {
            centerImage.setVisibility(VISIBLE);
            centerImage.setImageResource(resId);
        }
    }

    public void setCenterText(String text) {
        if (text == null) {
            centerText.setVisibility(GONE);
        } else {
            centerText.setVisibility(VISIBLE);
            centerText.setText(text);
        }
    }

    public void showRight(boolean show) {
        if (show) {
            rightLayout.setVisibility(VISIBLE);
        } else {
            rightLayout.setVisibility(GONE);
        }
    }

    public void setRightImage(int resId) {
        if (resId <= 0) {
            rightImage.setVisibility(GONE);
        } else {
            rightImage.setVisibility(VISIBLE);
            rightImage.setImageResource(resId);
        }
    }

    public void setRightText(String text) {
        if (text == null) {
            rightText.setVisibility(GONE);
        } else {
            rightText.setVisibility(VISIBLE);
            rightText.setText(text);
        }
    }

    public void showRight2(boolean show) {
        if (show) {
            rightLayout2.setVisibility(VISIBLE);
        } else {
            rightLayout2.setVisibility(GONE);
        }
    }

    public void setRightImage2(int resId) {
        if (resId <= 0) {
            rightImage2.setVisibility(GONE);
        } else {
            rightImage2.setVisibility(VISIBLE);
            rightImage2.setImageResource(resId);
        }
    }

    public void setRightText2(String text) {
        if (text == null) {
            rightText2.setVisibility(GONE);
        } else {
            rightText2.setVisibility(VISIBLE);
            rightText2.setText(text);
        }
    }

    public ImageView getRightImageView() {
        return rightImage;
    }

    public void setOnTitleClickListener(OnTitleClickListener listener) {
        this.onTitleClickListener = listener;
    }

    public interface OnTitleClickListener {
        void onTitleClick(int which);
    }

    @OnClick({R.id.title_left_layout, R.id.title_center_layout, R.id.title_right_layout, R.id.title_right_layout_2})
    public void onLeftClick(View view) {
        int which = -1;
        int i = view.getId();
        if (i == R.id.title_left_layout) {
            which = TITLE_LEFT;

        } else if (i == R.id.title_center_layout) {
            which = TITLE_CENTER;

        } else if (i == R.id.title_right_layout) {
            which = TITLE_RIGHT;
        } else if (i == R.id.title_right_layout_2) {
            which = TITLE_RIGHT_2;
        }
        if (onTitleClickListener != null) {
            onTitleClickListener.onTitleClick(which);
        }
    }
}
