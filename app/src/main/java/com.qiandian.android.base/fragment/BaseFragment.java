package com.qiandian.android.base.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.qiandian.android.base.activity.BaseActivity;
import com.qiandian.android.base.common.utils.StatusBarUtil;
import com.qiandian.android.base.widget.TJADTTitleView;

import java.util.List;

import butterknife.ButterKnife;
import cn.tjad.tjad2.R;
import cn.tjad.tjad2.consts.Keys;
import cn.tjad.tjad2.module.common.fragment.FragmentContainer;
import cn.tjad.tjad2.module.backhandle.BackHandledFragment;
import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by yanghongbing on 17/10/15.
 */

public abstract class BaseFragment extends BackHandledFragment implements TJADTTitleView.OnTitleClickListener {

    protected boolean needTitle = true;

    //needTitle = false时为null
    protected TJADTTitleView titleView;

    protected CompositeDisposable compositeDisposable;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (showTitle()) {
            view = inflater.inflate(R.layout.base_activity, null);
            FrameLayout body = (FrameLayout) view.findViewById(R.id.body_layout);
            titleView = (TJADTTitleView) view.findViewById(R.id.title_view);
            body.removeAllViews();
            View bodyView = getContentView(inflater);
            if (bodyView != null) {
                body.addView(bodyView);
            }
            setDefaultTitle();
            setCustomerTitle(titleView);
            StatusBarUtil.setGradientColor(getActivity(), titleView);
        } else {
            view = getContentView(inflater);
            titleView = ((BaseActivity) getActivity()).getTitleView();
            if (titleView != null) {
                setDefaultTitle();
                setCustomerTitle(titleView);
            }
        }
        ButterKnife.bind(this, view);
        return view;
    }

    protected boolean showTitle() {
        return needTitle;
    }

    private void setDefaultTitle() {
        if (titleView != null) {
            titleView.showLeft(true);
            titleView.setLeftImage(R.drawable.icon_back);
            titleView.setLeftText(null);
            titleView.showCenter(true);
            titleView.setCenterImage(0);
            titleView.showRight(false);
            titleView.setOnTitleClickListener(this);

            Bundle bundle = getArguments();
            if (bundle != null) {
                String title = bundle.getString(Keys.TITLE);
                if (title != null) {
                    titleView.setCenterText(title);
                }
            }
        }
    }

    protected void setCustomerTitle(TJADTTitleView titleView) {

    }

    public abstract View getContentView(LayoutInflater inflater);

    @Override
    public void onTitleClick(int which) {
        if (which == TJADTTitleView.TITLE_LEFT) {
            Fragment parent = getParentFragment();
            if (parent instanceof FragmentContainer) {
                ((FragmentContainer) parent).interceptBackPressed();
            } else {
                getActivity().finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults,this);
        FragmentManager fragmentManager = getChildFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment f : fragments) {
                f.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }
}
