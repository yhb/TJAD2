package com.qiandian.android.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import java.util.NoSuchElementException;
import java.util.Vector;

public class ActivityRecords implements Application.ActivityLifecycleCallbacks {

    private final Vector<Activity> activityStack = new Vector<>();

    public Activity getTopActivity() {
        Activity activity = null;
        try {
            activity = activityStack.lastElement();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return activity;
    }

    public void finishAll() {
        if (activityStack != null && activityStack.size() > 0) {
            for (Activity activity : activityStack) {
                activity.finish();
            }
        }
    }

    public void finishAllExceptTop() {
        if (activityStack != null && activityStack.size() > 0) {
            for (Activity activity : activityStack) {
                if (activityStack.size() > 1) {
                    activity.finish();
                }
            }
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        activityStack.add(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        activityStack.removeElement(activity);
    }
}
