package com.qiandian.android.base.net.http;

/**
 * Created by yanghongbing on 17/10/18.
 */

public interface HttpId {


    int LOGIN = 1001; //登录

    int REGISTER = 1002; //注册

    int MODIFY_USER_INFO = 1003; //修改信息

    int USER_INFO = 1004; //个人信息

    int MODIFY_PASSWORD = 1005;//修改密码

    int PROJECT_LIST = 2001; //项目列表

    int FILE_LIST = 2002;//文件列表

    int CREATE_FOLDER = 2003; //创建文件夹

    int CREATE_FILE = 2004; //新建文件

    int DELETE_FILE = 2005; //删除文件

    int RENAME_FILE = 2006; //重命名文件

    int MOVE_FILE = 2007; //移动文件

    int MODEL = 2008; //模型

    int DOC_SHARE = 2009; //文件共享

    int DOC_LINK = 2010; //文件链接

    int ADD_DOC_LINK = 2011;//增加文件链接

    int DELETE_DOC_LINK = 2012;//删除文件链接

    int PERMISSION_LIST = 2013; //权限列表

    int ADD_PERMISSION = 2014; //增加权限

    int DELETE_PERMISSION = 2015; //删除权限

    int UPDATE_PERMISSION = 2016; //修改权限

    int SHARE_CONTENT = 2017; //获取分享的内容

    int UPLOAD_IMAGE = 3001; //上传图片

    int MEMBER_LIST = 4001; //成员架构

    int CONSTRUCTION_LIST = 5001; //施工列表

    int CONSTRUCTION_DETAIL = 5002; //施工详情

    int CONSTRUCTION_FINISH = 5003; //施工完成

}
