package com.qiandian.android.base.net.http;

/**
 * Created by yanghongbing on 17/10/18.
 */

public class HttpConstants {

    public static final String TAG = "tjad-http";

    public static final int CODE_SUCCESS = 0;

    public static final int TOKEN_EXPIRED = 1002;
}
