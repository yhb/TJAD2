package com.qiandian.android.base.net.http;


import com.qiandian.android.base.common.utils.JsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Callback;

public class HttpRequest {

    private int httpId;
    private String url;
    private Callback httpListener;
    private Map<String, Object> paramsMap;
    private long requestSendTimeMap = 0;

    //给某个请求设置单独的超时时间
    private int timeOutInMilliseconds = 0;

    //头信息
    public Map<String, List<String>> headMap;

    public static final String PARAM_LIST_KEY = "paramListKey";


    public HttpRequest(int httpId, String url) {
        this(httpId, url, null);
    }

    public HttpRequest(int httpId, String url, Map<String, Object> params) {
        this.url = url;
        this.httpId = httpId;
        this.paramsMap = params;
        headMap = new HashMap<String, List<String>>();
        requestSendTimeMap = System.currentTimeMillis();
    }

    //设置map格式的参数
    public void setParams(Map<String, Object> params) {
        this.paramsMap = params;
    }


    public String getUrl() {
        return url;
    }

    public int getHttpId() {
        return httpId;
    }


    public Callback getHttpCallBack() {
        return httpListener;
    }


    public void setHttpCallBack(Callback httpListener) {
        this.httpListener = httpListener;

    }


    /**
     * 获取string格式的参数
     */
    public String getParamsString() {
        if (paramsMap == null) {
            return null;
        }
        if (paramsMap.size() == 1 && paramsMap.containsKey(PARAM_LIST_KEY)) {
            return (String) paramsMap.get(PARAM_LIST_KEY);
        }
        return JsonUtils.toJSONString(paramsMap);
    }

    /**
     * 获取Map格式的参数
     */
    public Map<String, Object> getParamsMap() {
        return paramsMap;
    }

    /**
     * 获取头信息
     */
    public Map<String, List<String>> getHeadMap() {
        return headMap;
    }

    /**
     * 设置头信息
     */
    public void setHeadMap(Map<String, List<String>> headMap) {
        this.headMap = headMap;
    }


    public void addHeader(String key, String value) {
        ArrayList<String> values = new ArrayList<String>();
        values.add(value);
        headMap.put(key, values);
    }
    
    public void setTimeOutInMilliseconds(int timeout) {
        this.timeOutInMilliseconds = timeout;
    }

    public int getTimeOutInMilliseconds() {
        return this.timeOutInMilliseconds;
    }

    public long getRequestSendTimeMap() {
        return requestSendTimeMap;
    }

    public void setRequestSendTimeMap(long requestSendTimeMap) {
        this.requestSendTimeMap = requestSendTimeMap;
    }
}
