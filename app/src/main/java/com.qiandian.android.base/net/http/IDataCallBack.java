package com.qiandian.android.base.net.http;

/**
 * Created by yanghongbing on 17/10/18.
 */

public interface IDataCallBack {

    //返回值为true, 表示不需要底层弹提示，由调用方自己提示错误，否则由底层统一弹出
    public boolean onReceiveError(int httpId, int errorCode, String errorMsg);

    public void onDataRefresh(int httpId, Object responseBean);

}
