package com.qiandian.android.base.common.utils;

/**
 * Created by yanghongbing on 17/10/22.
 */

public class UrlUtils {
    public static String getUrl(String rawUrl, String... args) {
        if (args.length == 0) {
            return rawUrl;
        }
        String newUrl = rawUrl;
        for (String s : args) {
            newUrl = newUrl.replaceFirst("\\$\\*\\$", s);
        }
        return newUrl;
    }
}
