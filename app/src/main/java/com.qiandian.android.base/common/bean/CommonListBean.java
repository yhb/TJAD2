package com.qiandian.android.base.common.bean;

import java.util.ArrayList;

import cn.tjad.tjad2.data.bean.Pagination;

/**
 * Created by yanghongbing on 17/10/19.
 */

public class CommonListBean<T> extends CommonBean<ArrayList<T>> {
    public Pagination pagination;
}
