package com.qiandian.android.base.common.bean;

/**
 * Created by yanghongbing on 17/10/18.
 */

public class CommonBean<T> extends BaseBean {

    public int resCode;
    public String respMsg;

    public T result;
}
