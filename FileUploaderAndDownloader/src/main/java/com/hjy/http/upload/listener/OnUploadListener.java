package com.hjy.http.upload.listener;


import com.hjy.http.upload.FileUploadInfo;
import com.hjy.http.upload.FileUploadTask;

/**
 * Created by hjy on 7/7/15.<br>
 */
public interface OnUploadListener<T> {

    /**
     * 上传失败
     *
     * @param uploadTask
     * @param errorType {@link com.hjy.http.upload.ErrorType}
     * @param msg
     */
    public void onError(FileUploadTask uploadTask, int errorType, String msg);

    /**
     * 上传成功
     *
     * @param uploadTask
     * @param data 数据返回的解析结果
     */
    public void onSuccess(FileUploadTask uploadTask, T data);

}
