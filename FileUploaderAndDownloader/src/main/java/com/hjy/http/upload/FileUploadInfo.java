package com.hjy.http.upload;


import com.hjy.http.upload.listener.OnUploadListener;
import com.hjy.http.upload.listener.OnUploadProgressListener;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by hjy on 7/8/15.<br>
 */
public class FileUploadInfo {

    private Map<String, String> formParamMap;

    private String id;
    private String filePath;             //要上传的文件路径
    private String mimeType;
    private String url;

    private List<OnUploadListener> apiCallbackList = new CopyOnWriteArrayList<>();
    private List<OnUploadProgressListener> progressListenerList = new CopyOnWriteArrayList<>();

    private UploadOptions uploadOptions;

    private String preProcessedFile;     //上传前对文件预处理后，生成的临时文件

    public FileUploadInfo(Map<String, String> formParamMap, String id, String filePath, String mimeType, String url,
                          OnUploadListener apiCallback, OnUploadProgressListener progressListener, UploadOptions uploadOptions) {
        this.formParamMap = formParamMap;
        this.id = id;
        this.filePath = filePath;
        this.mimeType = mimeType;
        this.url = url;
        apiCallbackList.add(apiCallback);
        progressListenerList.add(progressListener);
        this.uploadOptions = uploadOptions;
    }

    @Override
    public String toString() {
        return "FileUploadInfo{" +
                "apiCallback=" + apiCallbackList +
                ", formParamMap=" + formParamMap +
                ", id='" + id + '\'' +
                ", filePath='" + filePath + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", url='" + url + '\'' +
                ", progressListener=" + progressListenerList +
                ", uploadOptions=" + uploadOptions +
                '}';
    }

    public String getOriginalFilePath() {
        return filePath;
    }

    public String getUploadFilePath() {
        if(preProcessedFile != null && !preProcessedFile.trim().equals("")) {
            return preProcessedFile;
        }
        return filePath;
    }

    public List<OnUploadListener> getApiCallbackList() {
        return apiCallbackList;
    }

    public List<OnUploadProgressListener> getProgressListenerList() {
        return progressListenerList;
    }

    public void addApiCallback(OnUploadListener listener) {
        if (!apiCallbackList.contains(listener)) {
            apiCallbackList.add(listener);
        }
    }

    public void addProgressListener(OnUploadProgressListener listener) {
        if (!progressListenerList.contains(listener)) {
            progressListenerList.add(listener);
        }
    }


    public Map<String, String> getFormParamMap() {
        return formParamMap;
    }

    public String getId() {
        return id;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setPreProcessedFile(String preProcessedFile) {
        this.preProcessedFile = preProcessedFile;
    }

    public UploadOptions getUploadOptions() {
        return uploadOptions;
    }

    public String getUrl() {
        return url;
    }

}
