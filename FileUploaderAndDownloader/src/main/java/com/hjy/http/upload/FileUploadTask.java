package com.hjy.http.upload;

import android.os.Handler;
import android.os.Looper;

import com.hjy.http.upload.listener.OnFileTransferredListener;
import com.hjy.http.upload.listener.OnUploadListener;
import com.hjy.http.upload.listener.OnUploadProgressListener;
import com.hjy.http.upload.parser.BaseResponseParser;
import com.hjy.http.upload.parser.ParserResult;
import com.hjy.http.upload.preprocessor.BasePreProcessor;
import com.hjy.http.upload.progressaware.ProgressAware;
import com.hjy.http.upload.uploader.BaseUploader;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by hjy on 7/8/15.<br>
 */
public class FileUploadTask implements Runnable {

    private FileUploadEngine mFileUploadEngine;
    private FileUploadConfiguration mFileUploadConfig;
    private FileUploadInfo mFileUploadInfo;
    private Handler mHandler;

    private volatile ProgressAware mProgressAware;

    private int mCurrProgress = 0;

    private volatile boolean mCanceled;

    public static final int UPLOADING = 0;
    public static final int UPLOAD_SUCCESS = 1;
    public static final int UPLOAD_FAILED = 2;

    private int uploadStatus = UPLOADING;

    /**
     * 是否同步加载，默认为false
     */
    private boolean mSyncLoading = false;

    private long startTime;

    public FileUploadTask(FileUploadEngine engine, FileUploadInfo fileUploadInfo, ProgressAware progressAware, Handler handler) {
        mFileUploadEngine = engine;
        mFileUploadConfig = engine.getFileUploadConfiguration();
        mFileUploadInfo = fileUploadInfo;
        mProgressAware = progressAware;
        mHandler = handler;
        startTime = System.currentTimeMillis();
    }

    public void reset() {
        startTime = System.currentTimeMillis();
        uploadStatus = UPLOADING;
        mCanceled = false;
    }

    public void setSyncLoading(boolean syncLoading) {
        mSyncLoading = syncLoading;
    }

    public FileUploadInfo getFileUploadInfo() {
        return mFileUploadInfo;
    }

    public void resetProgressAware(final ProgressAware progressAware) {
        mProgressAware = progressAware;
        if(progressAware != null) {
            if(Looper.myLooper() == Looper.getMainLooper()) {
                progressAware.setProgress(mCurrProgress);
            } else {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressAware.setProgress(mCurrProgress);
                    }
                });
            }
        }
    }

    private OnFileTransferredListener mFileTransferredListener = new OnFileTransferredListener() {

        long tmpTime = 0;

        @Override
        public void transferred(long transferred, long totalSize) {
            if(mCanceled)
                return;

            final long currSize = transferred;
            final int progress = (int)((transferred / (float) totalSize) * 100);
            long now = System.currentTimeMillis();

            //防止频繁刷新，阻塞主线程
            if(now - tmpTime > 100 || progress >= 100) {
                tmpTime = now;
                fireProgressEvent(totalSize, currSize, progress);
            }
            mCurrProgress = progress;
        }
    };

    @Override
    public void run() {
        if(checkCanceled())
            return;

        UploadOptions options = mFileUploadInfo.getUploadOptions();
        if(options != null) {
            BasePreProcessor preProcessor = options.getPreProcessor();
            if(preProcessor != null) {
                String tmpFilePath = preProcessor.process(mFileUploadInfo.getOriginalFilePath());
                mFileUploadInfo.setPreProcessedFile(tmpFilePath);
            }
        }

        BaseUploader fileUploader = mFileUploadConfig.getFileUploader();
        try {
            String respStr = fileUploader.upload(mFileUploadInfo, mFileTransferredListener);

            if(checkCanceled())
                return;

            try {
                BaseResponseParser processor = null;
                if(options != null) {
                    processor = options.getResponseParser();
                }
                if(processor == null) {
                    processor = mFileUploadConfig.getResponseProcessor();
                }

                ParserResult result = processor.process(respStr);
                if(checkCanceled())
                    return;

                if(result.isSuccessful()) {
                    fireSuccessEvent(result);
                } else {
                    fireFailEvent(ErrorType.ERROR_TYPE_BUSINESS_LOGIC_ERROR, result.getMsg());
                }
            } catch (Exception e) {
                e.printStackTrace();
                fireFailEvent(ErrorType.ERROR_TYPE_PARSE_DATA_ERROR, e.getMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
            fireFailEvent(ErrorType.ERROR_TYPE_IO_ERROR, e.getMessage());
        } catch (Exception e) {
            fireFailEvent(ErrorType.ERROR_TYPE_UNKNOWN, e.getMessage());
        }
    }

    private void fireFailEvent(final int errorType, final String errorMsg) {
        removeUploadTask();

        Runnable task = new Runnable() {
            @Override
            public void run() {
                List<OnUploadListener> apiList = mFileUploadInfo.getApiCallbackList();
                if (apiList != null && apiList.size() > 0) {
                    for (OnUploadListener listener : apiList) {
                        if (listener != null) {
                            if (mCanceled)
                                listener.onError(FileUploadTask.this, ErrorType.ERROR_TYPE_CANCELED, null);
                            else
                                listener.onError(FileUploadTask.this, errorType, errorMsg);
                        }
                    }
                }
                ProgressAware pa = mProgressAware;
                cancelUpdateProgressTask(pa);
            }
        };
        uploadStatus = UPLOAD_FAILED;
        runTask(task, null);
    }

    private void fireSuccessEvent(final ParserResult result) {
        removeUploadTask();
        Runnable task = new Runnable() {
            @Override
            public void run() {
                List<OnUploadListener> apiList = mFileUploadInfo.getApiCallbackList();
                if (apiList != null && apiList.size() > 0) {
                    for (OnUploadListener listener : apiList) {
                        if (listener != null) {
                            listener.onSuccess(FileUploadTask.this, result.data);
                        }
                    }
                }
                ProgressAware pa = mProgressAware;
                cancelUpdateProgressTask(pa);
            }
        };
        uploadStatus = UPLOAD_SUCCESS;
        runTask(task, null);
    }

    private void fireProgressEvent(final long totalSize, final long currSize, final int progress) {
        final List<OnUploadProgressListener> progressListenerList = mFileUploadInfo.getProgressListenerList();
        if((progressListenerList == null || progressListenerList.size() == 0) && mProgressAware == null)
            return;

        Runnable task = new Runnable() {
            @Override
            public void run() {
                for (OnUploadProgressListener listener : progressListenerList) {
                    if (listener != null) {
                        listener.onProgress(FileUploadTask.this, totalSize, currSize, progress);
                    }
                }
                ProgressAware pa = mProgressAware;
                if(pa != null) {
                    if(!isProgressViewCollected(pa) && !isProgressViewReused(pa)) {
                        pa.setProgress(progress);
                    } else {
                    }
                }
            }
        };
        uploadStatus = UPLOADING;
        runTask(task, mHandler);
    }

    private void runTask(Runnable task, Handler handler) {
        if(handler != null) {
            handler.post(task);
        } else {
            if(mSyncLoading) {
                task.run();
            } else {
                mHandler.post(task);
            }
        }
    }

    private boolean isProgressViewCollected(ProgressAware progressAware) {
        if(progressAware.isCollected())
            return true;
        return false;
    }

    private boolean isProgressViewReused(ProgressAware progressAware) {
        String currentFileUploadId = mFileUploadEngine.getFileUploadInfoIdForProgressAware(progressAware);
        if(!mFileUploadInfo.getId().equals(currentFileUploadId))
            return true;
        return false;
    }

    private void cancelUpdateProgressTask(ProgressAware progressAware) {
        if(progressAware != null) {
            if(isProgressViewCollected(progressAware)) {
                mFileUploadEngine.cancelUpdateProgressTaskFor(progressAware);
            } else {
                if(!isProgressViewReused(progressAware)) {
                    mFileUploadEngine.cancelUpdateProgressTaskFor(progressAware);
                }
            }
        }
    }

    private void removeUploadTask() {
        mFileUploadEngine.removeTask(this);
    }

    /**
     * 检查是否被取消
     *
     * @return
     */
    private boolean checkCanceled() {
        if(mCanceled) {
            fireFailEvent(ErrorType.ERROR_TYPE_CANCELED, null);
            return true;
        }
        return false;
    }

    public void stopTask() {
        mCanceled = true;
        BaseUploader fileUploader = mFileUploadConfig.getFileUploader();
        fileUploader.cancel(mFileUploadInfo);
    }

    public int getCurrProgress() {
        return mCurrProgress;
    }

    public long getFileSize() {
        String filePath = mFileUploadInfo.getUploadFilePath();
        File file = new File(filePath);
        if (file != null) {
            return file.length();
        }
        return -1;
    }

    public int getUploadStatus() {
        return uploadStatus;
    }

    public long getStartTime() {
        return startTime;
    }

}