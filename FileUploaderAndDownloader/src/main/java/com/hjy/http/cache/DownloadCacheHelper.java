package com.hjy.http.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hjy.http.cache.db.DBSql;
import com.hjy.http.cache.db.TJADDateBaseHelper;

public class DownloadCacheHelper {

    private TJADDateBaseHelper dbHelper;
    private SQLiteDatabase database;

    private final String TABLE_NAME = DBSql.TABLE_DOWNLOAD_HISTORY;

    private static volatile DownloadCacheHelper instance;

    private DownloadCacheHelper() {
    }

    public void init(Context context) {
        this.dbHelper = new TJADDateBaseHelper(context.getApplicationContext(), null);
        database = dbHelper.getWritableDatabase();
    }

    public static DownloadCacheHelper getInstance() {
        if (instance == null) {
            synchronized (DownloadCacheHelper.class) {
                if (instance == null) {
                    instance = new DownloadCacheHelper();
                }
            }
        }
        return instance;
    }

    public void insertOrUpdate(String url, String localPath, String status, String createTime, String fileSize, String downloadSize, String version) {
        List<Map<String, String>> results = getAllRecords();
        if (results == null || results.size() == 0) {
            insert(url, localPath, status, createTime, fileSize, downloadSize, version);
        } else {
            update(url, localPath, status, createTime, fileSize, downloadSize, version);
        }
    }

    public void insert(String url, String localPath, String status, String createTime, String fileSize) {
        insert(url, localPath, status, createTime, fileSize, "", "");
    }

    public void insert(String url, String localPath, String status, String createTime, String fileSize, String downloadSize, String version) {
        ContentValues cv = new ContentValues();
        cv.put(DBSql.COL_DLHIS_URL, url);
        cv.put(DBSql.COL_DLHIS_LOCAL_PATH, localPath);
        cv.put(DBSql.COL_DLHIS_STATUS, status);
        cv.put(DBSql.COL_DLHIS_TIME, createTime);
        cv.put(DBSql.COL_DLHIS_FILE_SIZE, fileSize);
        cv.put(DBSql.COL_DLHIS_FILE_DOWNLOAD_SIZE, downloadSize);
        cv.put(DBSql.COL_DLHIS_FILE_VERSION, version);
        database.insert(TABLE_NAME, DBSql.COL_DLHIS_URL, cv);
    }

    public void update(String url, String localPath, String status, String createTime, String fileSize, String downloadSize, String version) {
        ContentValues cv = new ContentValues();
        cv.put(DBSql.COL_DLHIS_URL, url);
        cv.put(DBSql.COL_DLHIS_LOCAL_PATH, localPath);
        cv.put(DBSql.COL_DLHIS_STATUS, status);
        cv.put(DBSql.COL_DLHIS_TIME, createTime);
        cv.put(DBSql.COL_DLHIS_FILE_SIZE, fileSize);
        cv.put(DBSql.COL_DLHIS_FILE_DOWNLOAD_SIZE, downloadSize);
        cv.put(DBSql.COL_DLHIS_FILE_VERSION, version);
        database.update(TABLE_NAME, cv,"url=?", new String[] {url});
    }

    public void updateStatus(String url, String status) {
        ContentValues cv = new ContentValues();
        cv.put(DBSql.COL_DLHIS_STATUS, status);
        database.update(TABLE_NAME, cv, "url=?", new String[] {url});
    }

    public void updateProgress(String url, long currentSize, long totalSize) {
        ContentValues cv = new ContentValues();
        cv.put(DBSql.COL_DLHIS_FILE_DOWNLOAD_SIZE, currentSize);
        cv.put(DBSql.COL_DLHIS_FILE_SIZE, totalSize);
        database.update(TABLE_NAME, cv, "url=?", new String[] {url});
    }

    public void delete(String url) {
        database.delete(TABLE_NAME, "url=?", new String[] {url});
    }

    public Map<String, String> getRecord(String url) {
        Cursor cursor = database.query(TABLE_NAME, null, "url=?", new String[] {url}, null, null, null);
        Map<String, String> result = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            result = new HashMap<>();
            int colCount = cursor.getColumnCount();
            for (int i = 0; i < colCount; i++) {
                String key = cursor.getColumnName(i);
                String value = cursor.getString(cursor.getColumnIndex(key));
                result.put(key, value);
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return result;
    }

    public List<Map<String, String>> getAllRecords() {
        Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, null);
        List<Map<String, String>> result = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Map<String, String> element = new HashMap<>();
                int colCount = cursor.getColumnCount();
                for (int i = 0; i < colCount; i++) {
                    String key = cursor.getColumnName(i);
                    String value = cursor.getString(cursor.getColumnIndex(key));
                    element.put(key, value);
                }
                result.add(element);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return result;
    }

    public void executeSql(String sql) {
        database.execSQL(sql);
    }

    public void close() {
        database.close();
        dbHelper.close();
    }
}
