package com.hjy.http.cache.db;

public class DBSql {

    public static final String DB_NAME = "tjad_database";

    public static final int DB_VERSION = 1;

    public static final String TABLE_DOWNLOAD_HISTORY = "download_history";


    public static final String COL_DLHIS_ID = "id";
    public static final String COL_DLHIS_URL = "url";
    public static final String COL_DLHIS_LOCAL_PATH = "local_path";
    public static final String COL_DLHIS_STATUS = "status";
    public static final String COL_DLHIS_TIME = "time";
    public static final String COL_DLHIS_FILE_SIZE = "file_size";
    public static final String COL_DLHIS_FILE_DOWNLOAD_SIZE = "download_size";
    public static final String COL_DLHIS_FILE_VERSION = "file_version";


    public static final String CREATE_TABLE_DOWNLOAD_HISTORY =
            "create table if not exists " + TABLE_DOWNLOAD_HISTORY
            + "(" + COL_DLHIS_URL + " TEXT, "
            + COL_DLHIS_LOCAL_PATH + " TEXT, "
            + COL_DLHIS_STATUS + " TEXT, "
            + COL_DLHIS_TIME + " TEXT, "
            + COL_DLHIS_FILE_SIZE + " TEXT, "
            + COL_DLHIS_FILE_DOWNLOAD_SIZE + " TEXT, "
            + COL_DLHIS_FILE_VERSION + " TEXT"
            + ")";
}
