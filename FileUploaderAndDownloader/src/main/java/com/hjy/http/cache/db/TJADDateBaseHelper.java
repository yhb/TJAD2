package com.hjy.http.cache.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TJADDateBaseHelper extends SQLiteOpenHelper {


    public TJADDateBaseHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DBSql.DB_NAME, factory, DBSql.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DBSql.CREATE_TABLE_DOWNLOAD_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
