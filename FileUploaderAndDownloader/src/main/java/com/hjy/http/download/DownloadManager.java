package com.hjy.http.download;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.hjy.http.download.listener.OnDownloadProgressListener;
import com.hjy.http.download.listener.OnDownloadingListener;
import com.hjy.http.upload.progressaware.ProgressAware;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hjy on 5/27/15.<br>
 */
public class DownloadManager {

    private static DownloadManager INSTANCE;

    public static DownloadManager getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new DownloadManager();
        }
        return INSTANCE;
    }

//    private Context mContext;

    private DownloadConfiguration mDownloadConfiguration;

    /**
     * 保存正在下载url
     */
    private List<FileDownloadTask> mTaskList = new ArrayList<FileDownloadTask>();

    //所有下载列表,包括已完成的
    private List<FileDownloadTask> mAllTaskList = new ArrayList<>();

    /**
     * 根据id存放监听器
     */

    private Map<String, List<OnDownloadingListener>> mDowndloadingMap = Collections.synchronizedMap(new HashMap<String, List<OnDownloadingListener>>());
    private Map<String, List<OnDownloadProgressListener>> mProgressMap = Collections.synchronizedMap(new HashMap<String, List<OnDownloadProgressListener>>());

    private Handler mHandler = new Handler(Looper.getMainLooper());

    /**
     * 如果需要显示下载进度条时，key为ProgressAware.getId()，value为FileDownloadInfo.id
     */
    private Map<Integer, String> mCacheKeysForProgressAwares = Collections.synchronizedMap(new HashMap<Integer, String>());


    private DownloadManager() {
//        mContext = context.getApplicationContext();
    }

    public List<FileDownloadTask> getTaskList() {
        return mTaskList;
    }

    public List<FileDownloadTask> getmAllTaskList() {
        return new ArrayList<>(mAllTaskList);
    }

    public synchronized void init(DownloadConfiguration downloadConfiguration) {
        if(downloadConfiguration == null) {
            throw new IllegalArgumentException("DownloadConfiguration can not be null.");
        }
        mDownloadConfiguration = downloadConfiguration;
    }

    private void checkConfiguration() {
        if(mDownloadConfiguration == null) {
            throw new IllegalStateException("Please call init() before use.");
        }
    }

    /**
     * 下载任务是否存在
     *
     * @param id 任务id
     * @param url 下载地址
     * @return true表示正在下载
     */
    public boolean isTaskExists(String id, String url) {
        if(id == null)
            id = "";
        for(FileDownloadTask task : mTaskList) {
            FileDownloadInfo downloadInfo = task.getFileDownloadInfo();
            if(id.equals(downloadInfo.getId()) && url.equals(downloadInfo.getUrl())) {
                return true;
            }
        }
        return false;
    }

    public void downloadFile(int type, String id, String url, OnDownloadingListener downloadingListener) {
        downloadFile(type, id, url, null, downloadingListener,null);
    }

    public void downloadFile(int type, String id, String url, String fileName, OnDownloadingListener downloadingListener, OnDownloadProgressListener downloadProgressListener) {
        downloadFile(type, id, url, fileName, null, downloadingListener, downloadProgressListener);
    }

    /**
     * 下载文件
     *
     * @param type FileType
     * @param id 任务id，自己生成，必须保证唯一
     * @param url 下载地址
     * @param downloadingListener
     * @param downloadProgressListener
     */
    public void downloadFile(int type, String id, String url, String fileName, ProgressAware progressAware, OnDownloadingListener downloadingListener, OnDownloadProgressListener downloadProgressListener) {
        checkConfiguration();
        synchronized (mTaskList) {
            if(isTaskExists(id, url))
                return;
            File cacheFile = generateCacheFile(url, fileName, type);
            FileDownloadInfo downloadInfo = new FileDownloadInfo(id, url, cacheFile, mOnDownloadDispatcher, mOnDwonloadProgressDispatcher);
            FileDownloadTask task = new FileDownloadTask(downloadInfo, this, progressAware);
            mTaskList.add(task);
            mAllTaskList.add(task);
            if(downloadingListener != null)
//                mDowndloadingMap.put(task, downloadingListener);
                putListener(mDowndloadingMap, url, downloadingListener);
            if(downloadProgressListener != null)
//                mProgressMap.put(task, downloadProgressListener);
                putListener(mProgressMap, url, downloadProgressListener);
            if(progressAware != null) {
                prepareUpdateProgressTaskFor(progressAware, downloadInfo.getId());
            }
            mDownloadConfiguration.getTaskExecutor().execute(task);
        }
    }

    /**
     * 更新下载进度
     *
     * @param id taskId
     * @param url 下载地址
     * @param progressAware
     */
    public void updateProgress(String id, String url, ProgressAware progressAware) {
        checkConfiguration();
        synchronized (mTaskList) {
            if(id == null)
                id = "";
            for(FileDownloadTask task : mTaskList) {
                FileDownloadInfo downloadInfo = task.getFileDownloadInfo();
                if(id.equals(downloadInfo.getId()) && url.equals(downloadInfo.getUrl())) {
                    task.resetProgressAware(progressAware, mHandler);
                    break;
                }
            }
        }
    }

    public void prepareUpdateProgressTaskFor(ProgressAware progressAware, String fileDownloadInfoId) {
        mCacheKeysForProgressAwares.put(progressAware.getId(), fileDownloadInfoId);
    }

    public void cancelUpdateProgressTaskFor(ProgressAware progressAware) {
        mCacheKeysForProgressAwares.remove(progressAware.getId());
    }

    public String getFileDownloadInfoIdForProgressAware(ProgressAware progressAware) {
        return mCacheKeysForProgressAwares.get(progressAware.getId());
    }


    public File downloadFileSync(String id, String url) {
        File cacheFile = generateCacheFile(url, null,  FileType.TYPE_OTHER);
        return downloadFileSync(cacheFile, id, url);
    }

    public File downloadFileSync(File cacheFile, String id, String url) {
        return downloadFileSync(cacheFile, id, url, null);
    }

    public File downloadFileSync(File cacheFile, String id, String url, OnDownloadProgressListener progressListener) {
        return downloadFileSync(cacheFile, id, url, null, progressListener);
    }

    /**
     * 同步下载方法
     *
     */
    public File downloadFileSync(File cacheFile, String id, String url, ProgressAware progressAware, OnDownloadProgressListener progressListener) {
        checkConfiguration();
        SyncDownloadLister syncDownloadLister = new SyncDownloadLister();
        FileDownloadInfo downloadInfo = new FileDownloadInfo(id, url, cacheFile, syncDownloadLister, progressListener);
        FileDownloadTask task = new FileDownloadTask(downloadInfo, this, progressAware);
        task.setSyncLoading(true);
//        mDowndloadingMap.put(task, syncDownloadLister);
        putListener(mDowndloadingMap, url, syncDownloadLister);
        if(progressListener != null)
            putListener(mProgressMap, url, progressListener);
//            mProgressMap.put(task, progressListener);
        task.setDownloadNew(true);
        task.run();

        return syncDownloadLister.getResult();
    }

    /**
     * 根据url生成缓存的文件名
     *
     * @param url 下载地址
     * @return 缓存文件名称
     */
    private String generateCacheName(String url) {
        Uri uri = Uri.parse(url);
        String name = url.hashCode() + "_" + System.currentTimeMillis();
        if (uri != null) {
            if (uri.getLastPathSegment() != null) {
                name = uri.getLastPathSegment();
            }
        }
        return name;
    }

    /**
     * 生成缓存的文件
     *
     * @param url 下载url
     * @param type 0-音频，1-视频，2-图片
     * @return 缓存文件
     */
    private File generateCacheFile(String url, String fileName, int type) {
        File cacheDir = mDownloadConfiguration.getCacheDir();
        if (type == FileType.TYPE_AUDIO) {
            cacheDir = new File(cacheDir.getAbsolutePath() + File.separator + "audio");
        } else if (type == FileType.TYPE_VIDEO) {
            cacheDir = new File(cacheDir.getAbsolutePath() + File.separator + "video");
        } else if (type == FileType.TYPE_IMAGE) {
            cacheDir = new File(cacheDir.getAbsolutePath() + File.separator + "image");
        } else {

        }
        if (!cacheDir.exists())
            cacheDir.mkdirs();
        if (fileName == null) {
            fileName = generateCacheName(url);
        }
        File file = new File(cacheDir, fileName);
        return file;
    }


    private OnDownloadingListener mOnDownloadDispatcher = new OnDownloadingListener() {
        @Override
        public void onDownloadFailed(final FileDownloadTask downloadTask, final int errorType, final String msg) {
//            final OnDownloadingListener downloadingListener = mDowndloadingMap.remove(downloadInfo);
            FileDownloadInfo info = downloadTask.getFileDownloadInfo();
            List<OnDownloadingListener> downloadingListenerList = mDowndloadingMap.get(info.getUrl());
//            mProgressMap.remove(downloadInfo);
            synchronized (mTaskList) {
                mTaskList.remove(downloadTask);
            }
            if (downloadingListenerList != null && downloadingListenerList.size() > 0) {
                for (final OnDownloadingListener downloadingListener : downloadingListenerList) {
                    if (downloadingListener != null) {
                        if (downloadTask.isSyncLoading()) {
                            downloadingListener.onDownloadFailed(downloadTask, errorType, msg);
                        } else {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    downloadingListener.onDownloadFailed(downloadTask, errorType, msg);
                                }
                            });
                        }
                    }
                }
            }
        }

        @Override
        public void onDownloadSucc(final FileDownloadTask downloadTask, final File outFile) {
//            final OnDownloadingListener downloadingListener = mDowndloadingMap.remove(downloadInfo);
            FileDownloadInfo info = downloadTask.getFileDownloadInfo();
            List<OnDownloadingListener> downloadingListenerList = mDowndloadingMap.remove(info.getUrl());
            mProgressMap.remove(info.getUrl());
            synchronized (mTaskList) {
                mTaskList.remove(downloadTask);
            }
            if (downloadingListenerList != null && downloadingListenerList.size() > 0) {
                for (final OnDownloadingListener downloadingListener : downloadingListenerList) {
                    if (downloadingListener != null) {
                        if (downloadTask.isSyncLoading()) {
                            downloadingListener.onDownloadSucc(downloadTask, outFile);
                        } else {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    downloadingListener.onDownloadSucc(downloadTask, outFile);
                                }
                            });
                        }
                    }
                }
            }
        }
    };

    private OnDownloadProgressListener mOnDwonloadProgressDispatcher = new OnDownloadProgressListener() {
        @Override
        public void onProgressUpdate(final FileDownloadTask downloadTask, final long current, final long totalSize) {
            FileDownloadInfo info = downloadTask.getFileDownloadInfo();
            List<OnDownloadProgressListener> downloadProgressListenerList = mProgressMap.get(info.getUrl());
            if (downloadProgressListenerList != null && downloadProgressListenerList.size() >0) {
                for (final OnDownloadProgressListener progressListener : downloadProgressListenerList) {
                    if (progressListener != null) {
                        long t = totalSize;
                        if (t == 0)
                            t = 1;
                        final int progress = (int) ((current / (float) t) * 100);
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                downloadTask.updateProgress(progress);
                                progressListener.onProgressUpdate(downloadTask, current, totalSize);
                            }
                        });
                    }
                }
            }
        }

    };

    private class SyncDownloadLister implements OnDownloadingListener {

        private File result = null;

        @Override
        public void onDownloadFailed(FileDownloadTask task, int errorType, String msg) {

        }

        @Override
        public void onDownloadSucc(FileDownloadTask task, File outFile) {
            result = task.getFileDownloadInfo().getOutFile();
        }

        public File getResult() {
            return result;
        }

    }

    private <K, V> void putListener(Map<K, List<V>> listenerMap, K key, V value) {
        List<V> list = listenerMap.get(key);
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(value);
        listenerMap.put(key, list);
    }

    public void addDownloadingListener(String url, OnDownloadingListener listener) {
        putListener(mDowndloadingMap, url, listener);
    }

    public void addDownloadingProgressListener(String url, OnDownloadProgressListener listener) {
        putListener(mProgressMap, url, listener);
    }

    public void resumeTasks() {
        if (mAllTaskList != null && mAllTaskList.size() > 0) {
            for (FileDownloadTask task : mAllTaskList) {
                task.setDownloadNew(false);
                mDownloadConfiguration.getTaskExecutor().execute(task);
            }
        }
    }

    public void executeTask(FileDownloadTask task, boolean downloadNew) {
        if (task == null) {
            return;
        }
        if (downloadNew) {
            task.setDownloadStatus(FileDownloadTask.STATUS_DOWNLOADING);
            task.setCurrSize(0);
            task.setStartTime(System.currentTimeMillis());
        }
        task.setDownloadNew(downloadNew);
        mDownloadConfiguration.getTaskExecutor().execute(task);
    }
}